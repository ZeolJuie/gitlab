# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Groups > Usage Quotas' do
  let_it_be(:user) { create(:user) }

  let(:group) { create(:group) }
  let!(:project) { create(:project, namespace: group, shared_runners_enabled: true) }
  let(:gitlab_dot_com) { true }

  before do
    allow(Gitlab).to receive(:com?).and_return(gitlab_dot_com)

    group.add_owner(user)
    sign_in(user)
  end

  describe 'new_route_ci_minutes_purchase feature flag' do
    context 'when is enabled' do
      it 'points to GitLab purchase flow' do
        visit group_usage_quotas_path(group)

        expect(page).to have_link('Buy additional minutes', href: ::Gitlab::SubscriptionPortal.subscriptions_more_minutes_url)
      end
    end
  end
end
