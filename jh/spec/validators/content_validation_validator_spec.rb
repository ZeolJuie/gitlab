# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ContentValidationValidator do
  let(:validator) { described_class.new(attributes: [:name]) }

  describe '#validates_each' do
    it 'content has sentitive words' do
      stub_content_validation_request(false)
      user = build(:user)

      validator.validate_each(user, :name, 'sensitive words')

      expect(user.errors[:name]).to match_array(['sensitive or illegal characters involved'])
    end

    it 'has no sentitive words' do
      stub_content_validation_request(true)
      user = build(:user)

      validator.validate_each(user, :name, 'Hello gitlab')

      expect(user.errors[:name]).to eq([])
    end

    it 'content validation service raise error' do
      stub_content_validation_request(false)
      allow_next_instance_of(ContentValidation::ContentValidationService) do |instance|
        allow(instance).to receive(:valid?).and_raise(StandardError)
      end

      user = build(:user)

      validator.validate_each(user, :name, 'Hello gitlab')

      expect(user.errors[:name]).to eq([])
    end
  end
end
