# frozen_string_literal: true

require 'spec_helper'

RSpec.describe ContentValidation::ContentValidationService do
  let(:content_validation_service) { ContentValidation::ContentValidationService.new }

  describe '#valid?' do
    context 'with webmock response true' do
      # rubocop: disable CodeReuse/ActiveRecord
      before do
        stub_content_validation_request(true)
      end
      # rubocop: enable CodeReuse/ActiveRecord
      it 'contain untrust content' do
        expect(content_validation_service.valid?("Hello giltab!")).to be true
      end
    end

    context 'with webmock response false' do
      # rubocop: disable CodeReuse/ActiveRecord
      before do
        stub_content_validation_request(false)
      end
      # rubocop: enable CodeReuse/ActiveRecord
      it 'trust content' do
        expect(content_validation_service.valid?("wow sensitive")).to be false
      end
    end
  end
end
