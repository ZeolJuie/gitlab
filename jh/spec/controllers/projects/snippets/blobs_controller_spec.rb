# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Snippets::BlobsController do
  describe 'GET #raw' do
    let_it_be(:user) { create(:user) }

    context 'with content blockd state' do
      let(:snippet) { create(:personal_snippet, :public, :repository, author: user) }
      let(:blob) { snippet.blobs.first }
      let(:commit) { snippet.repository.last_commit_for_path(snippet.default_branch, blob.path, literal_pathspec: false)}
      let(:content_blocked_state) { create(:content_blocked_state, container: snippet, commit_sha: commit.id, path: blob.path) }
      let(:blocked_message) { s_("ContentValidation|According to the relevant laws and regulations, this content is not displayed.") }

      before do
        allow(ContentValidation::Setting).to receive(:block_enabled?).and_return(true)
        sign_in(user)
      end

      it "return blocked message" do
        content_blocked_state
        get(:raw,
          params: {
            snippet_id: snippet,
            path: blob.path,
            ref: commit.id,
            inline: false
          })
        expect(response.body).to eq(blocked_message)
      end
    end
  end
end
