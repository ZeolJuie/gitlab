# frozen_string_literal: true

require 'spec_helper'

RSpec.describe NamespacesHelper do
  let!(:user) { create(:user) }
  let!(:user_project_creation_level) { nil }

  let(:user_group) do
    create(:namespace, :with_ci_minutes,
      project_creation_level: user_project_creation_level,
      owner: user,
      ci_minutes_used: ci_minutes_used)
  end

  let(:ci_minutes_used) { 100 }

  describe '#buy_additional_minutes_path' do
    subject { helper.buy_additional_minutes_path(namespace) }

    let(:namespace) { build_stubbed(:group) }

    it { is_expected.to eq ::Gitlab::SubscriptionPortal.subscriptions_more_minutes_url }

    context 'new_route_ci_minutes_purchase' do
      context 'when is disabled' do
        before do
          stub_feature_flags(new_route_ci_minutes_purchase: false)
        end

        it { is_expected.to eq EE::SUBSCRIPTIONS_MORE_MINUTES_URL }
      end

      context 'when new_route_ci_minutes_purchase is enabled only for a specific namespace' do
        let(:enabled_namespace) { build_stubbed(:group) }

        before do
          stub_feature_flags(new_route_ci_minutes_purchase: false)
          stub_feature_flags(new_route_ci_minutes_purchase: enabled_namespace)
        end

        it 'returns GitLab purchase path for the disabled namespace' do
          expect(helper.buy_additional_minutes_path(enabled_namespace)).to eq ::Gitlab::SubscriptionPortal.subscriptions_more_minutes_url
        end
      end
    end
  end

  describe '#buy_storage_path' do
    subject { helper.buy_storage_path(namespace) }

    let(:namespace) { build_stubbed(:group) }

    it { is_expected.to eq ::Gitlab::SubscriptionPortal.subscriptions_more_storage_url }

    context 'new_route_storage_purchase' do
      context 'when new_route_storage_purchase is enabled only for a specific namespace' do
        let(:enabled_namespace) { build_stubbed(:group) }

        before do
          stub_feature_flags(new_route_storage_purchase: false)
          stub_feature_flags(new_route_storage_purchase: enabled_namespace)
        end

        it 'returns GitLab purchase path for the disabled namespace' do
          expect(helper.buy_storage_path(enabled_namespace)).to eq ::Gitlab::SubscriptionPortal.subscriptions_more_storage_url
        end
      end
    end
  end
end
