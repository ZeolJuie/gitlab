# frozen_string_literal: true

require 'fast_spec_helper'

RSpec.describe Gitlab::Patch::DrawRoute do
  subject do
    Class.new do
      include Gitlab::Patch::DrawRoute

      def route_path(route_name)
        File.expand_path("../../../../../#{route_name}", __dir__)
      end
    end.new
  end

  before do
    allow(subject).to receive(:instance_eval)
  end

  it 'evaluates JH only routes' do
    subject.draw(:province)

    # rubocop: disable CodeReuse/ActiveRecord
    expect(subject).to have_received(:instance_eval)
      .with(File.read(subject.route_path('jh/config/routes/province.rb')))
      .once
    # rubocop: enable CodeReuse/ActiveRecord

    expect(subject).to have_received(:instance_eval)
      .once
  end
end
