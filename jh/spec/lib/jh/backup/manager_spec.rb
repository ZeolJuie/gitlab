# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Backup::Manager do
  include StubENV

  let(:progress) { StringIO.new }

  subject { described_class.new(progress) }

  before do
    allow(progress).to receive(:puts)
    allow(progress).to receive(:print)
  end

  describe '#remove_old' do
    let(:files) do
      [
        '1451520000_2015_12_31_4.5.6-pre-jh_gitlab_backup.tar'
      ]
    end

    before do
      allow(Dir).to receive(:chdir).and_yield
      allow(Dir).to receive(:glob).and_return(files)
      allow(FileUtils).to receive(:rm)
      allow(Time).to receive(:now).and_return(Time.utc(2016))
    end

    context 'when keep_time is set to remove files' do
      before do
        # Set to 1 second
        allow(Gitlab.config.backup).to receive(:keep_time).and_return(1)

        subject.remove_old
      end

      # rubocop: disable CodeReuse/ActiveRecord
      it 'removes matching files with a human-readable versioned timestamp with tagged JH' do
        expect(FileUtils).to have_received(:rm).with(files[0])
      end
      # rubocop: enable CodeReuse/ActiveRecord
    end
  end
end
