# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Issue do
  describe 'validations' do
    it_behaves_like "content validation with project", :issue, :title
    it_behaves_like "content validation with project", :issue, :description
  end
end
