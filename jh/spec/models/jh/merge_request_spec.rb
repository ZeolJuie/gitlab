# frozen_string_literal: true

require 'spec_helper'

RSpec.describe MergeRequest do
  describe 'validations' do
    it_behaves_like "content validation with project", :merge_request, :title
    it_behaves_like "content validation with project", :merge_request, :description
  end
end
