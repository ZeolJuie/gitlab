# frozen_string_literal: true

module SkipSpecs
  module_function

  def skip?(example)
    example_location = example.location
    example_index = "#{example.file_path}[#{example.metadata[:scoped_id]}]"

    skipped_specs.find do |skipped_example_index|
      example_location.include?(skipped_example_index) ||
        example_index.include?(skipped_example_index)
    end
  end

  def skipped_specs
    @skipped_specs ||= File.readlines(
      File.expand_path('list', __dir__),
      "\n",
      chomp: true
    )
  end
end
