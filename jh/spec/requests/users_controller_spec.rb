# frozen_string_literal: true

require 'spec_helper'

RSpec.describe UsersController do
  let(:user_from_oauth) { create(:user, email: 'temp-email-for-oauth-u1@email.com') }
  let(:user) { create(:user, email: GpgHelpers::User1.emails[0]) }
  let(:phone) { "+8615688888888" }

  describe 'GET #phone_exists' do
    before do
      create(:user, email: 'temp-email-for-oauth-u2@email.com', phone: phone)
    end

    context 'when phone exists' do
      it 'returns JSON indicating the phone exists with login by oauth' do
        sign_in(user_from_oauth)
        get user_phone_exists_url phone

        # here tests https://gitlab.com/gitlab-jh/gitlab/-/merge_requests/186
        # https://gitlab.com/gitlab-jh/gitlab/-/issues/389
        expect(response).to have_gitlab_http_status(:ok)

        expected_json = { exists: true }.to_json
        expect(response.body).to eq(expected_json)
      end

      it 'returns JSON indicating the phone exists with login by normal' do
        sign_in(user)
        get user_phone_exists_url phone

        expected_json = { exists: true }.to_json
        expect(response.body).to eq(expected_json)
      end
    end
  end
end
