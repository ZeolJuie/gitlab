import MockAdapter from 'axios-mock-adapter';
import { merge } from 'lodash';
import { GlForm, GlButton, GlFormGroup, GlFormSelect, GlFormInputGroup } from '@gitlab/ui';
import PhoneForm from 'jh/terms/components/phone_form.vue';
import { mountExtended } from 'helpers/vue_test_utils_helper';
import axios from '~/lib/utils/axios_utils';
import httpStatus from '~/lib/utils/http_status';
import waitForPromises from 'helpers/wait_for_promises';
import { s__ } from '~/locale';
import { captchaCheck } from 'jh/captcha';
import createFlash from '~/flash';
import Api from 'jh/api';
import { verificationCodePath } from 'jh/rest_api';

jest.mock('~/locale', () => ({
  ...jest.requireActual('~/locale'),
  s__: (key) => key,
}));

jest.mock('jh/captcha', () => ({
  captchaCheck: jest.fn().mockImplementation(() =>
    Promise.resolve({
      randstr: 'some_callback_string',
      ticket: 'test_ticket',
      ip: '127.0.0.1',
    }),
  ),
}));

jest.mock('~/flash');

describe('PhoneForm', () => {
  const dummyUrlRoot = '/gitlab';
  const dummyGon = {
    relative_url_root: dummyUrlRoot,
  };
  const defaultProvide = {
    paths: {
      accept: '/-/users/terms/1/accept',
    },
  };
  const mainlandNumber = '13800000000';
  const hkNumber = '00000000';
  const macauNumber = '0000000';

  let wrapper;
  let mock;
  let originalGon;

  const findAreaCodeSelector = () => wrapper.findComponent(GlFormSelect);
  const findAreaCodeOptions = () => findAreaCodeSelector().findAll('option');
  const findVerificationInputComponent = () => wrapper.findComponent(GlFormInputGroup);
  const findVerificationInputElement = () => findVerificationInputComponent().find('input');
  const findSenCodeButton = () => wrapper.findComponent(GlButton);

  const createComponent = (provide = {}) => {
    wrapper = mountExtended(PhoneForm, {
      provide: merge({}, defaultProvide, provide),
    });
  };

  beforeEach(() => {
    mock = new MockAdapter(axios);
    originalGon = window.gon;
    window.gon = { ...dummyGon };
  });

  afterEach(() => {
    wrapper.destroy();
    mock.restore();
    window.gon = originalGon;
    captchaCheck.mockClear();
    createFlash.mockClear();
  });

  it('should render correctly', () => {
    createComponent();

    expect(wrapper.element).toMatchSnapshot();
    expect(wrapper.findComponent(GlForm).attributes('action')).toEqual(defaultProvide.paths.accept);
    expect(findSenCodeButton().props('disabled')).toBe(true);
    expect(wrapper.vm.isValidPhone).toBe(false);
    const formGroups = wrapper.findAllComponents(GlFormGroup);
    expect(formGroups).toHaveLength(2);
    // These lines should be tested after i18n function gets fixed.
    // expect(formGroups.at(0).attributes('label')).toBe(s__('RealName|Phone'));
    // expect(formGroups.at(1).attributes('label')).toBe(s__('RealName|Verification code'));
  });

  it('should be able to submit form to any endpoint', () => {
    const otherPath = '/-/users/other/1/accept';
    createComponent({
      paths: {
        accept: otherPath,
      },
    });

    expect(wrapper.findComponent(GlForm).attributes('action')).toEqual(otherPath);
  });

  it('should change area code correctly', async () => {
    createComponent();

    const selectComponent = findAreaCodeSelector();
    expect(selectComponent.exists()).toBe(true);
    const areaCodes = findAreaCodeOptions();
    expect(areaCodes).toHaveLength(3);
    expect(wrapper.vm.areaCode).toEqual(areaCodes.at(0).attributes('value'));
    expect(wrapper.vm.isValidPhone).toBe(false);
    await areaCodes.at(1).setSelected();

    expect(wrapper.vm.areaCode).toEqual(areaCodes.at(1).attributes('value'));
    expect(wrapper.vm.isValidPhone).toBe(false);
  });

  describe('phone existence check', () => {
    it('should be able send code after set valid phone number', async () => {
      mock
        .onGet(`${dummyGon.relative_url_root}/users/+86${mainlandNumber}/phone_exists`)
        .reply(httpStatus.OK, {
          exists: false,
        });
      createComponent();
      const inputComponent = findVerificationInputComponent();
      expect(inputComponent.exists()).toBe(true);
      const inputBoxElement = inputComponent.find('input');
      expect(inputBoxElement.exists()).toBe(true);
      expect(wrapper.vm.isValidPhone).toBe(false);

      // current area code = +86
      await inputBoxElement.setValue(mainlandNumber);
      await waitForPromises();
      expect(wrapper.vm.phoneNumber).toBe(mainlandNumber);
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.phoneAvailable).toBe(s__('RealName|Phone is available.'));
      expect(findSenCodeButton().props('disabled')).toBe(false);
    });

    it('should be able to validate the phone number after area code changed', async () => {
      mock
        .onGet(`${dummyGon.relative_url_root}/users/+86${mainlandNumber}/phone_exists`)
        .reply(httpStatus.OK, {
          exists: false,
        });
      createComponent();

      const inputBoxElement = findVerificationInputElement();
      await inputBoxElement.setValue(mainlandNumber);
      await waitForPromises();
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.phoneAvailable).toBe(s__('RealName|Phone is available.'));
      expect(findSenCodeButton().props('disabled')).toBe(false);

      const areaCodes = findAreaCodeOptions();
      await areaCodes.at(1).setSelected();
      await waitForPromises();
      expect(wrapper.vm.isValidPhone).toBe(false);
      expect(wrapper.vm.phoneAvailable).toBe('');
      expect(findSenCodeButton().props('disabled')).toBe(true);
    });

    it('should validate HK and Macau phone number', async () => {
      mock
        .onGet(`${dummyGon.relative_url_root}/users/+852${hkNumber}/phone_exists`)
        .reply(httpStatus.OK, {
          exists: false,
        });
      mock
        .onGet(`${dummyGon.relative_url_root}/users/+853${macauNumber}/phone_exists`)
        .reply(httpStatus.OK, {
          exists: false,
        });
      createComponent();

      const areaCodes = findAreaCodeOptions();
      await areaCodes.at(1).setSelected();

      const inputBoxElement = findVerificationInputElement();
      await inputBoxElement.setValue(hkNumber);
      await waitForPromises();
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.phoneAvailable).toBe(s__('RealName|Phone is available.'));
      expect(findSenCodeButton().props('disabled')).toBe(false);

      await inputBoxElement.setValue('');
      await areaCodes.at(2).setSelected();
      await inputBoxElement.setValue(macauNumber);
      await waitForPromises();
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.phoneAvailable).toBe(s__('RealName|Phone is available.'));
      expect(findSenCodeButton().props('disabled')).toBe(false);
    });

    it('should not be able to send code if number exists', async () => {
      mock
        .onGet(`${dummyGon.relative_url_root}/users/+86${mainlandNumber}/phone_exists`)
        .reply(httpStatus.OK, {
          exists: true,
        });

      createComponent();
      const inputBoxElement = findVerificationInputElement();
      await inputBoxElement.setValue(mainlandNumber);
      await waitForPromises();
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.errorMsg).toBe(s__('RealName|Phone is already taken.'));
      expect(findSenCodeButton().props('disabled')).toBe(true);
    });
  });

  describe('send verification code', () => {
    let sendCodeEndpoint;

    beforeEach(() => {
      sendCodeEndpoint = Api.buildUrl(verificationCodePath);
      mock
        .onGet(`${dummyGon.relative_url_root}/users/+86${mainlandNumber}/phone_exists`)
        .reply(httpStatus.OK, {
          exists: false,
        });
    });

    it('should be able to send verification code', async () => {
      mock.onPost(sendCodeEndpoint).reply(httpStatus.OK, {
        status: 'OK',
      });
      createComponent();

      const inputBoxElement = findVerificationInputElement();
      await inputBoxElement.setValue(mainlandNumber);
      await waitForPromises();

      const sendButton = findSenCodeButton();
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.phoneAvailable).toBe(s__('RealName|Phone is available.'));
      expect(sendButton.props('disabled')).toBe(false);

      await sendButton.trigger('click');
      await waitForPromises();
      expect(wrapper.vm.resendCounter).not.toBeNull();
      expect(sendButton.props('disabled')).toBe(true);
    });

    it('should be rate limited', async () => {
      mock.onPost(sendCodeEndpoint).reply(httpStatus.OK, {
        status: 'SENDING_LIMIT_RATE_ERROR',
      });
      createComponent();

      const inputBoxElement = findVerificationInputElement();
      await inputBoxElement.setValue(mainlandNumber);
      await waitForPromises();

      const sendButton = findSenCodeButton();
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.phoneAvailable).toBe(s__('RealName|Phone is available.'));
      expect(sendButton.props('disabled')).toBe(false);
      expect(createFlash).not.toHaveBeenCalled();

      await sendButton.trigger('click');
      await waitForPromises();
      expect(wrapper.vm.resendCounter).toBeNull();
      expect(createFlash).toHaveBeenCalled();
    });

    it('should not send code if responses not ok', async () => {
      mock.onPost(sendCodeEndpoint).reply(httpStatus.OK, {
        status: 'SOME_OTHER_CODE',
      });
      createComponent();

      const inputBoxElement = findVerificationInputElement();
      await inputBoxElement.setValue(mainlandNumber);
      await waitForPromises();

      const sendButton = findSenCodeButton();
      expect(wrapper.vm.isValidPhone).toBe(true);
      expect(wrapper.vm.phoneAvailable).toBe(s__('RealName|Phone is available.'));
      expect(sendButton.props('disabled')).toBe(false);
      expect(createFlash).not.toHaveBeenCalled();

      await sendButton.trigger('click');
      await waitForPromises();
      expect(wrapper.vm.resendCounter).toBeNull();
      expect(createFlash).toHaveBeenCalled();
    });
  });
});
