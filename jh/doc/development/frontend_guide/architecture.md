---
stage: none
group: unassigned
---

# 极狐GitLab架构

本文主要介绍极狐GitLab本身的架构，关于 GitLab CE/EE 的架构请看 [GitLab 架构](https://docs.gitlab.com/ee/development/fe_guide/architecture.html)。

## 极狐GitLab与 GitLab EE 的关系

极狐GitLab基于 GitLab EE 即包含所有 GitLab EE 的功能，并且还具有极狐GitLab独有的功能。

## 极狐GitLab的加载机制

待添加。

## 极狐GitLab如何基于 GitLab EE 本身开发

由于 [GitLab](https://gitlab.com/gitlab-org/gitlab) 本身的特殊性，每一个前端页面都对应一个独立的 Vue 实例，与传统的单页应用有差别，所以每一个页面都需要一个独立的 index.html.haml（模板文件，里面可以访问 Rails 的变量，从而为渲染的页面提供元数据）+ index.js 来启用。路由的管理是由 Ruby 来完成的，前端无需关心路由问题。

极狐GitLab的代码存放在[仓库地址](https://gitlab.com/gitlab-jh/gitlab)，这个仓库是基于 [GitLab 上游仓库](https://gitlab.com/gitlab-org/gitlab) 的一个[镜像](https://docs.gitlab.com/ee/user/project/repository/mirror/)，为了在 GitLab EE 的基础上进行开发，继承原有提供的功能以及加入本土化的功能，并且不让镜像代码产生冲突，所有的极狐GitLab的开发代码都放在 `jh/` 目录下，通过 Ruby 的特殊方法来加载极狐GitLab的代码，从而对上游不产生影响也兼顾了自己的代码开发。在极狐GitLab的仓库代码中，`jh/` 目录下的文件的优先级顺序为 **`jh/` > `ee/` > `ce/`**。在这三个目录同时拥有某个文件时，所有 `jh/` 目录下的文件都会优于其他目录下的文件加载。
