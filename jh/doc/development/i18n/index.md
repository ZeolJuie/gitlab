---
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 翻译 GitLab 界面字符串

GitLab 用户界面中的文本字符串可以翻译成中文。随着每个字符串被翻译，它被添加到语言翻译文件中，并在未来的 GitLab 版本中可用。

我们始终欢迎对翻译的贡献，有助于完善和改进用户体验。部分字符串还不能用于翻译，因为它们还没有被外部化。

<!--
There are many ways you can contribute in translating GitLab.
-->

<!--
## Externalize strings

Before a string can be translated, it must be externalized. This is the process where English
strings in the GitLab source code are wrapped in a function that retrieves the translated string for
the user's language.

As new features are added and existing features are updated, the surrounding strings are
externalized. However, there are many parts of GitLab that still need more work to externalize all
strings.

See [Externalization for GitLab](externalization.md).
-->

## 翻译字符串

翻译过程在 [https://translate.gitlab.com](https://translate.gitlab.com) 使用 [Crowdin](https://crowdin.com/) 进行管理。
您必须先创建一个 Crowdin 帐户，然后才能提交翻译。登录后，选择您希望提供翻译的语言，比如 Chinese simplified 简体中文。

为翻译投票也很有价值，有助于确认好的翻译和标记不准确的翻译。

请参阅[翻译指南](translation.md)。

## 校对

校对有助于确保翻译的准确性和一致性。所有翻译在被接受之前都经过校对。<!--如果翻译需要更改，解释原因的注释会通知您。-->

请参阅[校对翻译](proofreader.md)，获取有关校对员的更多信息。

## 发布

翻译通常包含在未来的主要或次要版本中。

请参阅[合并来自 Crowdin 的翻译](merging_translations.md)。
