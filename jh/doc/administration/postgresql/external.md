---
stage: Enablement
group: Database
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用外部 PostgreSQL 服务配置 GitLab **(FREE SELF)**

如果您在云提供商上托管 GitLab，您可以选择使用 PostgreSQL 托管服务。例如，AWS 提供了一个运行 PostgreSQL 的托管关系数据库服务 (RDS)。

或者，您可以选择独立于 Omnibus GitLab 包，管理您自己的 PostgreSQL 实例或集群。

如果您使用云托管服务，或提供您自己的 PostgreSQL 实例：

1. 根据[数据库需求文档](../../install/requirements.md#数据库)设置 PostgreSQL。
1. 使用您选择的密码设置一个 `gitlab` 用户，创建 `gitlabhq_production` 数据库，并使该用户成为数据库的所有者。<!--You can see an example of this setup in the [installation from source documentation](../../install/installation.md#6-database).-->
1. 如果您使用的是云管理服务，您可能需要为您的 `gitlab` 用户授予额外的角色：
   - Amazon RDS 需要 [`rds_superuser`](https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.PostgreSQL.CommonDBATasks.html#Appendix.PostgreSQL.CommonDBATasks.Roles) 角色。
   - Azure Database for PostgreSQL 需要 [`azure_pg_admin`](https://docs.microsoft.com/en-us/azure/postgresql/howto-create-users#how-to-create-additional-admin-users-in-azure-database-for-postgresql) 角色。
   - Google Cloud SQL 需要 [`cloudsqlsuperuser`](https://cloud.google.com/sql/docs/postgres/users#default-users) 角色。

   这是用于在安装和升级期间安装扩展。作为替代方案，确保手动安装扩展，并阅读未来升级过程中可能出现的问题<!--[确保手动安装扩展，并阅读未来升级过程中可能出现的问题](../../install/postgresql_extensions.md)-->。

1. 在 `/etc/gitlab/gitlab.rb` 文件中，为您的外部 PostgreSQL 服务配置与 GitLab 应用服务器连接的适当细节：

    ```ruby
    # Disable the bundled Omnibus provided PostgreSQL
    postgresql['enable'] = false

    # PostgreSQL connection details
    gitlab_rails['db_adapter'] = 'postgresql'
    gitlab_rails['db_encoding'] = 'unicode'
    gitlab_rails['db_host'] = '10.1.0.5' # IP/hostname of database server
    gitlab_rails['db_password'] = 'DB password'
    ```

   <!--有关 GitLab 多节点设置的更多信息，请参阅[参考架构](../reference_architectures/index.md)。-->

1. 重新配置以使更改生效：

   ```shell
   sudo gitlab-ctl reconfigure
   ```
