---
stage: Enablement
group: Geo
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# Geo Rake 任务 **(PREMIUM SELF)**

以下 Rake 任务适用于 Geo 安装<!--[Geo 安装](../geo/index.md)-->。

## Git housekeeping

很少有您可以运行的任务来安排 Git housekeeping，以便在**次要**节点中的下一个仓库同步时开始：

### 增量重新打包

相当于在 *bare* 存储库上运行 `git repack -d`。

**Omnibus 安装实例**

```shell
sudo gitlab-rake geo:git:housekeeping:incremental_repack
```

**源安装实例**

```shell
sudo -u git -H bundle exec rake geo:git:housekeeping:incremental_repack RAILS_ENV=production
```

### 完全重新包装

这相当于在 *bare* 仓库上运行 `git repack -d -A --pack-kept-objects`，当在 GitLab 中启用此功能时，该仓库将可选地写入可达性位图索引。

**Omnibus 安装实例**

```shell
sudo gitlab-rake geo:git:housekeeping:full_repack
```

**源安装实例**

```shell
sudo -u git -H bundle exec rake geo:git:housekeeping:full_repack RAILS_ENV=production
```

### GC

这相当于在 *bare* 仓库上运行 `git repack -d -A --pack-kept-objects`，当在 GitLab 中启用它时，它可以选择写入可达位图索引。

**Omnibus 安装实例**

```shell
sudo gitlab-rake geo:git:housekeeping:gc
```

**源安装实例**

```shell
sudo -u git -H bundle exec rake geo:git:housekeeping:gc RAILS_ENV=production
```

## 删除孤立的项目库

在某些情况下，您的项目库可能包含过时的记录，您可以使用 Rake 任务 `geo:run_orphaned_project_registry_cleaner` 删除它们：

**Omnibus 安装实例**

```shell
sudo gitlab-rake geo:run_orphaned_project_registry_cleaner
```

**源安装实例**

```shell
sudo -u git -H bundle exec rake geo:run_orphaned_project_registry_cleaner RAILS_ENV=production
```
