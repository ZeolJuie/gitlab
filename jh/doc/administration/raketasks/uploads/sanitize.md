---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 上传文件清理 Rake 任务 **(FREE SELF)**

EXIF 数据会自动从 JPG 或 TIFF 图像上传中剥离。

EXIF 数据可能包含敏感信息（例如 GPS 位置），因此您可以从上传到早期版本的 GitLab 的现有图像中删除 EXIF 数据。

## 要求

要运行这个 Rake 任务，需要在您的系统上安装 `exiftool`。如果您安装了 GitLab：

- 使用 Omnibus 包，一切就绪。
- 从源代码，确保安装了`exiftool`：

  ```shell
  # Debian/Ubuntu
  sudo apt-get install libimage-exiftool-perl

  # RHEL/CentOS
  sudo yum install perl-Image-ExifTool
  ```

## 从现有上传数据中删除 EXIF 数据

要从现有上传数据中删除 EXIF 数据，请运行以下命令：

```shell
sudo RAILS_ENV=production -u git -H bundle exec rake gitlab:uploads:sanitize:remove_exif
```

默认情况下，此命令以“试运行”模式运行，并且不会删除 EXIF 数据。 它可用于检查是否（以及多少）图像应该被清理。

Rake 任务接受以下参数。

| 参数    | 类型   | 说明                                                                                                              |
|:-------------|:--------|:----------------------------------------------------------------------------------------------------------------------------|
| `start_id`   | integer | 仅处理具有相同或更大 ID 的上传文件                                                                    |
| `stop_id`    | integer | 仅处理 ID 相同或更小的上传文件                                                                    |
| `dry_run`    | boolean | 不要删除 EXIF 数据，只检查 EXIF 数据是否存在。默认为`true`                                     |
| `sleep_time` | float   | 处理每个图像后暂停秒数。默认为 0.3 seconds                                           |
| `uploader`   | string  | 仅对给定上传器的上传文件运行清理：`FileUploader`、`PersonalFileUploader` 或 `NamespaceFileUploader` |
| `since`      | date    | 仅对比给定日期更新的上传文件运行清理。例如，`2019-05-01`                                          |

如果上传文件过多，可以通过以下方式加快清理速度：

- 将 `sleep_time` 设置为较低的值。
- 并行运行多个 Rake 任务，每个任务都有单独的上传 ID 范围（通过设置 `start_id` 和 `stop_id`）。

要从所有上传文件中删除 EXIF 数据，请使用：

```shell
sudo RAILS_ENV=production -u git -H bundle exec rake gitlab:uploads:sanitize:remove_exif[,,false,] 2>&1 | tee exif.log
```

要删除 ID 介于 100 和 5000 之间的上传的 EXIF 数据并在每个文件后暂停 0.1 秒，请使用：

```shell
sudo RAILS_ENV=production -u git -H bundle exec rake gitlab:uploads:sanitize:remove_exif[100,5000,false,0.1] 2>&1 | tee exif.log
```

输出被写入一个 `exif.log` 文件，因为它通常很长。

如果上传文件的清理失败，则 Rake 任务的输出中应显示错误消息。典型的原因包括文件在存储中丢失或它不是有效的图像。

<!--
[Report](https://gitlab.com/gitlab-org/gitlab/-/issues/new) any issues and use the prefix 'EXIF' in
the issue title with the error output and (if possible) the image.
-->