---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# OmniAuth **(FREE SELF)**

<!--
GitLab leverages OmniAuth to allow users to sign in using Twitter, GitHub, and
other popular services. [OmniAuth](https://rubygems.org/gems/omniauth/) is a
"generalized Rack framework for multiple-provider authentication" built on Ruby.

Configuring OmniAuth does not prevent standard GitLab authentication or LDAP
(if configured) from continuing to work. Users can choose to sign in using any
of the configured mechanisms.

- [Initial OmniAuth Configuration](#initial-omniauth-configuration)
- [Supported Providers](#supported-providers)
- [Enable OmniAuth for an Existing User](#enable-omniauth-for-an-existing-user)
- [OmniAuth configuration sample when using Omnibus GitLab](https://gitlab.com/gitlab-org/omnibus-gitlab/tree/master#omniauth-google-twitter-github-login)
- [Enable or disable Sign In with an OmniAuth provider without disabling import sources](#enable-or-disable-sign-in-with-an-omniauth-provider-without-disabling-import-sources)
-->

## 支持的提供商

这是当前支持的 OmniAuth 提供商的列表。在继续阅读每个提供商的文档之前，请务必先阅读本文档，因为它包含所有提供商通用的一些设置。

| 提供商                                              | OmniAuth 提供商名称    |
|---------------------------------------------------------------------|----------------------------|
| [DingTalk](ding_talk.md)                                            | `dingtalk`                |
| GitHub                                                 | `github`                   |
| GitLab.com                                             | `gitlab`                   |

## 初始化配置

NOTE:
在 11.4 及更高版本中，OmniAuth 默认处于启用状态。如果您使用的是早期版本，则必须明确启用它。

在配置 OmniAuth provider 之前，请配置所有提供商通用的设置。

设置                    | 描述 | 默认值
---------------------------|-------------|--------------
`allow_single_sign_on`     | 使您能够列出自动创建极狐GitLab 帐户的提供程序。提供者名称在[支持的提供商列表](#支持的提供商)的 **OmniAuth 提供商名称** 列中可用。 | 默认值为`false`。 如果为`false`，则必须手动创建用户，否则他们无法使用 OmniAuth 登录。
`auto_link_ldap_user`      | 如果启用，则在极狐GitLab 中为通过 OmniAuth 提供商创建的用户创建 LDAP 身份。如果您启用了 [LDAP (ActiveDirectory)](../administration/auth/ldap/index.md) 集成，则可以启用此设置。 要求用户的 `uid` 在 LDAP 和 OmniAuth 提供商中都相同。 | 默认值为 `false`.
`block_auto_created_users` | 如果启用，将阻止自动创建的用户登录，直到他们获得管理员批准。 | 默认值为 `true`。如果您将该值设置为 `false`，请确保只为您可以控制的 `allow_single_sign_on` 定义提供程序，例如 SAML、Shibboleth、Crowd 或 Google。 否则，互联网上的任何用户都可以在未经管理员批准的情况下登录 GitLab。

要更改这些设置：

- **对于 Omnibus 安装实例**

  1. 打开配置文件：

     ```shell
     sudo editor /etc/gitlab/gitlab.rb
     ```

  1. 更新以下部分：

     ```ruby
     # CAUTION!
     # This allows users to sign in without having a user account first. Define the allowed providers
     # using an array, for example, ["github", "gitlab"], or as true/false to allow all providers or none.
     # User accounts will be created automatically when authentication was successful.
     gitlab_rails['omniauth_allow_single_sign_on'] = ['github', 'gitlab']
     gitlab_rails['omniauth_auto_link_ldap_user'] = true
     gitlab_rails['omniauth_block_auto_created_users'] = true
     ```

- **对于源安装实例**

  1. 打开配置文件：

     ```shell
     cd /home/git/gitlab

     sudo -u git -H editor config/gitlab.yml
     ```

  1. 更新以下部分：

     ```yaml
     ## OmniAuth settings
     omniauth:
       # Allow sign-in by using Twitter, Google, etc. using OmniAuth providers
       # Versions prior to 11.4 require this to be set to true
       # enabled: true

       # CAUTION!
       # This allows users to sign in without having a user account first. Define the allowed providers
       # using an array, for example, ["github", "gitlab"], or as true/false to allow all providers or none.
       # User accounts will be created automatically when authentication was successful.
       allow_single_sign_on: ["github", "gitlab"]

       auto_link_ldap_user: true

       # Locks down those users until they have been cleared by the admin (default: true).
       block_auto_created_users: true
     ```

配置完这些设置后，您可以配置您选择的 [provider](#支持的提供商)。

<!--
### Passwords for users created via OmniAuth

The [Generated passwords for users created through integrated authentication](../security/passwords_for_integrated_authentication_methods.md)
guide provides an overview about how GitLab generates and sets passwords for
users created with OmniAuth.

## Enable OmniAuth for an Existing User

Existing users can enable OmniAuth for specific providers after the account is
created. For example, if the user originally signed in with LDAP, an OmniAuth
provider such as Twitter can be enabled. Follow the steps below to enable an
OmniAuth provider for an existing user.

1. Sign in normally - whether standard sign in, LDAP, or another OmniAuth provider.
1. In the top-right corner, select your avatar.
1. Select **Edit profile**.
1. On the left sidebar, select **Account**.
1. In the **Connected Accounts** section, select the desired OmniAuth provider, such as Twitter.
1. The user is redirected to the provider. After the user authorizes GitLab,
   they are redirected back to GitLab.

The chosen OmniAuth provider is now active and can be used to sign in to GitLab from then on.

## Enable or disable sign-in with an OmniAuth provider without disabling import sources

Administrators can enable or disable sign-in for some OmniAuth providers.

NOTE:
By default, sign-in is enabled for all the OAuth providers configured in `config/gitlab.yml`.

To enable or disable an OmniAuth provider:

1. On the top bar, select **Menu > Admin**.
1. On the left sidebar, select **Settings**.
1. Expand **Sign-in restrictions**.
1. In the **Enabled OAuth authentication sources** section, select or clear the checkbox for each provider you want to enable or disable.

## Disable OmniAuth

In GitLab 11.4 and later, OmniAuth is enabled by default. However, OmniAuth only works
if providers are configured and [enabled](#enable-or-disable-sign-in-with-an-omniauth-provider-without-disabling-import-sources).

If OmniAuth providers are causing problems even when individually disabled, you
can disable the entire OmniAuth subsystem by modifying the configuration file:

- **For Omnibus installations**

  ```ruby
  gitlab_rails['omniauth_enabled'] = false
  ```

- **For installations from source**:

  ```yaml
  omniauth:
    enabled: false
  ```

## Automatically Link Existing Users to OmniAuth Users

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/36664) in GitLab 13.4.

You can automatically link OmniAuth users with existing GitLab users if their email addresses match.
Automatic linking using this method works for all providers
[except the SAML provider](https://gitlab.com/gitlab-org/gitlab/-/issues/338293). For automatic
linking using the SAML provider, see [SAML-specific](saml.md#general-setup) instructions.

As an example, the following configuration is used to enable the auto link
feature for both an **OpenID Connect provider** and a **Twitter OAuth provider**.

- **For Omnibus installations**

  ```ruby
  gitlab_rails['omniauth_auto_link_user'] = ["openid_connect", "twitter"]
  ```

- **For installations from source**

  ```yaml
  omniauth:
    auto_link_user: ["openid_connect", "twitter"]
  ```

## Configure OmniAuth Providers as External

You can define which OmniAuth providers you want to be `external`. Users
creating accounts, or logging in by using these `external` providers cannot have
access to internal projects. You must use the full name of the provider,
like `google_oauth2` for Google. Refer to the examples for the full names of the
supported providers.

NOTE:
If you decide to remove an OmniAuth provider from the external providers list,
you must manually update the users that use this method to sign in if you want
their accounts to be upgraded to full internal accounts.

- **For Omnibus installations**

  ```ruby
  gitlab_rails['omniauth_external_providers'] = ['twitter', 'google_oauth2']
  ```

- **For installations from source**

  ```yaml
  omniauth:
    external_providers: ['twitter', 'google_oauth2']
  ```

## Using Custom OmniAuth Providers

NOTE:
The following information only applies for installations from source.

GitLab uses [OmniAuth](https://github.com/omniauth/omniauth) for authentication and already ships
with a few providers pre-installed, such as LDAP, GitHub, and Twitter. You may also
have to integrate with other authentication solutions. For
these cases, you can use the OmniAuth provider.

### Steps

These steps are fairly general and you must figure out the exact details
from the OmniAuth provider's documentation.

- Stop GitLab:

  ```shell
  sudo service gitlab stop
  ```

- Add the gem to your [`Gemfile`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/Gemfile):

  ```shell
  gem "omniauth-your-auth-provider"
  ```

- Install the new OmniAuth provider gem by running the following command:

  ```shell
  sudo -u git -H bundle install --without development test mysql --path vendor/bundle --no-deployment
  ```

  > These are the same commands you used during initial installation in the [Install Gems section](../install/installation.md#install-gems) with `--path vendor/bundle --no-deployment` instead of `--deployment`.

- Start GitLab:

  ```shell
  sudo service gitlab start
  ```

### Examples

If you have successfully set up a provider that is not shipped with GitLab itself,
please let us know.

While we can't officially support every possible authentication mechanism out there,
we'd like to at least help those with specific needs.

## Enable or disable Sign In with an OmniAuth provider without disabling import sources

Administrators are able to enable or disable **Sign In** by using some OmniAuth providers.

NOTE:
By default, **Sign In** is enabled by using all the OAuth Providers that have been configured in `config/gitlab.yml`.

To enable/disable an OmniAuth provider:

1. On the top bar, select **Menu > Admin**.
1. On the left sidebar, go to **Settings**.
1. Scroll to the **Sign-in Restrictions** section, and click **Expand**.
1. Below **Enabled OAuth Sign-In sources**, select the checkbox for each provider you want to enable or disable.

   ![Enabled OAuth Sign-In sources](img/enabled-oauth-sign-in-sources_v13_10.png)

## Disabling OmniAuth

Starting from version 11.4 of GitLab, OmniAuth is enabled by default. This only
has an effect if providers are configured and [enabled](#enable-or-disable-sign-in-with-an-omniauth-provider-without-disabling-import-sources).

If OmniAuth providers are causing problems even when individually disabled, you
can disable the entire OmniAuth subsystem by modifying the configuration file:

- **For Omnibus installations**

  ```ruby
  gitlab_rails['omniauth_enabled'] = false
  ```

- **For installations from source**:

  ```yaml
  omniauth:
    enabled: false
  ```

## Keep OmniAuth user profiles up to date

You can enable profile syncing from selected OmniAuth providers and for all or for specific user information.

When authenticating using LDAP, the user's name and email are always synced.

- **For Omnibus installations**

  ```ruby
  gitlab_rails['omniauth_sync_profile_from_provider'] = ['twitter', 'google_oauth2']
  gitlab_rails['omniauth_sync_profile_attributes'] = ['name', 'email', 'location']
  ```

- **For installations from source**

  ```yaml
  omniauth:
    sync_profile_from_provider: ['twitter', 'google_oauth2']
    sync_profile_attributes: ['email', 'location']
  ```

## Bypassing two factor authentication

In GitLab 12.3 or later, users can sign in with specified providers _without_
using two factor authentication.

Define the allowed providers using an array (for example, `["twitter", 'google_oauth2']`),
or as `true` or `false` to allow all providers (or none). This option should be
configured only for providers which already have two factor authentication
(default: false). This configuration doesn't apply to SAML.

- **For Omnibus package**

  ```ruby
  gitlab_rails['omniauth_allow_bypass_two_factor'] = ['twitter', 'google_oauth2']
  ```

- **For installations from source**

  ```yaml
  omniauth:
    allow_bypass_two_factor: ['twitter', 'google_oauth2']
  ```

## Automatically sign in with provider

You can add the `auto_sign_in_with_provider` setting to your GitLab
configuration to redirect login requests to your OmniAuth provider for
authentication. This removes the need to click a button before actually signing in.

For example, when using the [Azure v2 integration](azure.md#microsoft-azure-oauth-20-omniauth-provider-v2), set the following to enable auto
sign-in:

- **For Omnibus package**

  ```ruby
  gitlab_rails['omniauth_auto_sign_in_with_provider'] = 'azure_activedirectory_v2'
  ```

- **For installations from source**

  ```yaml
  omniauth:
    auto_sign_in_with_provider: azure_activedirectory_v2
  ```

Keep in mind that every sign-in attempt is redirected to the OmniAuth
provider; you can't sign in using local credentials. Ensure at least
one of the OmniAuth users has an administrator role.

You may also bypass the auto sign in feature by browsing to
`https://gitlab.example.com/users/sign_in?auto_sign_in=false`.

## Passwords for users created via OmniAuth

The [Generated passwords for users created through integrated authentication](../security/passwords_for_integrated_authentication_methods.md)
guide provides an overview about how GitLab generates and sets passwords for
users created with OmniAuth.

## Custom OmniAuth provider icon

Most supported providers include a built-in icon for the rendered sign-in button.
After you ensure your image is optimized for rendering at 64 x 64 pixels,
you can override this icon in one of two ways:

- **Provide a custom image path**:

  1. *If you are hosting the image outside of your GitLab server domain,* ensure
     your [content security policies](https://docs.gitlab.com/omnibus/settings/configuration.html#content-security-policy)
     are configured to allow access to the image file.
  1. Depending on your method of installing GitLab, add a custom `icon` parameter
     to your GitLab configuration file. Read [OpenID Connect OmniAuth provider](../administration/auth/oidc.md)
     for an example for the OpenID Connect provider.

- **Directly embed an image in a configuration file**: This example creates a Base64-encoded
  version of your image you can serve through a
  [Data URL](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs):

  1. Encode your image file with GNU `base64` command (such as `base64 -w 0 <logo.png>`)
     which returns a single-line `<base64-data>` string.
  1. Add the Base64-encoded data to a custom `icon` parameter in your GitLab
     configuration file:

     ```yaml
     omniauth:
       providers:
         - { name: '...'
             icon: 'data:image/png;base64,<base64-data>'
             ...
           }
     ```
-->
