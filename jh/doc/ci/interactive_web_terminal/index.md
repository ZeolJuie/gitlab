---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 交互式 web 终端 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/50144) in GitLab 11.3.
-->

交互式 Web 终端允许用户访问极狐GitLab 中的终端，以便为其 CI 流水线运行一次性命令。您可以将其视为使用 SSH 进行调试的一种方法，但直接从作业页面完成。由于这允许用户 shell 访问部署 GitLab Runner<!--[GitLab Runner](https://docs.gitlab.com/runner/)--> 的环境，因此有一些安全预防措施<!--[安全预防措施](../../administration/integration/terminal.md#security)-->被用来保护用户。

<!--
NOTE:
[Shared runners on GitLab.com](../runners/index.md) do not
provide an interactive web terminal. Follow [this
issue](https://gitlab.com/gitlab-org/gitlab/-/issues/24674) for progress on
adding support. For groups and projects hosted on GitLab.com, interactive web
terminals are available when using your own group or project runner.
-->

## 配置

要使交互式 Web 终端工作，需要配置：

- runner 需要 `[session_server]` 正确配置<!--[`[session_server]` 正确配置](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-session_server-section)-->
- 如果您在实例中使用反向代理，则需要启用 <!--[启用](../../administration/integration/terminal.md#enabling-and-disabling-terminal-support)--> web 终端

NOTE:
`gitlab-runner` Helm chart 尚不支持交互式 web 终端。

## 调试正在运行的作业

NOTE:
不是所有的 executor 都受支持<!--[支持](https://docs.gitlab.com/runner/executors/#compatibility-chart)-->。

NOTE:
构建脚本完成后，`docker` executor 不会继续运行。此时，终端会自动断开连接，不会等待用户完成。

有时，当一项作业正在运行时，事情不会像您预期的那样进行，如果可以有一个 shell 来帮助调试会很有帮助。当作业正在运行时，在右侧面板上您可以看到一个按钮 `debug`，用于打开当前作业的终端。

![Example of job running with terminal
available](img/interactive_web_terminal_running_job.png)

单击后，将打开一个新选项卡到终端页面，您可以在其中访问终端并像普通 shell 一样键入命令。

![terminal of the job](img/interactive_web_terminal_page.png)

如果您打开终端并且作业已完成其任务，则终端会在 `[session_server].session_timeout`<!--[`[session_server].session_timeout`](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-session_server-section)--> 中配置的持续时间内，阻止作业完成，直到您关闭终端窗口。

![finished job with terminal open](img/finished_job_with_terminal_open.png)

## 用于 Web IDE 的交互式 Web 终端

阅读 Web IDE 文档以了解如何运行[通过 Web IDE 的交互式终端](../../user/project/web_ide/index.md#用于-web-ide-的交互式-web-终端)。
