---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# GitLab CI/CD include 示例 **(FREE)**

您可以使用 [`include`](index.md#include) 在 CI/CD 作业中包含外部 YAML 文件。

## 包含单个配置文件

要包含单个配置文件，请使用以下任一语法选项：

- `include` 本身带有单个文件，与 [`include:local`](index.md#includelocal) 相同：

  ```yaml
  include: '/templates/.after-script-template.yml'
  ```

- `include` 与单个文件，并指定 `include` 类型：

  ```yaml
  include:
    remote: 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
  ```

## 包含一组配置文件

您可以包含一组配置文件：

- 如果不指定 `include` 类型，则类型默认为 [`include:local`](index.md#includelocal)：

  ```yaml
  include:
    - 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
    - '/templates/.after-script-template.yml'
  ```

- 您可以定义单个项目数组：

  ```yaml
  include:
    - remote: 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
  ```

- 您可以定义一个数组并明确指定多个 `include` 类型：

  ```yaml
  include:
    - remote: 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
    - local: '/templates/.after-script-template.yml'
    - template: Auto-DevOps.gitlab-ci.yml
  ```

- 您可以定义一个结合默认和特定 `include` 类型的数组：

  ```yaml
  include:
    - 'https://gitlab.com/awesome-project/raw/main/.before-script-template.yml'
    - '/templates/.after-script-template.yml'
    - template: Auto-DevOps.gitlab-ci.yml
    - project: 'my-group/my-project'
      ref: main
      file: '/templates/.gitlab-ci-template.yml'
  ```

## 使用包含的配置文件中的 `default` 配置

您可以在配置文件中定义 [`default`](index.md#default) 部分。当您使用带有 `include` 关键字的 `default` 部分时，默认值适用于流水线中的所有作业。

例如，您可以使用带有 [`before_script`](index.md#before_script) 的 `default` 部分。

名为 `/templates/.before-script-template.yml` 的自定义配置文件的内容：

```yaml
default:
  before_script:
    - apt-get update -qq && apt-get install -y -qq sqlite3 libsqlite3-dev nodejs
    - gem install bundler --no-document
    - bundle install --jobs $(nproc)  "${FLAGS[@]}"
```

`.gitlab-ci.yml` 的内容：

```yaml
include: '/templates/.before-script-template.yml'

rspec1:
  script:
    - bundle exec rspec

rspec2:
  script:
    - bundle exec rspec
```

默认的 `before_script` 命令在 `script` 命令之前在两个 `rspec` 作业中执行。

## 覆盖包含的配置值

当您使用 `include` 关键字时，您可以覆盖包含的配置值以使其适应您的流水线要求。

以下示例显示了在 `.gitlab-ci.yml` 文件中自定义的 `include` 文件。 特定的 YAML 定义的变量和 `production` 作业的详细信息被覆盖。

名为 `autodevops-template.yml` 的自定义配置文件的内容：

```yaml
variables:
  POSTGRES_USER: user
  POSTGRES_PASSWORD: testing_password
  POSTGRES_DB: $CI_ENVIRONMENT_SLUG

production:
  stage: production
  script:
    - install_dependencies
    - deploy
  environment:
    name: production
    url: https://$CI_PROJECT_PATH_SLUG.$KUBE_INGRESS_BASE_DOMAIN
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```

`.gitlab-ci.yml` 的内容：

```yaml
include: 'https://company.com/autodevops-template.yml'

image: alpine:latest

variables:
  POSTGRES_USER: root
  POSTGRES_PASSWORD: secure_password

stages:
  - build
  - test
  - production

production:
  environment:
    url: https://domain.com
```

`.gitlab-ci.yml` 文件中定义的 `POSTGRES_USER` 和 `POSTGRES_PASSWORD` 变量以及 `production` 作业的 `environment:url` 覆盖了 `autodevops-template.yml` 文件中定义的值。其他关键字不变。这种方法称为 *merging*。

## 覆盖包含的配置数组

您可以使用合并来扩展和覆盖包含模板中的配置，但不能添加或修改数组中的单个项目。例如，将额外的 `notify_owner` 命令添加到扩展的 `production` 作业的 `script` 数组中：

`autodevops-template.yml` 的内容：

```yaml
production:
  stage: production
  script:
    - install_dependencies
    - deploy
```

`.gitlab-ci.yml` 的内容：

```yaml
include: 'autodevops-template.yml'

stages:
  - production

production:
  script:
    - install_dependencies
    - deploy
    - notify_owner
```

如果 `.gitlab-ci.yml` 文件中没有重复 `install_dependencies` 和 `deploy`，那么 `production` 作业在脚本中将只有 `notify_owner`。

## 使用 nested includes

您可以在配置文件中嵌套 `include` 部分，然后将其包含在另一个配置中。例如，对于 `include` 关键字嵌套了三层深度：

`.gitlab-ci.yml` 的内容：

```yaml
include:
  - local: /.gitlab-ci/another-config.yml
```

`/.gitlab-ci/another-config.yml` 的内容：

```yaml
include:
  - local: /.gitlab-ci/config-defaults.yml
```

`/.gitlab-ci/config-defaults.yml` 的内容：

```yaml
default:
  after_script:
    - echo "Job complete."
```

## 将变量与 `include` 一起使用

> - 引入于 13.8 版本。
> - 功能标志移除于 13.9 版本
> - 对项目、群组和实例变量的支持添加于 14.2 版本。
> - 对流水线变量的支持添加于 14.5 版本。

在您的 `.gitlab-ci.yml` 文件的 `include` 部分，您可以使用：

- 项目变量
- 群组变量
- 实例变量
- 项目预定义变量
- 在 14.2 及更高版本，`$CI_COMMIT_REF_NAME` 预定义变量

  当在 `include` 中使用时，`CI_COMMIT_REF_NAME` 变量返回完整的 ref 路径，如 `refs/heads/branch-name`。在 `include:rules` 中，可能需要使用 `if: $CI_COMMIT_REF_NAME =~ /main/`（不是`== main`）。已在 GitLab 14.5 中解决。

在 14.5 及更高版本中，您还可以使用：

- 触发器变量
- 计划流水线变量
- 手动流水线运行变量
- 流水线预定义变量

  在创建流水线之前解析 YAML 文件，因此以下流水线预定义变量**不**可用：

  - `CI_PIPELINE_ID`
  - `CI_PIPELINE_URL`
  - `CI_PIPELINE_IID`
  - `CI_PIPELINE_CREATED_AT`

示例：

```yaml
include:
  project: '$CI_PROJECT_PATH'
  file: '.compliance-gitlab-ci.yml'
```

<!--
For an example of how you can include these predefined variables, and the variables' impact on CI/CD jobs,
see this [CI/CD variable demo](https://youtu.be/4XR8gw3Pkos).
-->

## Use `rules` with `include`

> - 引入于 14.2 版本，在名为 `ci_include_rules` 的功能标志后默认禁用。
> - 适用于自助管理版于 14.3 版本。
> - 功能标志 `ci_include_rules` 移除于 14.4 版本。
> - `exists` 关键字支持添加于 14.5 版本。

您可以使用 [`rules`](index.md#rules) 和 `include` 来有条件地包含其他配置文件。

您只能将以下规则与 `include` 一起使用（并且只能与[某些变量](#将变量与-include-一起使用)一起使用）：

- [`if` 规则](index.md#rulesif)。例如：

  ```yaml
  include:
    - local: builds.yml
      rules:
        - if: '$INCLUDE_BUILDS == "true"'
    - local: deploys.yml
      rules:
        - if: $CI_COMMIT_BRANCH == "main"

  test:
    stage: test
    script: exit 0
  ```

- [`exists` 规则](index.md#rulesexists)。 例如：

  ```yaml
  include:
    - local: builds.yml
      rules:
        - exists:
            - file.md

  test:
    stage: test
    script: exit 0
  ```

不支持 `rules` 关键字 `changes`。

## 将 `include:local` 与通配符文件路径一起使用

> - 引入于 13.11 版本。
> - 功能标志移除于 14.2 版本。

您可以在 `include:local` 中使用通配符路径（`*` 和 `**`）。

例子：

```yaml
include: 'configs/*.yml'
```

当流水线运行时，系统：

- 将 `configs` 目录中的所有 `.yml` 文件添加到流水线配置中。
- 不在 `configs` 目录的子文件夹中添加 `.yml` 文件。 为此，请添加以下配置：

  ```yaml
  # This matches all `.yml` files in `configs` and any subfolder in it.
  include: 'configs/**.yml'

  # This matches all `.yml` files only in subfolders of `configs`.
  include: 'configs/**/*.yml'
  ```
