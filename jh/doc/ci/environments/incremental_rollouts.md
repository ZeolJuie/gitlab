---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: concepts, howto
---

# 使用 GitLab CI/CD 增量部署 **(FREE)**

在对应用程序进行更改时，可以将生产更改仅发布到 Kubernetes pod 的一部分，作为风险缓解策略。通过逐步发布生产变更，可以监控错误率或性能下降，如果没有问题，可以更新所有 Pod。

系统支持使用增量部署手动触发和定时部署到 Kubernetes 生产系统。使用 Manual Rollouts 时，每批 pod 的发布都是手动触发的，而在 Timed Rollouts 中，发布是在默认暂停 5 分钟后分批执行的。
也可以在暂停期到期之前手动触发定时推出。

手动和定时部署自动包含在由 <!--[Auto DevOps](../../topics/autodevops/index.md)-->Auto DevOps 控制的项目中，但它们也可以通过 GitLab CI/CD 在 `.gitlab-ci.yml` 配置文件中进行配置。

手动触发的发布可以使用您的持续交付<!--[持续交付](../introduction/index.md#continuous-delivery)-->方法来实施，而定时发布不需要干预并且可以成为您的持续部署<!--[持续部署](../introduction/index.md#continuous-deployment)-->策略。
您还可以通过自动部署应用程序的方式将两者结合起来，除非您最终在必要时手动干预。

<!--
We created sample applications to demonstrate the three options, which you can
use as examples to build your own:

- [Manual incremental rollouts](https://gitlab.com/gl-release/incremental-rollout-example/blob/master/.gitlab-ci.yml)
- [Timed incremental rollouts](https://gitlab.com/gl-release/timed-rollout-example/blob/master/.gitlab-ci.yml)
- [Both manual and timed rollouts](https://gitlab.com/gl-release/incremental-timed-rollout-example/blob/master/.gitlab-ci.yml)
-->

## 手动发布

可以将极狐GitLab 配置为通过 `.gitlab-ci.yml` 手动进行增量部署。手动配置允许更多地控制此功能。增量部署的步骤取决于为部署定义的 Pod 数量，这些数量是在创建 Kubernetes 集群时配置的。

例如，如果您的应用程序有 10 个 Pod，并且运行了 10% 的 rollout 作业，则应用程序的新实例将部署到单个 Pod，而其余 Pod 显示应用程序的前一个实例。

首先我们[将模板定义为手动](https://gitlab.com/gl-release/incremental-rollout-example/blob/master/.gitlab-ci.yml#L100-103)：

```yaml
.manual_rollout_template: &manual_rollout_template
  <<: *rollout_template
  stage: production
  when: manual
```

然后我们[定义每一步的发布量](https://gitlab.com/gl-release/incremental-rollout-example/blob/master/.gitlab-ci.yml#L152-155)：

```yaml
rollout 10%:
  <<: *manual_rollout_template
  variables:
    ROLLOUT_PERCENTAGE: 10
```

构建作业后，作业名称旁边会出现一个 **运行** 按钮。单击 **运行** 按钮以释放每个阶段的 pod。您还可以通过运行较低百分比的作业来回滚。一旦达到 100%，您将无法使用此方法回滚。仍然可以通过使用环境页面上的 **回滚** 按钮重新部署旧版本来回滚。

![Play button](img/incremental_rollouts_play_v12_7.png)

<!--
A [deployable application](https://gitlab.com/gl-release/incremental-rollout-example) is
available, demonstrating manually triggered incremental rollouts.
-->

## 定时发布

定时发布的行为方式与手动部署相同，不同之处在于每个作业在部署之前定义为延迟数分钟。单击作业会显示倒计时。

![Timed rollout](img/timed_rollout_v12_7.png)

可以将此功能与手动增量部署相结合，以便作业倒计时然后部署。

首先我们[将模板定义为定时](https://gitlab.com/gl-release/timed-rollout-example/blob/master/.gitlab-ci.yml#L86-89)：

```yaml
.timed_rollout_template: &timed_rollout_template
  <<: *rollout_template
  when: delayed
  start_in: 1 minutes
```

我们可以使用 start_in 键定义延迟时间：

```yaml
start_in: 1 minutes
```

然后我们[定义每一步的发布量](https://gitlab.com/gl-release/timed-rollout-example/blob/master/.gitlab-ci.yml#L97-101)：

```yaml
timed rollout 30%:
  <<: *timed_rollout_template
  stage: timed rollout 30%
  variables:
    ROLLOUT_PERCENTAGE: 30
```

<!--
A [deployable application](https://gitlab.com/gl-release/timed-rollout-example) is
available, [demonstrating configuration of timed rollouts](https://gitlab.com/gl-release/timed-rollout-example/blob/master/.gitlab-ci.yml#L86-95).
-->

## 蓝绿部署

NOTE:
从 13.7 开始，团队可以利用 Ingress annotation 和设置流量权重<!--[设置流量权重](../../user/project/canary_deployments.md#how-to-change-the-traffic-weight-on-a-canary-ingress)-->作为此处记录的蓝绿部署策略的替代方法。

有时也称为 A/B 部署或红黑部署，此技术用于减少部署期间的停机时间和风险。与增量部署结合使用时，您可以将导致问题的部署的影响降至最低。

使用这种技术有两种部署（“蓝色”和“绿色”，但可以使用任何命名）。
在任何给定时间，只有其中一个部署处于活动状态，增量部署期间除外。

例如，您的蓝色部署当前可以在生产中处于活动状态，而绿色部署是“实时”用于测试，但未部署到生产中。如果发现问题，可以在不影响生产部署的情况下更新绿色部署（当前为蓝色）。如果测试没有发现问题，您将生产切换到绿色部署，现在蓝色可用于测试下一个版本。

此过程减少了停机时间，因为无需关闭生产部署即可切换到不同的部署。两个部署并行运行，可以随时切换。

<!--
An [example deployable application](https://gitlab.com/gl-release/blue-green-example)
is available, with a [`.gitlab-ci.yml` CI/CD configuration file](https://gitlab.com/gl-release/blue-green-example/blob/master/.gitlab-ci.yml)
that demonstrates blue-green deployments.
-->