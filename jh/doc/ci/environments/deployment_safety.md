---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 部署安全 **(FREE)**

部署作业可能比流水线中的其他作业更敏感，可能需要格外小心。极狐GitLab 具有多项有助于维护部署安全性和稳定性的功能。

您可以：

- [限制对关键环境的写访问](#限制对关键环境的写访问)
- [在部署冻结窗口期间阻止部署](#在部署冻结窗口期间阻止部署)
- [为您的项目设置适当的角色](#为您的项目设置适当的角色)
- [保护生产 secrets](#保护生产-secrets)
- [用于部署的单独项目](#用于部署的单独项目)

如果您使用的是持续部署工作流，并希望确保不会发生对同一环境的并发部署，则应启用以下选项：

- [确保一次只运行一个部署作业](#确保一次只运行一个部署作业)
- [跳过过期的部署作业](#跳过过期的部署作业)

## 限制对关键环境的写访问

默认情况下，任何至少具有开发者角色的团队成员都可以修改环境。
如果您想限制对关键环境（例如“生产”环境）的写访问，您可以设置[受保护的环境](protected_environments.md)。

## 确保一次只运行一个部署作业

GitLab CI/CD 中的流水线作业并行运行，因此两个不同流水线中的两个部署作业可能会尝试同时部署到同一环境。这不是预期的行为，因为部署应该按顺序进行。

您可以使用 `.gitlab-ci.yml` 中的 [`resource_group` 关键字](../yaml/index.md#resource_group) 确保一次只运行一个部署作业。

例如：

```yaml
deploy:
 script: deploy-to-prod
 resource_group: prod
```

使用资源组**之前**，有问题的流水线工作流示例：

1. Pipeline-A 中的 `deploy` 作业开始运行。
1. Pipeline-B 中的 `deploy` 作业开始运行。*这是可能导致意外结果的并发部署。*
1. Pipeline-A 中的 `deploy` 作业完成。
1. Pipeline-B 中的 `deploy` 作业完成。

使用资源组**之后**，改进的流水线工作流示例：

1. Pipeline-A 中的 `deploy` 作业开始运行。
1. Pipeline-B 中的 `deploy` 作业尝试启动，但等待第一个 `deploy` 作业完成。
1. Pipeline-A 中的 `deploy` 作业完成。
1. Pipeline-B 中的 `deploy` 作业开始运行。

<!--
For more information, see [Resource Group documentation](../resource_groups/index.md).
-->

## 跳过过期的部署作业

流水线作业的执行顺序可能因运行而异，这可能会导致意外。例如，较新流水线中的部署作业可能会在较旧流水线中的部署作业之前完成。
这会造成竞争的情况，较旧的部署稍后完成，覆盖“较新的”部署。

您可以通过启用[跳过过期的部署作业](../pipelines/settings.md#跳过过期的部署作业)功能，来确保在运行较新的部署时自动取消较旧的部署作业。

启用跳过过时的部署作业**之前**，有问题的流水线工作流示例：

1. Pipeline-A 是在默认分支上创建的。
1. 稍后，在默认分支上创建了 Pipeline-B（使用更新的提交 SHA）。
1. Pipeline-B 中的 `deploy` 作业首先完成，并部署较新的代码。
1. Pipeline-A 中的 `deploy` 作业稍后完成，并部署旧代码，**覆盖**较新（最新）的部署。

启用跳过过时的部署作业**之后**，改进的流水线工作流示例：

1. Pipeline-A 是在默认分支上创建的。
1. 稍后，在默认分支上创建了 Pipeline-B（使用更新的 SHA）。
1. Pipeline-B 中的 `deploy` 作业首先完成，并部署较新的代码。
1. Pipeline-A 中的 `deploy` 作业被自动取消，因此它不会覆盖来自较新流水线的部署。

## 在部署冻结窗口期间阻止部署

如果您想在特定时期阻止部署，例如在大多数员工外出的计划假期期间，您可以设置部署冻结<!--[部署冻结](../../user/project/releases/index.md#prevent-unintentional-releases-by-setting-a-deploy-freeze)-->。
在部署冻结期间，不能执行任何部署。这有助于确保部署不会意外发生。
  
## 为您的项目设置适当的角色

极狐GitLab 支持多种不同的角色，这些角色可以分配给您的项目成员。<!--请参阅 [项目成员权限](../../user/permissions.md#project-members-permissions) 了解这些角色和每个角色的权限。-->

<!--
<div class="video-fallback">
  See the video: <a href="https://www.youtube.com/watch?v=Mq3C1KveDc0">How to secure your CD pipelines</a>.
</div>
<figure class="video-container">
  <iframe src="https://www.youtube.com/embed/Mq3C1KveDc0" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
-->

## 保护生产 secrets

成功部署需要生产 secrets。例如，在部署到云时，云提供商需要这些 secrets 才能连接到他们的服务。在项目设置中，您可以为这些 secrets 定义和保护 CI/CD 变量。[受保护的变量](../variables/index.md#保护-cicd-变量)仅传递到[受保护分支](../../user/project/protected_branches.md)或[受保护的标签](../../user/project/protected_tags.md)。
其他流水线没有得到受保护的变量。您还可以[将变量作用于特定环境](../variables/where_variables_can_be_used.md#具有环境范围的变量)。
我们建议您在受保护的环境中使用受保护的变量，以确保不会无意中泄露  secrets。您还可以在 runner 端<!--[runner 端](../runners/configure_runners.md#prevent-runners-from-revealing-sensitive-information)--> 定义生产 secrets。
这可以防止具有维护者角色的其他用户读取 secrets，并确保 runner 仅在受保护的分支上运行。

<!--
For more information, see [pipeline security](../pipelines/index.md#pipeline-security-on-protected-branches).
-->

## 用于部署的单独项目

具有项目维护者角色的所有用户都可以访问生产 secrets。如果需要限制可以部署到生产环境的用户数量，可以创建一个单独的项目，并配置一个新的权限模型，将 CD 权限与原始项目隔离，阻止具有项目维护者角色的原始用户访问生产 secrets 和 CD 配置。您可以使用[多项目流水线](../pipelines/multi_project_pipelines.md) 将 CD 项目连接到您的开发项目。

## 保护 `gitlab-ci.yml` 免受更改

`.gitlab-ci.yml` 可能包含将应用程序部署到生产服务器的规则。此部署通常在推送合并请求后自动运行。为了防止开发人员更改 `.gitlab-ci.yml`，您可以在不同的仓库中定义它。配置可以引用具有完全不同权限集的另一个项目中的文件（类似于[区分部署项目](#用于部署的单独项目)）。
在这种情况下，`.gitlab-ci.yml` 是可公开访问的，但只能由其他项目中具有适当权限的用户进行编辑。

<!--
For more information, see [Custom CI/CD configuration path](../pipelines/settings.md#specify-a-custom-cicd-configuration-file).
-->

## 故障排查

### 流水线作业失败，并显示 `The deployment job is older than the previously succeeded deployment job...`

这是由[跳过过期的部署作业](../pipelines/settings.md#跳过过期的部署作业)功能引起的。
如果同一个环境有多个作业（包括非部署作业），可能会遇到这个问题，例如：

```yaml
build:service-a:
 environment:
   name: production

build:service-b:
 environment:
   name: production
```

[跳过过期的部署作业](../pipelines/settings.md#跳过过期的部署作业)可能不适用于此配置，必须禁用。
