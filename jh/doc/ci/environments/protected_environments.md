---
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 受保护环境 **(PREMIUM)**

[环境](../environments/index.md) 可用于测试和生产原因。

由于部署作业可以由具有不同角色的不同用户发起，因此能够保护特定环境免受未经授权用户的影响非常重要。

默认情况下，受保护的环境确保只有具有适当权限的人才能部署到它，从而保证环境安全。

NOTE:
GitLab 管理员可以使用所有环境，包括受保护的环境。

要保护、更新或取消保护环境，您至少需要具有维护者角色。

## 保护环境

要保护环境：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **受保护的环境**。
1. 从 **环境** 列表中，选择您要保护的环境。
1. 在 **允许部署** 列表中，选择您要为其授予部署访问权限的角色、用户或群组。请记住：
    - 有两种角色可供选择：
      - **维护者**：允许访问所有具有维护者角色的项目用户。
      - **开发者**：允许访问所有具有维护者和开发者角色的项目用户。
    - 您只能选择已经与项目关联的群组。
    - 用户必须至少具有开发人员角色才能出现在 **允许部署** 列表中。
1. 选择 **保护**。

受保护环境现在出现在受保护环境列表中。

### 使用 API 保护环境

或者，您可以使用 API 来保护环境：

1. 使用带有 CI 的项目创建环境。例如：

   ```yaml
   stages:
     - test
     - deploy

   test:
     stage: test
     script:
       - 'echo "Testing Application: ${CI_PROJECT_NAME}"'

   production:
     stage: deploy
     when: manual
     script:
       - 'echo "Deploying to ${CI_ENVIRONMENT_NAME}"'
     environment:
       name: ${CI_JOB_NAME}
   ```

1. 使用 UI [创建新群组](../../user/group/index.md#创建一个群组)。例如，该群组称为 `protected-access-group`，其群组 ID 为`9899826`。请注意，这些步骤中的其余示例均使用此群组。

   ![Group Access](img/protected_access_group_v13_6.png)

1. 使用 API 将用户作为报告者添加到群组中：

   ```shell
   $ curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
          --data "user_id=3222377&access_level=20" "https://gitlab.com/api/v4/groups/9899826/members"

   {"id":3222377,"name":"Sean Carroll","username":"sfcarroll","state":"active","avatar_url":"https://gitlab.com/uploads/-/system/user/avatar/3222377/avatar.png","web_url":"https://gitlab.com/sfcarroll","access_level":20,"created_at":"2020-10-26T17:37:50.309Z","expires_at":null}
   ```

1. 使用 API 将群组作为报告者添加到项目中：

   ```shell
   $ curl --request POST --header "PRIVATE-TOKEN: <your_access_token>" \
          --request POST "https://gitlab.com/api/v4/projects/22034114/share?group_id=9899826&group_access=20"

   {"id":1233335,"project_id":22034114,"group_id":9899826,"group_access":20,"expires_at":null}
   ```

1. 使用 API 添加具有受保护环境访问权限的群组：

   ```shell
   curl --header 'Content-Type: application/json' --request POST --data '{"name": "production", "deploy_access_levels": [{"group_id": 9899826}]}' \
        --header "PRIVATE-TOKEN: <your_access_token>" "https://gitlab.com/api/v4/projects/22034114/protected_environments"
   ```

该群组现在可以访问并且可以在 UI 中看到。

## 按群组成员身份访问环境

作为[群组成员资格](../../user/group/index.md)的一部分，用户可以被授予访问受保护环境的权限。具有报告者角色的用户只能使用此方法被授予访问受保护环境的权限。

## 部署分支访问

具有开发者角色的用户可以通过以下任一方法获得对受保护环境的访问权限：

- 作为个人贡献者，通过角色。
- 通过群组成员身份。

如果用户还拥有对生产部署的分支的推送或合并访问权限，则他们具有以下权限：

- [停止环境](index.md#停止环境)。
- [删除停止的环境](index.md#删除一个停止的环境).
- 创建环境终端<!--[创建环境终端](index.md#web-terminals-deprecated).-->

## 对受保护环境的仅限部署访问

被授予对受保护环境的访问权限的用户，但不能推送或合并对其部署的分支，他们仅被授予部署环境的访问权限。具有报告者角色的群组中，或添加到具有报告者角色的群组中的个人，出现在下拉菜单中，仅用于部署访问。

添加仅限部署的访问：

1. 添加一个具有报告者角色的群组。
1. 将用户添加到群组中。
1. 邀请群组成为项目成员。
1. 按照[保护环境](#保护环境)中的步骤进行操作。

请注意，仅部署访问权限是具有报告者角色的群组的唯一可能访问级别。

## 修改和取消保护环境

维护者可以：

- 通过更改 **允许部署** 下拉菜单中的访问权限，随时更新现有的受保护环境。
- 通过单击该环境的 **取消保护** 按钮来取消保护受保护的环境。

环境取消保护后，所有访问条目都将被删除，如果环境被重新保护，则必须重新输入。

更多信息请参见[部署安全](deployment_safety.md)。

## 群组级受保护环境

> - 引入于 14.0 版本，功能标志名为 `group_level_protected_environments`。默认禁用。
> - 功能标志移除于 14.3 版本。
> - 普遍适用于 14.3 版本。

通常，大型企业组织在开发人员和运维人员<!--[开发人员和运维人员](https://about.gitlab.com/topics/devops/)-->之间有明确的权限边界。
开发人员构建和测试他们的代码，运维人员部署和监控应用程序。在群组级保护环境中，每个群组的权限都经过仔细配置，以防止未经授权的访问并保持适当的职责分离。群组级受保护环境将[项目级受保护环境](#保护环境)扩展到群组级。

部署权限如下表所示：

| 环境 | 开发人员  | 运维人员 | 类别 |
|-------------|------------|----------|----------|
| Development | 允许    | 允许 | Lower environment  |
| Testing     | 允许    | 允许  | Lower environment  |
| Staging     | 不允许 | 允许  | Higher environment |
| Production  | 不允许 | 允许  | Higher environment |

<!--
_(Reference: [Deployment environments on Wikipedia](https://en.wikipedia.org/wiki/Deployment_environment))_
-->

### 群组级受保护环境名称

与项目级受保护环境相反，群组级受保护环境使用[部署级别](index.md#环境部署级别)作为其名称。

一个群组可能由许多具有唯一名称的项目环境组成。
例如，Project-A 有一个 `gprd` 环境，Project-B 有一个 `Production` 环境，因此保护特定的环境名称不能很好地扩展。
通过使用部署级别，两者都被识别为 `production` 部署级别并同时受到保护。

### 配置群组级成员资格

在一个企业组织中，在一个群组下有数千个项目，确保所有[项目级受保护环境](#保护环境)都得到正确配置并不是一个可扩展的解决方案。例如，当开发人员被授予新项目的维护者角色时，他们可能会获得对更高级别环境的特权访问。在这种情况下，群组级受保护环境可能是一种解决方案。

为了最大化组级受保护环境的有效性，必须正确配置[群组级成员资格](../../user/group/index.md)：

- 运维人员至少应该被赋予顶级群组的维护者角色。他们可以在群组级设置页面中维护更高级别环境（例如生产）的 CI/CD 配置，其中包括群组级受保护环境，群组级 runner<!--[群组级 runner](../runners/runners_scope.md#group-runners)--> 和群组级集群<!--[群组级集群](../../user/group/clusters/index.md)-->。这些配置作为只读条目继承到子项目。确保只有运维人员可以配置组织范围的部署规则集。
- 开发人员不应被授予不超过顶级群组的开发者角色，或明确授予用于子项目的维护者角色。他们*无权*访问顶级群组中的 CI/CD 配置，这样运维人员可以确保关键配置不会被开发人员意外更改。
- 对于子组和子项目：
  - 关于[子组](../../user/group/subgroups/index.md)，如果上级群组配置了群组级保护环境，下级组不能覆盖。
  - [项目级受保护环境](#保护环境)可以结合群组级设置。如果群组级别和项目级别的环境配置都存在，要运行部署作业，必须在**两个**规则集中允许用户。
  - 在一个项目或顶级群组的子组中，可以安全地为开发人员分配维护者角色<!--[维护者角色](../../user/permissions.md)-->，允许调整他们的较低级别环境（例如`testing`）。

有了这个配置：

- 如果用户将要在项目中运行部署作业并允许部署到环境中，则部署作业继续进行。
- 如果用户将要在项目中运行部署作业但不允许部署到环境中，则部署作业将失败并显示错误消息。

### 保护群组级环境

保护群组级环境：

1. 确保您的环境具有在 `.gitlab-ci.yml` 中定义的正确 [`deployment_tier`](index.md#环境部署级别)。
1. 使用 REST API<!--[REST API](../../api/group_protected_environments.md)--> 配置群组级受保护环境。

<!--
NOTE:
Configuration [with the UI](https://gitlab.com/gitlab-org/gitlab/-/issues/325249)
is scheduled for a later release.
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
