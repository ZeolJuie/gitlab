---
stage: Verify
group: Runner
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 使用极狐GitLab 作为微服务 **(FREE)**

许多应用程序需要访问 JSON API，因此应用程序测试可能也需要访问 API。以下示例展示了如何使用极狐GitLab 作为微服务，来为测试提供对 GitLab API 的访问权限。

1. 使用 Docker 或 Kubernetes executor 配置 runner<!--[runner](../runners/index.md)-->。
1. 在 `.gitlab-ci.yml` 中添加：

   ```yaml
   services:
     - name: gitlab/gitlab-ce:latest
       alias: gitlab

   variables:
     GITLAB_HTTPS: "false"             # ensure that plain http works
     GITLAB_ROOT_PASSWORD: "password"  # to access the api with user root:password
   ```

NOTE:
GitLab UI 中设置的变量不会传递到服务容器。[了解更多](../variables/index.md#)。

然后，`.gitlab-ci.yml` 文件中 `script` 部分中的命令可以访问 `http://gitlab/api/v4` 上的 API。

有关为何将 `gitlab` 用于 `Host` 的更多信息，请参阅[服务如何链接到作业](../docker/using_docker_images.md#扩展的-docker-配置选项)。

<!--
您还可以使用 [Docker Hub](https://hub.docker.com/u/gitlab) 上提供的任何其他 Docker 镜像。
-->

`gitlab` 镜像可以接受环境变量。有关更多详细信息，请参阅 [Omnibus 文档](../../install/index.md)。
