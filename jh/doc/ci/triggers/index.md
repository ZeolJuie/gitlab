---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: tutorial
---

# 使用 API 触发流水线 **(FREE)**

要为特定分支或标签触发流水线，您可以使用对流水线触发器 API 端点<!--[流水线触发器 API 端点](../../api/pipeline_triggers.md)-->的 API 调用。

使用 API 进行身份验证时，您可以使用：

- [触发器令牌](#创建触发器令牌) 触发分支或标签流水线。
- [CI/CD 作业令牌](../jobs/ci_job_token.md) 触发[多项目流水线](../pipelines/multi_project_pipelines.md#使用-api-创建多项目流水线)。

## 创建触发器令牌

您可以通过生成触发器令牌，并使用它来验证 API 调用来触发分支或标签的流水线。令牌模拟用户的项目访问和权限。

先决条件：

- 您必须至少具有该项目的维护者角色。

创建触发器令牌：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线触发器**。
1. 输入描述并选择 **添加触发器**。
   - 您可以查看和复制您创建的所有触发器的完整令牌。
   - 您只能看到其他项目成员创建的令牌的前 4 个字符。

WARNING:
在公共项目中以纯文本形式保存令牌存在安全风险。潜在的攻击者可以使用在 `.gitlab-ci.yml` 文件中公开的触发器令牌来模拟创建令牌的用户。使用[隐藏 CI/CD 变量](../variables/index.md#隐藏-cicd-变量)来提高触发器令牌的安全性。

## 触发流水线

在您[创建一个触发器令牌](#创建触发器令牌)之后，您可以使用它，通过一个可以访问 API 的工具或一个 webhook 来触发流水线。

### 使用 cURL

您可以使用 cURL 通过流水线触发器 API 端点<!--[pipeline triggers API 端点](../../api/pipeline_triggers.md)-->来触发流水线。
例如：

- 使用多行 cURL 命令：

  ```shell
  curl --request POST \
       --form token=<token> \
       --formref=<ref_name> \
       "https://gitlab.example.com/api/v4/projects/<project_id>/trigger/pipeline"
  ```

- 使用 cURL 并在查询字符串中传递 `<token>` 和 `<ref_name>`：

  ```shell
  curl --request POST \
      "https://gitlab.example.com/api/v4/projects/<project_id>/trigger/pipeline?token=<token>&ref=<ref_name>"
  ```

在示例中，替换：

- 使用您的实例的 URL。
- `<token>`：使用您的触发器令牌。
- `<ref_name>`：分支或标签名称，如 `main`。
- `<project_id>`：您的项目 ID，比如 `123456`。项目 ID 显示在每个项目的登录页面的顶部。

### 使用 CI/CD 作业

您可以使用带有触发器令牌的 CI/CD 作业，在另一个流水线运行时触发流水线。

例如，要在 `project-A` 中创建标签时，触发 `project-B` 的 `main` 分支上的流水线，请将以下作业添加到项目 A 的 `.gitlab-ci.yml` 文件中：

```yaml
trigger_pipeline:
  stage: deploy
  script:
    - 'curl --fail --request POST --form token=$MY_TRIGGER_TOKEN --form ref=main "https://gitlab.example.com/api/v4/projects/123456/trigger/pipeline"'
  rules:
    - if: $CI_COMMIT_TAG
```

在此例中：

- `1234` 是 `project-B` 的项目 ID。项目 ID 显示在每个项目的登录页面的顶部。
- [`rules`](../yaml/index.md#rules) 导致作业在每次将标签添加到 `project-A` 时运行。
- `MY_TRIGGER_TOKEN` 是一个[隐藏 CI/CD 变量](../variables/index.md#隐藏-cicd-变量)，包含触发器令牌。

### 使用 webhook

要从另一个项目的 webhook 触发流水线，请对推送和标签事件使用如下所示的 webhook URL：

```plaintext
https://gitlab.example.com/api/v4/projects/9/ref/main/trigger/pipeline?token=TOKEN
```

在示例中，替换：

- 使用您的实例的 URL。
- `<token>`：使用您的触发器令牌。
- `<ref_name>`：分支或标签名称，如 `main`。
- `<project_id>`：您的项目 ID，比如 `123456`。项目 ID 显示在每个项目的登录页面的顶部。

URL 中的 `ref` 优先于 webhook 负载中的 `ref`。有效负载 `ref` 是在源仓库中触发触发器的分支。
如果 `ref` 包含斜杠，则必须对其进行 URL 编码。

#### 使用 webhook 负载

> - 引入于 13.9 版本。
> - 功能标志移除于 13.11 版本。

如果您使用 webhook 触发流水线，则可以使用 `TRIGGER_PAYLOAD` [预定义 CI/CD 变量](../variables/predefined_variables.md) 访问 webhook 负载。
负载作为[文件类型变量](../variables/index.md#cicd-变量类型)公开，因此您可以使用 `cat $TRIGGER_PAYLOAD` 或类似命令访问数据。

### 在 API 调用中传递 CI/CD 变量

您可以在触发器 API 调用中传递任意数量的 [CI/CD 变量](../variables/index.md)。
这些变量具有[最高优先级](../variables/index.md#cicd-变量优先级)，并覆盖所有具有相同名称的变量。

参数的形式为 `variables[key]=value`，例如：

```shell
curl --request POST \
  --form token=TOKEN \
  --form ref=main \
  --form "variables[UPLOAD_TO_S3]=true" \
  "https://gitlab.example.com/api/v4/projects/123456/trigger/pipeline"
```

触发流水线中的 CI/CD 变量显示在每个作业的页面上，但只有拥有所有者和维护者角色的用户才能查看这些值。

![Job variables in UI](img/trigger_variables.png)

## 撤销触发器令牌

撤销触发器令牌：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线触发器**。
1. 在要撤销的触发器令牌的左侧，选择 **撤销** (**{remove}**)。

被撤销的触发器令牌不能被添加回来。

## 配置 CI/CD 作业以在触发的流水线中运行

在触发的流水线中[配置何时运行作业](../jobs/job_control.md)：

- 使用 [`rules`](../yaml/index.md#rules) 和 `$CI_PIPELINE_SOURCE` [预定义 CI/CD 变量](../variables/predefined_variables.md)。
- 使用 [`only`/`except`](../yaml/index.md#onlyrefs--exceptrefs) 关键字。

| `$CI_PIPELINE_SOURCE` 值 | `only`/`except` 关键字 | 触发方式      |
|-----------------------------|--------------------------|---------------------|
| `trigger`                   | `triggers`               | 在使用[触发器令牌](#创建触发器令牌)，并由流水线触发 API<!--[流水线触发 API](../../api/pipeline_triggers.md)--> 触发的流水线中。 |
| `pipeline`                  | `pipelines`              | 在[多项目流水线](../pipelines/multi_project_pipelines.md#使用-api-创建多项目流水线)中使用[流水线触发器 API](../../api/pipeline_triggers.md) 触发，并在 CI/CD 配置文件通过[`$CI_JOB_TOKEN`](../jobs/ci_job_token.md)，或使用 [`trigger`](../yaml/index.md#trigger) 关键字。 |

此外，`$CI_PIPELINE_TRIGGERED` 预定义的 CI/CD 变量在由触发器令牌触发的流水线中设置为 `true`。

## 查看使用了哪个触发器令牌

您可以通过访问单个作业页面来查看哪个触发器导致作业运行。
触发器令牌的一部分显示在页面右侧的作业详细信息下方：

![Marked as triggered on a single job page](img/trigger_single_job.png)

在使用触发器令牌触发的流水线中，作业在 **CI/CD > 作业** 中被标记为 `triggered`。

## 故障排查

### 触发流水线时 `404 not found`

触发流水线时 `{"message":"404 Not Found"}` 的响应可能是由于使用[个人访问令牌](../../user/profile/personal_access_tokens.md)引起的，而不是触发器令牌。[创建新的触发器令牌](#创建触发器令牌) 并使用它代替个人访问令牌。
