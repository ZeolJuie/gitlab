---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 流水线编辑器 **(FREE)**

> - 引入于 13.8 版本
> - 功能标志移除于 13.10 版本

流水线编辑器是在仓库根目录的 `.gitlab-ci.yml` 文件中编辑 GitLab CI/CD 配置的主要位置。要访问编辑器，请转到 **CI/CD > 编辑器**。

从流水线编辑器页面，您可以：

- 选择要工作的分支。
- [验证](#验证-ci-配置)编辑文件时的配置语法。
- 对您的配置进行更深入的 [lint](#lint-ci-配置)，使用添加了 [`include`](../yaml/index.md#include) 关键字的任何配置对其进行验证。
- 查看当前配置的[可视化](#可视化-ci-配置)。
- 查看配置的[扩展](#查看扩展配置)版本。
- [提交](#提交对-ci-配置的更改)对特定分支的更改。

在 13.9 及更早版本中，您必须在项目的默认分支上已经有 [`.gitlab-ci.yml` 文件](../quick_start/index.md#创建-gitlab-ciyml-文件)。

## 验证 CI 配置

当您编辑流水线配置时，它会根据 GitLab CI/CD 流水线架构不断进行验证。它检查您的 CI YAML 配置的语法，并运行一些基本的逻辑验证。

此验证的结果显示在编辑器页面的顶部。如果您的配置无效，则会显示一条提示以帮助您解决问题：

![Errors in a CI configuration validation](img/pipeline_editor_validate_v13_8.png)

## Lint CI 配置

要在提交更改之前测试 GitLab CI/CD 配置的有效性，您可以使用 CI lint 工具。要访问它，请转到 **CI/CD > 编辑器** 并选择 **Lint** 选项卡。

此工具会检查语法和逻辑错误，但比编辑器中的自动[验证](#验证-ci-配置)更详细。

结果实时更新。您对配置所做的任何更改都会反映在 CI lint 中。它显示与现有 [CI Lint 工具](../lint.md)相同的结果。

![Linting errors in a CI configuration](img/pipeline_editor_lint_v13_8.png)

## 可视化 CI 配置

> - 引入于 13.5 版本
> - 移动到 **CI/CD > 编辑器**于 13.7 版本。
> - 功能标志移除于 13.12 版本。

要查看您的 `.gitlab-ci.yml` 配置的可视化，请在您的项目中，转到 **CI/CD > 编辑器**，然后选择 **可视化** 选项卡。可视化显示所有阶段和作业。任何 [`needs`](../yaml/index.md#needs) 关系都显示为将作业连接在一起的线，显示执行的层次结构：

![CI configuration Visualization](img/ci_config_visualization_v13_7.png)

将鼠标悬停在工作上以突出其 `needs` 关系：

![CI configuration visualization on hover](img/ci_config_visualization_hover_v13_7.png)

如果配置没有任何  `needs` 关系，则不会绘制任何线条，因为每个作业仅取决于成功完成前一个阶段。

## 查看扩展配置

> - 引入于 13.9 版本
> - 功能标志移除于 13.12 版本

要将完全扩展的 CI/CD 配置作为一个组合文件查看，请转到流水线编辑器的 **查看合并的 YAML** 选项卡。此选项卡显示扩展配置，其中：

- 使用 [`include`](../yaml/index.md#include) 导入的配置被复制到视图中。
- 使用 [`extends`](../yaml/index.md#extends) 的作业，扩展配置合并显示到作业<!--[扩展配置合并到作业](../yaml/index.md#合并详细信息)-->。
- YAML 锚点[替换为链接配置](../yaml/yaml_optimization.md#锚点)。

## 提交对 CI 配置的更改

提交表单出现在编辑器中每个选项卡的底部，因此您可以随时提交更改。

当您对更改感到满意时，添加描述性提交消息并输入分支。分支字段默认为您项目的默认分支。

如果您输入新的分支名称，则会出现 **使用这些更改开始新的合并请求** 复选框。选择它以在提交更改后启动新的合并请求。

![The commit form with a new branch](img/pipeline_editor_commit_v13_8.png)
