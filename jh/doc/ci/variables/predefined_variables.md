---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 预定义变量参考 **(FREE)**

每个 GitLab CI/CD 流水线中都有预定义的 [CI/CD 变量](index.md)。

某些变量仅适用于更新版本的 GitLab Runner<!--[GitLab Runner](https://docs.gitlab.com/runner/)-->。

您可以使用 `script` 命令[输出可用于作业的所有变量的值](index.md#列出所有环境变量)。

<!--
还有 [Kubernetes 特定的部署变量](../../user/project/clusters/deploy_to_cluster.md#deployment-variables)。

There are also a number of [variables you can use to configure runner behavior](../runners/configure_runners.md#configure-runner-behavior-with-variables) globally or for individual jobs.

-->

| 变量                                 | GitLab | Runner | 描述 |
|------------------------------------------|--------|--------|-------------|
| `CHAT_CHANNEL`                           | 10.6   | all    | 触发 ChatOps<!--[ChatOps](../chatops/index.md)--> 命令的源聊天频道。 |
| `CHAT_INPUT`                             | 10.6   | all    | 使用 ChatOps<!--[ChatOps](../chatops/index.md)--> 命令传递的附加参数。 |
| `CHAT_USER_ID`                           | 14.4   | all    | 触发 ChatOps<!--[ChatOps](../chatops/index.md)--> 命令的用户的聊天服务用户 ID。 |
| `CI`                                     | all    | 0.4    | 适用于在 CI/CD 中执行的所有作业。可用时为 `true`。 |
| `CI_API_V4_URL`                          | 11.7   | all    | GitLab API v4 根 URL。 |
| `CI_BUILDS_DIR`                          | all    | 11.10  | 执行构建的顶级目录。 |
| `CI_COMMIT_AUTHOR`                       | 13.11  | all    | `Name <email>` 格式的提交作者。 |
| `CI_COMMIT_BEFORE_SHA`                   | 11.2   | all    | 上一个最新提交出现在一个分支上。在合并请求的流水线中总是 `0000000000000000000000000000000000000000`。 |
| `CI_COMMIT_BRANCH`                       | 12.6   | 0.5    | 提交分支名称。在分支流水线中可用，包括默认分支的流水线。在合并请求流水线或标签流水线中不可用。 |
| `CI_COMMIT_DESCRIPTION`                  | 10.8   | all    | 提交的描述。如果标题短于 100 个字符，则消息没有第一行。 |
| `CI_COMMIT_MESSAGE`                      | 10.8   | all    | 完整的提交消息。 |
| `CI_COMMIT_REF_NAME`                     | 9.0    | all    | 为其构建项目的分支或标签名称。 |
| `CI_COMMIT_REF_PROTECTED`                | 11.11  | all    | 如果作业正在运行以获取受保护的 ref 为 `true` 。 |
| `CI_COMMIT_REF_SLUG`                     | 9.0    | all    | `CI_COMMIT_REF_NAME` 小写，缩短为 63 字节，除了 `0-9` 和 `a-z` 之外的所有内容都替换为 `-`。没有前导/尾随`-`。 在 URL、主机名和域名中使用。 |
| `CI_COMMIT_SHA`                          | 9.0    | all    | 项目为其构建的提交修订。 |
| `CI_COMMIT_SHORT_SHA`                    | 11.7   | all    | `CI_COMMIT_SHA` 的前八个字符。 |
| `CI_COMMIT_TAG`                          | 9.0    | 0.5    | 提交标签名称。仅在标签流水线中可用。 |
| `CI_COMMIT_TIMESTAMP`                    | 13.4   | all    | ISO 8601 格式的提交时间戳。 |
| `CI_COMMIT_TITLE`                        | 10.8   | all    | 提交的标题。消息的完整第一行。 |
| `CI_CONCURRENT_ID`                       | all    | 11.10  | 单个 executor 中构建执行的唯一 ID。 |
| `CI_CONCURRENT_PROJECT_ID`               | all    | 11.10  | 单个 executor 和项目中构建执行的唯一 ID。 |
| `CI_CONFIG_PATH`                         | 9.4    | 0.5    | CI/CD 配置文件的路径。默认为`.gitlab-ci.yml`。在正在运行的流水线中只读。 |
| `CI_DEBUG_TRACE`                         | all    | 1.7    | 如果 [debug 日志 (跟踪)](index.md#debug-日志) 已启用为`true`。 |
| `CI_DEFAULT_BRANCH`                      | 12.4   | all    | 项目默认分支的名称。 |
| `CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX` | 13.7   | all    | 通过 Dependency Proxy 拉取镜像的顶级群组镜像前缀。 |
| `CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX` | 14.3   | all    | 通过 Dependency Proxy 拉取镜像的直接群组镜像前缀。 |
| `CI_DEPENDENCY_PROXY_PASSWORD`           | 13.7   | all    | 通过 Dependency Proxy 拉取镜像的密码。 |
| `CI_DEPENDENCY_PROXY_SERVER`             | 13.7   | all    | 用于登录 Dependency Proxy 的服务器。相当于 `$CI_SERVER_HOST:$CI_SERVER_PORT`。 |
| `CI_DEPENDENCY_PROXY_USER`               | 13.7   | all    | 通过 Dependency Proxy 拉取镜像的用户名。 |
| `CI_DEPLOY_FREEZE`                       | 13.2   | all    | 仅当流水线在<!--[部署冻结窗口](../../user/project/releases/index.md#prevent-unintentional-releases-by-setting-a-deploy-freeze)-->部署冻结窗口期间运行时才可用。可用时为 `true` 。 |
| `CI_DEPLOY_PASSWORD`                     | 10.8   | all    | <!--[GitLab Deploy Token](../../user/project/deploy_tokens/index.md#gitlab-deploy-token)-->GitLab Deploy Token 的认证密码，如果项目有的话。 |
| `CI_DEPLOY_USER`                         | 10.8   | all    | <!--[GitLab Deploy Token](../../user/project/deploy_tokens/index.md#gitlab-deploy-token)-->GitLab Deploy Token 的认证用户名，如果项目有的话。 |
| `CI_DISPOSABLE_ENVIRONMENT`              | all    | 10.1   | 仅当作业在一次性环境中执行时才可用（仅为该作业创建并在执行后处理/销毁 - 除 `shell` 和 `ssh` 之外的所有 executor）。可用时为 `true`。 |
| `CI_ENVIRONMENT_NAME`                    | 8.15   | all    | 此作业的环境名称。 如果设置了 [`environment:name`](../yaml/index.md#environmentname)，则可用。 |
| `CI_ENVIRONMENT_SLUG`                    | 8.15   | all    | 环境名称的简化版本，适合包含在 DNS、URL、Kubernetes labels 等。如果设置了 [`environment:name`](../yaml/index.md#environmentname)，则可用。Slug 被截断为 24 个字符。 |
| `CI_ENVIRONMENT_URL`                     | 9.3    | all    | 此作业的环境 URL。如果设置了 [`environment:url`](../yaml/index.md#environmenturl)，则可用。 |
| `CI_ENVIRONMENT_ACTION`                  | 13.11  | all    | 为此作业的环境指定的操作注释。如果设置了 [`environment:action`](../yaml/index.md#environmentaction)，则可用。可以是`start`、`prepare` 或 `stop`。 |
| `CI_ENVIRONMENT_TIER`                    | 14.0   | all    | 此作业的环境部署级别<!--[环境部署级别](../environments/index.md#deployment-tier-of-environments)-->。 |
| `CI_HAS_OPEN_REQUIREMENTS`               | 13.1   | all    | 仅当流水线的项目具有打开的[需求](../../user/project/requirements/index.md) 时才可用。可用时为 `true`。 |
| `CI_JOB_ID`                              | 9.0    | all    | 作业的内部 ID，在 GitLab 实例中的所有作业中是唯一的。 |
| `CI_JOB_IMAGE`                           | 12.9   | 12.9   | 运行作业的 Docker 镜像的名称。 |
| `CI_JOB_JWT`                             | 12.10  | all    | 一个 RS256 JSON Web 令牌，用于与支持 JWT 身份验证的第三方系统进行身份验证，例如 [HashiCorp's Vault](../secrets/index.md)。 |
| `CI_JOB_MANUAL`                          | 8.12   | all    | 如果作业是手动启动为 `true`。 |
| `CI_JOB_NAME`                            | 9.0    | 0.5    | 作业名称 |
| `CI_JOB_STAGE`                           | 9.0    | 0.5    | 作业阶段名称 |
| `CI_JOB_STATUS`                          | all    | 13.5   | 执行每个 runner 阶段时的作业状态。与 [`after_script`](../yaml/index.md#after_script) 一起使用。可以是 `success`，`failed` 或 `canceled`。 |
| `CI_JOB_TOKEN`                           | 9.0    | 1.2    | 使用某些 API 端点<!--[某些 API 端点](../jobs/ci_job_token.md)-->进行身份验证的令牌。只要作业正在运行，令牌就有效。 |
| `CI_JOB_URL`                             | 11.1   | 0.5    | 作业详细信息 URL。 |
| `CI_JOB_STARTED_AT`                      | 13.10  | all    | 作业开始时的 UTC 日期时间，采用 [ISO 8601](https://tools.ietf.org/html/rfc3339#appendix-A) 格式。 |
| `CI_KUBERNETES_ACTIVE`                   | 13.0   | all    | 仅当流水线具有可用于部署的 Kubernetes 集群时才可用。可用时为 `true`。 |
| `CI_NODE_INDEX`                          | 11.5   | all    | 作业集中的作业 index。仅当作业使用 [`parallel`](../yaml/index.md#parallel) 时可用。 |
| `CI_NODE_TOTAL`                          | 11.5   | all    | 此作业并行运行的实例总数。如果作业不使用 [`parallel`](../yaml/index.md#parallel)，则设置为 `1`。 |
| `CI_OPEN_MERGE_REQUESTS`                 | 13.8   | all    | 使用当前分支和项目作为合并请求源的最多四个合并请求的逗号分隔列表。如果分支具有关联的合并请求，则仅在分支和合并请求流水线中可用。例如，`gitlab-org/gitlab!333,gitlab-org/gitlab-foss!11`。 |
| `CI_PAGES_DOMAIN`                        | 11.8   | all    | 托管 GitLab Pages 的配置域名。 |
| `CI_PAGES_URL`                           | 11.8   | all    | GitLab Pages 站点的 URL。始终是 `CI_PAGES_DOMAIN` 的子域名。 |
| `CI_PIPELINE_ID`                         | 8.10   | all    | 当前流水线的实例级 ID。该 ID 在实例上的所有项目中都是唯一的。 |
| `CI_PIPELINE_IID`                        | 11.0   | all    | 当前流水线的项目级 IID（内部 ID）。此 ID 仅在当前项目中是唯一的。 |
| `CI_PIPELINE_SOURCE`                     | 10.0   | all    | 流水线是如何触发的。可以是 `push`、`web`、`schedule`、`api`、`external`、`chat`、`webide`、`merge_request_event`、`external_pull_request_event`、`parent_pipeline`、[`trigger` 或 `pipeline `](../triggers/index.md#身份验证令牌)。 |
| `CI_PIPELINE_TRIGGERED`                  | all    | all    | 如果作业是[触发的](../triggers/index.md)为 true。 |
| `CI_PIPELINE_URL`                        | 11.1   | 0.5    | 流水线详细信息的 URL。 |
| `CI_PIPELINE_CREATED_AT`                 | 13.10  | all    | 创建流水线时的 UTC 日期时间，采用 [ISO 8601](https://tools.ietf.org/html/rfc3339#appendix-A) 格式。 |
| `CI_PROJECT_CONFIG_PATH`                 | 13.8 to 13.12 | all    | 在 14.0 版本中移除。使用 `CI_CONFIG_PATH`。 |
| `CI_PROJECT_DIR`                         | all    | all    | 仓库克隆到的完整路径，以及作业从哪里运行。如果设置了 GitLab Runner `builds_dir` 参数，这个变量是相对于 `builds_dir` 的值设置的。<!--For more information, see the [Advanced GitLab Runner configuration](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#the-runners-section).--> |
| `CI_PROJECT_ID`                          | all    | all    | 当前项目的 ID。该 ID 在实例上的所有项目中都是唯一的。 |
| `CI_PROJECT_NAME`                        | 8.10   | 0.5    | 项目目录的名称。例如，如果项目 URL 是 `gitlab.example.com/group-name/project-1`，则 `CI_PROJECT_NAME` 是 `project-1`。 |
| `CI_PROJECT_NAMESPACE`                   | 8.10   | 0.5    | 作业的项目命名空间（用户名或组名）。 |
| `CI_PROJECT_PATH_SLUG`                   | 9.3    | all    | `$CI_PROJECT_PATH` 小写，不是 `a-z` 或 `0-9` 的字符替换为 `-` 并缩短为 63 字节。 在 URL 和域名中使用。 |
| `CI_PROJECT_PATH`                        | 8.10   | 0.5    | 包含项目名称的项目命名空间。|
| `CI_PROJECT_REPOSITORY_LANGUAGES`        | 12.3   | all    | 仓库中使用的语言的逗号分隔的小写列表。例如`ruby,javascript,html,css`。 |
| `CI_PROJECT_ROOT_NAMESPACE`              | 13.2   | 0.5    | 作业的根项目命名空间（用户名或组名）。例如，如果`CI_PROJECT_NAMESPACE` 是 `root-group/child-group/grandchild-group`，则 `CI_PROJECT_ROOT_NAMESPACE` 是 `root-group`。 |
| `CI_PROJECT_TITLE`                       | 12.4   | all    | GitLab Web 界面中显示的人类可读的项目名称。 |
| `CI_PROJECT_URL`                         | 8.10   | 0.5    | 项目的 HTTP(S) 地址。 |
| `CI_PROJECT_VISIBILITY`                  | 10.3   | all    | 项目可见性。可以是 `internal`、`private` 或 `public`。 |
| `CI_PROJECT_CLASSIFICATION_LABEL`        | 14.2   | all    | 项目外部授权分类标记<!--[外部授权分类标记](../../user/admin_area/settings/external_authorization.md)-->。 |
| `CI_REGISTRY_IMAGE`                      | 8.10   | 0.5    | 项目的 Container Registry 的地址。仅当为项目启用了 Container Registry 时才可用。 |
| `CI_REGISTRY_PASSWORD`                   | 9.0    | all    | 将容器推送到项目的 GitLab Container Registry 的密码。仅当为项目启用了 Container Registry 时才可用。 此密码值与 `CI_JOB_TOKEN` 相同，并且仅在作业运行时有效。使用 `CI_DEPLOY_PASSWORD` 长期访问镜像库。 |
| `CI_REGISTRY_USER`                       | 9.0    | all    | 将容器推送到项目的 GitLab Container Registry 的用户名。仅当为项目启用了 Container Registry 时才可用。 |
| `CI_REGISTRY`                            | 8.10   | 0.5    | GitLab Container Registry 的地址。仅当为项目启用了 Container Registry 时才可用。 如果在镜像库配置中指定了一个值，则此变量包括一个 `:port` 值。 |
| `CI_REPOSITORY_URL`                      | 9.0    | all    | 克隆 Git 仓库的 URL。 |
| `CI_RUNNER_DESCRIPTION`                  | 8.10   | 0.5    | runner 的描述。 |
| `CI_RUNNER_EXECUTABLE_ARCH`              | all    | 10.6   | GitLab Runner 可执行文件的操作系统/架构。可能和 executor 的环境不一样。 |
| `CI_RUNNER_ID`                           | 8.10   | 0.5    | 正在使用的 runner 的唯一 ID。 |
| `CI_RUNNER_REVISION`                     | all    | 10.6   | 运行作业的 runner 的修订版。 |
| `CI_RUNNER_SHORT_TOKEN`                  | all    | 12.3   | 用于验证新作业请求的 runner 令牌的前八个字符。用作 runner 的唯一 ID。 |
| `CI_RUNNER_TAGS`                         | 8.10   | 0.5    | 以逗号分隔的 runner 标签列表。 |
| `CI_RUNNER_VERSION`                      | all    | 10.6   | 运行作业的 GitLab Runner 的版本。 |
| `CI_SERVER_HOST`                         | 12.1   | all    | GitLab 实例 URL 的主机，没有协议或端口。例如`gitlab.example.com`。 |
| `CI_SERVER_NAME`                         | all    | all    | 协调作业的 CI/CD 服务器的名称。 |
| `CI_SERVER_PORT`                         | 12.8   | all    | GitLab 实例 URL 的端口，没有主机或协议。例如 `8080`。 |
| `CI_SERVER_PROTOCOL`                     | 12.8   | all    | GitLab 实例 URL 的协议，没有主机或端口。例如 `https`。 |
| `CI_SERVER_REVISION`                     | all    | all    | 计划作业的 GitLab 修订版。 |
| `CI_SERVER_URL`                          | 12.7   | all    | GitLab 实例的基本 URL，包括协议和端口。 例如`https://gitlab.example.com:8080`。 |
| `CI_SERVER_VERSION_MAJOR`                | 11.4   | all    | GitLab 实例的主版本。例如，如果版本为 `13.6.1`，则`CI_SERVER_VERSION_MAJOR` 为 `13`。 |
| `CI_SERVER_VERSION_MINOR`                | 11.4   | all    | GitLab 实例的小版本。例如，如果版本为 `13.6.1`，则`CI_SERVER_VERSION_MINOR` 为 `6`。 |
| `CI_SERVER_VERSION_PATCH`                | 11.4   | all    | GitLab 实例的补丁版本。例如，如果版本为 `13.6.1`，则`CI_SERVER_VERSION_PATCH` 为 `1`。 |
| `CI_SERVER_VERSION`                      | all    | all    | GitLab 实例的完整版本。 |
| `CI_SERVER`                              | all    | all    | 适用于在 CI/CD 中执行的所有作业。可用时为 `true`。 |
| `CI_SHARED_ENVIRONMENT`                  | all    | 10.1   | 仅当作业在共享环境中执行时才可用（跨 CI/CD 调用持久化的，如 `shell` 或 `ssh` executor）。可用时为 `true`。 |
| `GITLAB_CI`                              | all    | all    | 适用于在 CI/CD 中执行的所有作业。可用时为 `true`。 |
| `GITLAB_FEATURES`                        | 10.6   | all    | 可用于实例和许可证的许可功能的逗号分隔列表。 |
| `GITLAB_USER_EMAIL`                      | 8.12   | all    | 开始作业的用户的电子邮件。 |
| `GITLAB_USER_ID`                         | 8.12   | all    | 启动作业的用户的 ID。 |
| `GITLAB_USER_LOGIN`                      | 10.0   | all    | 开始作业的用户的用户名。 |
| `GITLAB_USER_NAME`                       | 10.0   | all    | 启动作业的用户的姓名。 |
| `TRIGGER_PAYLOAD`                        | 13.9   | all    | webhook 负载。仅当流水线使用 webhook 触发<!--[使用 webhook 触发](../triggers/index.md#using-webhook-payload-in-the-triggered-pipeline)-->时可用。 |

## 合并请求流水线的预定义变量

这些变量在以下情况下可用：

- 流水线[是合并请求流水线](../pipelines/merge_request_pipelines.md)。
- 合并请求已打开。

| 变量                               | GitLab | Runner | 描述 |
|----------------------------------------|--------|--------|-------------|
| `CI_MERGE_REQUEST_APPROVED`            | 14.1   | all    | 合并请求的批准状态。当[合并请求批准](../../user/project/merge_requests/approvals/index.md) 可用并且合并请求已被批准时为 `true`。 |
| `CI_MERGE_REQUEST_ASSIGNEES`           | 11.9   | all    | 合并请求的指派人用户名的逗号分隔列表。 |
| `CI_MERGE_REQUEST_ID`                  | 11.6   | all    | 合并请求的实例级 ID。这是 GitLab 上所有项目的唯一 ID。 |
| `CI_MERGE_REQUEST_IID`                 | 11.6   | all    | 合并请求的项目级 IID（内部 ID）。此 ID 对于当前项目是唯一的。 |
| `CI_MERGE_REQUEST_LABELS`              | 11.9   | all    | 合并请求的逗号分隔标签名称。 |
| `CI_MERGE_REQUEST_MILESTONE`           | 11.9   | all    | 合并请求的里程碑标题。 |
| `CI_MERGE_REQUEST_PROJECT_ID`          | 11.6   | all    | 合并请求的项目 ID。 |
| `CI_MERGE_REQUEST_PROJECT_PATH`        | 11.6   | all    | 合并请求的项目路径。例如 `namespace/awesome-project`。 |
| `CI_MERGE_REQUEST_PROJECT_URL`         | 11.6   | all    | 合并请求的项目的 URL。例如，`http://192.168.10.15:3000/namespace/awesome-project`。 |
| `CI_MERGE_REQUEST_REF_PATH`            | 11.6   | all    | 合并请求的引用路径。例如，`refs/merge-requests/1/head`。 |
| `CI_MERGE_REQUEST_SOURCE_BRANCH_NAME`  | 11.6   | all    | 合并请求的源分支名称。 |
| `CI_MERGE_REQUEST_SOURCE_BRANCH_SHA`   | 11.9   | all    | 合并请求的源分支的 HEAD SHA。该变量在合并请求流水线中为空。SHA 仅存在于[合并结果流水线](../pipelines/pipelines_for_merged_results.md)中。 **(PREMIUM)** |
| `CI_MERGE_REQUEST_SOURCE_PROJECT_ID`   | 11.6   | all    | 合并请求的源项目的 ID。 |
| `CI_MERGE_REQUEST_SOURCE_PROJECT_PATH` | 11.6   | all    | 合并请求的源项目路径。 |
| `CI_MERGE_REQUEST_SOURCE_PROJECT_URL`  | 11.6   | all    | 合并请求的源项目的 URL。 |
| `CI_MERGE_REQUEST_TARGET_BRANCH_NAME`  | 11.6   | all    | 合并请求的目标分支名称。 |
| `CI_MERGE_REQUEST_TARGET_BRANCH_SHA`   | 11.9   | all    | 合并请求的目标分支的 HEAD SHA。该变量在合并请求流水线中为空。SHA 仅存在于[合并结果流水线](../pipelines/pipelines_for_merged_results.md)中。 **(PREMIUM)** |
| `CI_MERGE_REQUEST_TITLE`               | 11.9   | all    | 合并请求的标题。 |
| `CI_MERGE_REQUEST_EVENT_TYPE`          | 12.3   | all    | 合并请求的事件类型。可以是 `detached`、`merged_result` 或 `merge_train`。 |
| `CI_MERGE_REQUEST_DIFF_ID`             | 13.7   | all    | 合并请求差异的版本。 |
| `CI_MERGE_REQUEST_DIFF_BASE_SHA`       | 13.7   | all    | 合并请求差异的基本 SHA。 |

## 外部拉取请求流水线的预定义变量

这些变量仅在以下情况下可用：

- 流水线是外部拉取请求流水线<!--[外部拉取请求流水线](../ci_cd_for_external_repos/index.md#pipelines-for-external-pull-requests)-->
- 拉取请求是开放的。

| 变量                                     | GitLab | Runner | 描述 |
|-----------------------------------------------|--------|--------|-------------|
| `CI_EXTERNAL_PULL_REQUEST_IID`                | 12.3   | all    | 从 GitHub 拉取请求 ID。 |
| `CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY`  | 13.3   | all    | 拉取请求的源仓库名称。 |
| `CI_EXTERNAL_PULL_REQUEST_TARGET_REPOSITORY`  | 13.3   | all    | 拉取请求的目标仓库名称。 |
| `CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME` | 12.3   | all    | 拉取请求的源分支名称。 |
| `CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA`  | 12.3   | all    | 拉取请求源分支的 HEAD SHA。 |
| `CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME` | 12.3   | all    | 拉取请求的目标分支名称。 |
| `CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA`  | 12.3   | all    | 拉取请求目标分支的 HEAD SHA。 |
