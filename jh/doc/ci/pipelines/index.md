---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/ci/pipelines.html'
type: reference
---

# CI/CD 流水线 **(FREE)**

<!--
NOTE:
Watch the
["Mastering continuous software development"](https://about.gitlab.com/webcast/mastering-ci-cd/)
webcast to see a comprehensive demo of a GitLab CI/CD pipeline.
-->

流水线是持续集成、交付和部署的顶级组件。

流水线包括：

- 工作，定义*做什么*。例如，编译或测试代码的作业。
- 阶段，定义*何时*运行作业。例如，在编译代码的阶段之后运行测试的阶段。

作业由 <!--[runners](../runners/index.md)-->runners 执行。如果有足够多的并发运行程序，同一阶段的多个作业将并行执行。

如果一个阶段中的*所有*作业成功，流水线将进入下一个阶段。

如果某个阶段中的*任何*作业失败，则下一个阶段（通常）不会执行并且流水线提前结束。

一般来说，流水线是自动执行的，一旦创建就不需要干预。但是，有时您也可以手动与流水线交互。

一个典型的流水线可能包含四个阶段，按以下顺序执行：

- 一个 `build` 阶段，有一个名为 `compile` 的作业。
- 一个 `test` 阶段，有两个名为 `test1` 和 `test2` 的作业。
- 一个 `staging` 阶段，有一个名为 `deploy-to-stage` 的作业。
- 一个`production`阶段，有一个名为`deploy-to-prod`的作业。

NOTE:
如果您有一个[极狐GitLab 从中拉取的镜像仓库](../../user/project/repository/mirror/pull.md)，您可能需要在项目的 **设置 > 仓库 > 从远端仓库中拉取 > 触发镜像更新的流水线**。

## 流水线类型

流水线可以通过多种不同的方式进行配置：

- [基本流水线](pipeline_architectures.md#基本流水线) 同时运行每个阶段的所有内容，然后是下一个阶段。
- [有向无环图管道 (DAG) 流水线](../directed_acyclic_graph/index.md) 基于作业之间的关系，可以比基本流水线运行得更快。
- [多项目流水线](multi_project_pipelines.md) 将不同项目的流水线组合在一起。
- [父子流水线](parent_child_pipelines.md) 将复杂的流水线分解为一个可以触发多个子流水线的父流水线，这些子流水线都运行在同一个项目中并具有相同的 SHA。这种流水线架构通常用于 mono-repos。
- [合并请求的流水线](../pipelines/merge_request_pipelines.md) 仅针对合并请求运行（而不是针对每次提交）。
- [合并结果的流水线](../pipelines/pipelines_for_merged_results.md) 是来自源分支的更改已经合并到目标分支的合并请求流水线。
- [合并队列](../pipelines/merge_trains.md) 流水线使合并结果来一个接一个地排队合并。

## 配置流水线

流水线及其组件作业和阶段在每个项目的 CI/CD 流水线配置文件中定义。

- [作业](../jobs/index.md)是基本的配置组件。
- 阶段是使用 [`stages`](../yaml/index.md#stages) 关键字定义的。

有关 CI 流水线文件中的配置选项列表，请参阅 [GitLab CI/CD 流水线配置参考](../yaml/index.md)。

您还可以通过 GitLab UI 配置流水线的特定方面。例如：

- 每个项目的[流水线设置](settings.md)。
- [计划流水线](schedules.md)。
- 自定义 CI/CD 变量<!--[自定义 CI/CD 变量](../variables/index.md#custom-cicd-variables)-->。

### Ref specs for runners

当 runner 选择流水线作业时，系统会提供该作业的元数据，包括 [Git refspecs](https://git-scm.com/book/en/v2/Git-Internals-The-Refspec)，指示哪个引用（分支、标签等）和提交（SHA1) 从您的项目仓库中检出。

下表列出了为每种流水线类型注入的 refspecs：

| 流水线类型                                                     | Refspecs                                                                                       |
|---------------                                                     |----------------------------------------                                                        |
| 分支流水线                                              | `+<sha>:refs/pipelines/<id>` 和 `+refs/heads/<name>:refs/remotes/origin/<name>` |
| 标签流水线                                                  | `+<sha>:refs/pipelines/<id>` 和 `+refs/tags/<name>:refs/tags/<name>`            |
| [合并请求流水线](../pipelines/merge_request_pipelines.md) | `+<sha>:refs/pipelines/<id>`                                                     |

refs `refs/heads/<name>` 和 `refs/tags/<name>` 存在于您的项目仓库中。极狐GitLab 在运行流水线作业期间，生成特殊引用 `refs/pipelines/<id>`。即使在删除关联的分支或标记后，也可以创建此引用。因此，它在某些功能中很有用，例如自动停止环境<!--[自动停止环境](../environments/index.md#stop-an-environment)-->和可能运行流水线的合并队列<!--[合并队列](../pipelines/merge_trains.md)-->分支删除后。

### 查看流水线

您可以在项目的 **CI/CD > 流水线** 页面下找到当前和历史流水线运行。您还可以通过导航到其 **流水线** 选项卡来访问合并请求的流水线。

![Pipelines index page](img/pipelines_index_v13_0.png)

单击流水线以打开 **流水线详细信息** 页面并显示为该流水线运行的作业。从这里您可以取消正在运行的流水线、在失败的流水线上重试作业，或[删除流水线](#删除流水线)。

从 12.3 版本开始，`/project/pipelines/[branch]/latest` 提供了指向给定分支最后一次提交的最新流水线的链接。
此外，`/project/pipelines/latest` 将您重定向到项目默认分支上最后一次提交的最新流水线。

从 13.0 版本开始，您可以通过以下方式过滤流水线列表：

- 触发器作者
- 分支名称
- 状态（13.1 及更高版本）
- 标签（13.1 及更高版本）
- 源（14.3 及更高版本）

从 14.2 版本开始，可以更改流水线列以显示流水线 ID 或流水线 IID。

如果您使用 VS Code 编辑您的 GitLab CI/CD 配置, <!--[GitLab Workflow VS Code 扩展](../../user/project/repository/vscode.md)-->GitLab Workflow VS Code 扩展帮助您[验证您的配置](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow#validate-gitlab-ci-configuration)并[查看您的流水线状态](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow#information-about-your-branch-pipelines-mr-closing-issue)。

### 手动运行流水线

可以使用预定义或手动指定的变量<!--[变量](../variables/index.md)-->手动执行流水线。

如果在流水线的正常操作之外需要流水线的结果（例如，代码构建），您可能会这样做。

要手动执行流水线：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **CI/CD > 流水线**。
1. 选择 **运行流水线**。
1. 在 **为分支名称或标签运行** 字段中，选择要为其运行流水线的分支或标签。
1. 输入流水线运行所需的任何环境变量<!--[环境变量](../variables/index.md)-->。您可以设置特定变量以使其值在表单中预填充<!--[值在表单中预填充](#手动流水线中的预填充变量)-->。
1. 选择 **运行流水线**。

流水线现在按照配置执行作业。

#### 手动流水线中的预填充变量

> 引入于 13.7 版本

您可以使用 [`value` 和 `description`](../yaml/index.md#手动流水线中的预填充变量) 关键字来定义在手动运行流水线时，预填充的流水线级（全局）变量<!--[流水线级（全局）变量](../variables/index.md#create-a-custom-cicd-variable-in-the-gitlab-ciyml-file)-->。

在手动触发的流水线中，**运行流水线** 页面显示所有顶级变量，以及 `.gitlab-ci.yml` 文件中定义的 `description` 和 `value`。然后可以根据需要修改这些值，这会覆盖该单个流水线运行的值。

说明显示在变量下方。它可以用来解释变量的用途，可接受的值是什么，等等：

```yaml
variables:
  DEPLOY_ENVIRONMENT:
    value: "staging"  # Deploy to staging by default
    description: "The deployment target. Change this variable to 'canary' or 'production' if needed."
```

当您手动运行流水线时，您无法将作业级变量设置为预填充。

### 使用 URL 查询字符串运行流水线

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/24146) in GitLab 12.5.
-->

您可以使用查询字符串预填充 **运行流水线** 页面。例如，查询字符串 `.../pipelines/new?ref=my_branch&var[foo]=bar&file_var[file_foo]=file_bar` 预先填充 **运行流水线** 页面：

- **运行**字段：`my_branch`。
- **变量**部分：
   - 变量：
     - 键：`foo`
     - 值：`bar`
   - 文件：
     - 键：`file_foo`
     - 值：`file_bar`

`pipelines/new` URL 的格式为：

```plaintext
.../pipelines/new?ref=<branch>&var[<variable_key>]=<value>&file_var[<file_key>]=<value>
```

支持以下参数：

- `ref`：指定用于填充 **运行** 字段的分支。
- `var`：指定一个 `Variable` 变量。
- `file_var`：指定一个 `File` 变量。

对于每个 `var` 或 `file_var`，都需要一个键和值。

### 向您的流水线添加手动交互

<!--[手动作业](../jobs/job_control.md#create-a-job-that-must-be-run-manually)-->手动作业允许您在推进流水线之前需要手动交互。

您可以直接从流水线图中执行此操作。只需单击运行按钮即可执行该特定作业。

例如，您的流水线可以自动启动，但需要手动操作才能部署到生产<!--[部署到生产](../environments/index.md#configure-manual-deployments)-->。
在下面的示例中，`production` 阶段有一个带有手动操作的作业：

![Pipelines example](img/manual_pipeline_v14_2.png)

#### 在一个阶段启动多个手动操作

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/27188) in GitLab 11.11.
-->

可以使用“全部手动运行”按钮同时启动单个阶段中的多个手动操作。
单击此按钮后，将触发每个单独的手动操作并刷新为更新状态。

此功能仅适用于：

- 对于至少具有开发人员角色的用户。
- 如果阶段包含[手动操作](#向您的流水线添加手动交互)。

### 跳过流水线

要在不触发流水线的情况下推送提交，请使用任何大写形式在提交消息中添加 `[ci skip]` 或 `[skip ci]`。

或者，如果您使用的是 Git 2.10 或更高版本，请使用 `ci.skip` [Git 推送选项](../../user/project/push_options.md#极狐gitlab-cicd-推送选项)。
`ci.skip` 推送选项不会跳过合并请求流水线。

### 删除流水线

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/24851) in GitLab 12.7.
-->

项目中具有所有者角色<!--[所有者角色](../../user/permissions.md)--> 的用户可以通过点击 **CI/CD > 流水线** 中的流水线到达 **流水线详情** 页面，然后使用 **删除** 按钮。

![Pipeline Delete Button](img/pipeline-delete.png)

WARNING:
删除流水线会使所有流水线缓存过期，并删除所有相关对象，例如构建、日志、产物和触发器。**此操作无法撤消。**

### 流水线配额

每个用户都有个人流水线配额，用于跟踪所有个人项目中共享 runner 的使用情况。
每个群组都有一个使用配额<!--[使用配额](../../subscriptions/gitlab_com/index.md#ci-pipeline-minutes)-->，用于跟踪群组内创建的所有项目的共享 runner 的使用情况。

当流水线被触发时，无论是谁触发的，都会使用项目所有者的[命名空间](../../user/group/index.md#命名空间)的流水线配额。在这种情况下，命名空间可以是拥有项目的用户或群组。

#### 如何计算流水线持续时间

给定管道的总运行时间不包括重试和挂起（排队）时间。

每个作业都表示为一个 `Period`，它包括：

- `Period#first`（作业开始时）。
- `Period#last`（当工作完成时）。

一个简单的例子是：

- A (1, 3)
- B (2, 4)
- C (6, 7)

在示例中：

- A 从 1 开始，到 3 结束。
- B 从 2 开始，到 4 结束。
- C 从 6 点开始，到 7 点结束。

它可以被视为：

```plaintext
0  1  2  3  4  5  6  7
   AAAAAAA
      BBBBBBB
                  CCCC
```

A、B 和 C 的并集是 (1, 4) 和 (6, 7)。 因此，总运行时间为：

```plaintext
(4 - 1) + (7 - 6) => 4
```

#### 如何计算流水线配额使用量

管道配额使用量计算为每个单独作业的持续时间总和。这与管道*持续时间*[计算](#如何计算流水线持续时间)的方式略有不同。流水线配额使用不考虑并行运行的作业的任何重叠。

例如，流水线由以下作业组成：

- 作业 A 需要 3 分钟。
- 作业 B 需要 3 分钟。
- 作业 C 需要 2 分钟。

流水线配额使用量是每个作业持续时间的总和。在此示例中，将使用 8 分钟 runner，计算公式为：3 + 3 + 2。

### 受保护分支上的流水线安全

在[受保护分支](../../user/project/protected_branches.md)上执行流水线时，会强制执行严格的安全模型。

仅当用户在该特定分支上[允许合并或推送](../../user/project/protected_branches.md)时，才允许在受保护分支上执行以下操作：

- 运行手动流水线（使用 [Web UI](#手动运行流水线) 或流水线 API<!--[流水线 API](#pipelines-api)-->）。
- 运行计划流水线。
- 使用触发器运行流水线。
- 运行按需 DAST 扫描。
- 在现有流水线上触发手动操作。
- 重试或取消现有作业（使用 Web UI 或流水线 API）。

标记为 **受保护** 的 **变量** 只能由在受保护分支上运行的作业访问，防止不受信任的用户意外访问敏感信息，如部署凭据和令牌。

标记为 **受保护** 的 **Runners** 只能在受保护的分支上运行作业，防止不受信任的代码在受保护的 runner 上执行，并保护部署密钥和其他凭据不被无意访问。为了确保要在受保护 runner 上执行的作业不使用常规 runner，必须相应地标记它们。

## 可视化流水线

流水线可以是具有许多顺序和并行作业的复杂结构。

为了更容易理解流水线的流程，极狐GitLab 提供了用于查看流水线及其状态的流水线图。

流水线图可以显示为大图或迷你图，具体取决于您访问该图的页面。

流水线图中，阶段的名称大写。

### 查看完整的流水线图

> - 可视化改进引入于 13.11 版本

[流水线详细信息页面](#查看流水线)显示流水线中所有作业的完整流水线图。

您可以按以下方式对作业进行分组：

- 阶段，将同一阶段的作业一起排列在同一列中：

  ![jobs grouped by stage](img/pipelines_graph_stage_view_v14_2.png)

- [作业依赖项](#在流水线图中查看作业依赖关系)，根据作业的 [`needs`](../yaml/index.md#needs) 依赖项来安排作业。

[多项目流水线图](multi_project_pipelines.md#多项目流水线可视化) 帮助您可视化整个流水线，包括所有跨项目的相互依赖。 **(PREMIUM)**

### 在流水线图中查看作业依赖关系

> - 引入于 13.12 版本。
> - 默认其用于 14.0 版本。
> - 功能标志移除于 14.2 版本。

您可以根据作业的 [`needs`](../yaml/index.md#needs) 依赖关系在流水线图中排列作业。

最左侧列中的作业首先运行，依赖它们的作业分组在下一列中。

例如，`test-job1` 仅依赖于第一列中的作业，因此它显示在左起第二列中。`deploy-job1` 依赖于第一列和第二列中的作业，并显示在第三列中：

![jobs grouped by needs dependency](img/pipelines_graph_dependency_view_v13_12.png)

要添加显示作业之间 `needs` 关系的行，请选择 **查看依赖项** 开关。
<!--
这些行类似于 [需求可视化](../directed_acyclic_graph/index.md#needs-visualization)：
-->

![jobs grouped by needs dependency with lines displayed](img/pipelines_graph_dependency_view_links_v13_12.png)

要查看作业的完整 `needs` 依赖关系树，请将鼠标悬停在其上：

![single job dependency tree highlighted](img/pipelines_graph_dependency_view_hover_v13_12.png)

### 流水线迷你图

流水线迷你图占用的空间更少，并且可以快速告诉您所有作业是否通过或失败。当您转到以下位置时，可以找到流水线迷你图：

- 流水线索引页面。
- 单个提交页面。
- 合并请求页面。
- [流水线编辑器](../pipeline_editor/index.md)（14.5 及更高版本）。

流水线迷你图允许您查看单个提交的所有相关作业以及流水线每个阶段的最终结果。这使您可以快速查看失败的内容并进行修复。

流水线迷你图仅按阶段显示作业。

流水线迷你图中的阶段是可折叠的。将鼠标悬停在他们上方并单击以展开他们的作业。

| 迷你图                                                  | 迷你图展开                                           |
|:-------------------------------------------------------------|:---------------------------------------------------------------|
| ![Pipelines mini graph](img/pipelines_mini_graph_simple.png) | ![Pipelines mini graph extended](img/pipelines_mini_graph.png) |

<!--
### Pipeline success and duration charts

Pipeline analytics are available on the [**CI/CD Analytics** page](../../user/analytics/ci_cd_analytics.md#pipeline-success-and-duration-charts).
-->

### 流水线徽章

流水线状态和测试覆盖率报告徽章可用于每个项目并可配置。
有关向项目添加流水线徽章的信息，请参阅[流水线徽章](settings.md#流水线徽章)。

<!--
## Pipelines API

GitLab provides API endpoints to:

- Perform basic functions. For more information, see [Pipelines API](../../api/pipelines.md).
- Maintain pipeline schedules. For more information, see [Pipeline schedules API](../../api/pipeline_schedules.md).
- Trigger pipeline runs. For more information, see:
  - [Triggering pipelines through the API](../triggers/index.md).
  - [Pipeline triggers API](../../api/pipeline_triggers.md).
-->