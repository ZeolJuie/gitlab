---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference
---

# 父子流水线 **(FREE)**

> 引入于 12.7 版本

随着流水线变得越来越复杂，一些相关的问题开始出现：

- 分阶段结构，其中一个阶段中的所有步骤必须在下一阶段的第一个工作开始之前完成，这会导致任意等待，减慢速度。
- 单个全局流水线的配置变得非常冗长和复杂，难以管理。
- 使用 [`include`](../yaml/index.md#include) 导入会增加配置的复杂性，并可能在无意中复制作业的情况下产生命名空间冲突。
- 流水线 UX 可能会因需要处理的工作和阶段太多而变得笨拙。

此外，有时流水线的行为需要更加动态。选择启动（或不启动）子流水线的能力是一种强大的能力，尤其是在动态生成 YAML 的情况下。

![Parent pipeline graph expanded](img/parent_pipeline_graph_expanded_v14_3.png)

类似于[多项目流水线](multi_project_pipelines.md)，一个流水线可以触发一组并发运行的子管道，但在同一个项目中：

- 子流水线仍然根据阶段顺序执行他们的每个工作，但可以自由地继续他们的阶段，而无需等待父流水线中不相关的工作完成。
- 该配置被拆分为更小的子流水线配置。每个子流水线只包含更容易理解的相关步骤，减少了理解整体配置的认知负担。
- 导入在子流水线级别完成，减少了冲突的可能性。

子流水线与其他 GitLab CI/CD 功能配合良好：

- 使用 [`rules:changes`](../yaml/index.md#ruleschanges) 仅在某些文件更改时触发流水线。例如，这对 monorepos 很有用。
- 由于 `.gitlab-ci.yml` 中的父流水线和子流水线作为正常流水线运行，它们可以有自己的行为和与触发器相关的顺序。

有关如何包含子流水线配置的完整详细信息，请参阅 [`trigger`](../yaml/index.md#trigger) 关键字文档。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Parent-Child Pipelines feature demo](https://youtu.be/n8KpBSqZNbk).
-->

## 示例

最简单的情况是[触发子流水线](../yaml/index.md#trigger) 使用本地 YAML 文件来定义流水线配置。在这种情况下，父流水线触发子流水线，不等待并继续：

```yaml
microservice_a:
  trigger:
    include: path/to/microservice_a.yml
```

您可以在定义子流水线时包含多个文件。子流水线的配置由合并在一起的所有配置文件组成：

```yaml
microservice_a:
  trigger:
    include:
      - local: path/to/microservice_a.yml
      - template: Security/SAST.gitlab-ci.yml
```

在  13.5 及更高版本中，您可以使用 [`include:file`](../yaml/index.md#includefile )，使用不同项目中的配置文件触发子流水线：

```yaml
microservice_a:
  trigger:
    include:
      - project: 'my-group/my-pipeline-library'
        ref: 'main'
        file: '/path/to/child-pipeline.yml'
```

`trigger:include` 接受的最大条目数是三个。

类似于[多项目流水线](multi_project_pipelines.md#触发作业中触发流水线的镜像状态)，我们可以设置父流水线在完成时依赖于子流水线的状态：

```yaml
microservice_a:
  trigger:
    include:
      - local: path/to/microservice_a.yml
      - template: Security/SAST.gitlab-ci.yml
    strategy: depend
```

## 合并请求子流水线

要将子流水线作为[合并请求流水线](merge_request_pipelines.md)触发，我们需要：

- 设置触发作业在合并请求上运行：

```yaml
# parent .gitlab-ci.yml
microservice_a:
  trigger:
    include: path/to/microservice_a.yml
  rules:
    - if: $CI_MERGE_REQUEST_ID
```

- 通过以下任一方式配置子流水线：

   - 将子流水线中的所有作业设置为在合并请求的上下文中进行评估：

    ```yaml
    # child path/to/microservice_a.yml
    workflow:
      rules:
        - if: $CI_MERGE_REQUEST_ID

    job1:
      script: ...

    job2:
      script: ...
    ```

  - 或者，为每个作业设置规则。例如，在合并请求流水线的上下文中只创建 `job1`：

    ```yaml
    # child path/to/microservice_a.yml
    job1:
      script: ...
      rules:
        - if: $CI_MERGE_REQUEST_ID

    job2:
      script: ...
    ```

## 动态子流水线

> 引入于 12.9 版本

您可以定义运行您自己的脚本以生成 YAML 文件的作业，而不是从静态 YAML 文件运行子流水线，该文件然后用于触发子流水线。

这种技术在生成针对已更改内容的流水线或构建目标和架构矩阵方面非常强大。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Create child pipelines using dynamically generated configurations](https://youtu.be/nMdfus2JWHM).
-->

<!--
我们还有一个使用 [Dynamic Child Pipelines with Jsonnet](https://gitlab.com/gitlab-org/project-templates/jsonnet) 的示例项目，它展示了如何使用数据模板语言来生成你的 `.gitlab-ci .yml` 在运行时。
您可以对其他模板语言使用类似的过程，例如 [Dhall](https://dhall-lang.org/) 或 [ytt](https://get-ytt.io/)。
-->

产物路径由 GitLab 解析，而不是 runner，因此路径必须与运行 GitLab 的操作系统的语法匹配。如果极狐GitLab 在 Linux 上运行但使用 Windows 运行程序进行测试，则触发作业的路径分隔符将为 `/`。使用 Windows runner 的作业的其它 CI/CD 配置（如脚本）将使用 `\`。

<!--
In GitLab 12.9, the child pipeline could fail to be created in certain cases, causing the parent pipeline to fail.
This is [resolved](https://gitlab.com/gitlab-org/gitlab/-/issues/209070) in GitLab 12.10.
-->

### 动态子流水线示例

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/35632) in GitLab 12.9.
-->

您可以从[动态生成的配置文件](../pipelines/parent_child_pipelines.md#动态子流水线)触发子流水线：

```yaml
generate-config:
  stage: build
  script: generate-ci-config > generated-config.yml
  artifacts:
    paths:
      - generated-config.yml

child-pipeline:
  stage: test
  trigger:
    include:
      - artifact: generated-config.yml
        job: generate-config
```

`generated-config.yml` 从产物中提取出来，用作触发子流水线的配置。

## 嵌套子流水线

> - 引入于 13.4 版本
> - 功能标志移除于 13.5 版本

引入了父子流水线，最大深度为一级子流水线，后来增加到二级。一个父流水线可以触发多个子流水线，这些子流水线可以触发自己的子流水线。不可能触发另一个级别的子流水线。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, see [Nested Dynamic Pipelines](https://youtu.be/C5j3ju9je2M).
-->

## 将 CI/CD 变量传递给子流水线

您可以使用与多项目流水线相同的方法将 CI/CD 变量传递到下游流水线：

- [使用 `variable` 关键字](multi_project_pipelines.md#使用-variables-关键字将-cicd-变量传递到下游流水线)。
- [使用变量继承](multi_project_pipelines.md#使用变量继承将-cicd-变量传递给下游流水线)。
