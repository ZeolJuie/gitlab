---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/pipelines/schedules.html'
type: reference, howto
---

# 流水线计划 **(FREE)**

流水线通常基于满足某些条件而运行。例如，当一个分支被推送到仓库时。

流水线计划也可用于以特定时间间隔运行[流水线](index.md)。例如：

- 针对某个分支，每个月的 22 日（cron 示例：`0 0 22 * *`）。
- 每个月的第二个星期一（cron 示例：`0 0 * * 1#2`）。
- 每隔一个星期天的 0900（cron 示例：`0 9 * * sun%2`）。
- 每天一次（cron 示例：`0 0 * * *`）。

计划时间使用 cron 符号配置，由 [Fugit](https://github.com/floraison/fugit) 解析。

<!--
In addition to using the GitLab UI, pipeline schedules can be maintained using the
[Pipeline schedules API](../../api/pipeline_schedules.md).
-->

## 先决条件

为了成功创建计划的流水线：

- 计划所有者必须具有权限，才能合并到目标分支。
- 流水线配置必须有效。

否则不会创建流水线。

## 配置流水线计划

要为项目配置计划流水线：

1. 导航到项目的 **CI/CD > 计划** 页面。
1. 点击 **新建计划** 按钮。
1. 填写 **新建流水线计划**表格。
1. 单击 **保存流水线计划** 按钮。

![New Schedule Form](img/pipeline_schedules_new_form.png)

NOTE:
流水线执行[时间取决于](#高级配置) Sidekiq 自己的时间表。

在 **计划** 索引页面中，您可以看到计划运行的流水线列表。下一次运行由安装 GitLab 的服务器自动计算。

![Schedules list](img/pipeline_schedules_list.png)

### 使用变量

您可以传递任意数量的任意变量。它们在 GitLab CI/CD 中可用，可以在您的 [`.gitlab-ci.yml` 文件](../../ci/yaml/index.md)中使用。

![Scheduled pipeline variables](img/pipeline_schedule_variables.png)

### 使用 `rules`

要将作业配置为仅在流水线已计划时执行，请使用 [`rules`](../yaml/index.md#rules) 关键字。

在这个例子中，`make world` 在计划流水线中运行，而 `make build` 在分支和标签流水线中运行：

```yaml
job:on-schedule:
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  script:
    - make world

job:
  rules:
    - if: $CI_PIPELINE_SOURCE == "push"
  script:
    - make build
```

### 高级配置 **(FREE SELF)**

流水线不会完全按计划执行，因为计划由 Sidekiq 处理，Sidekiq 根据其间隔运行。

例如，如果出现以下情况，每天只会创建两个流水线：

- 您设置了一个计划，每分钟创建一个流水线 (`* * * * *`)。
- Sidekiq worker 每天在 00:00 和 12:00 运行（`0 */12 * * *`）。

要更改 Sidekiq worker 的频率：

1. 编辑实例的 `gitlab.rb` 文件中的 `gitlab_rails['pipeline_schedule_worker_cron']` 值。
1. [重新配置极狐GitLab](../../administration/restart_gitlab.md#omnibus-gitlab-reconfigure) 使更改生效。

<!--
For GitLab.com, refer to the [dedicated settings page](../../user/gitlab_com/index.md#gitlab-cicd).
-->

## 使用计划的流水线

配置后，极狐GitLab 支持许多用于处理计划流水线的功能。

### 手动运行

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/15700) in GitLab 10.4.
-->

要手动触发流水线计划，请单击“运行”按钮：

![Play Pipeline Schedule](img/pipeline_schedule_play.png)

这将安排后台作业以运行流水线计划。一条消息将提供一个指向 CI/CD 流水线索引页面的链接。

NOTE:
为了帮助避免滥用，用户被限制为每分钟触发一次流水线。

### 取得所有权

流水线为拥有计划的用户执行。这会影响流水线可以访问的项目和其他资源。

如果用户不拥有流水线，您可以通过单击 **获取所有权** 按钮获取所有权。
下次安排流水线时，将使用您的凭据。

![Schedules list](img/pipeline_schedules_ownership.png)

如果流水线计划的所有者无法在目标分支上创建流水线，计划将停止创建新流水线。

这可能发生在，例如：

- 所有者被禁用或从项目中删除。
- 目标分支或标签受到保护。

在这种情况下，具有足够权限的人必须拥有计划的所有权。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
