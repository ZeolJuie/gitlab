---
type: reference
stage: Manage
group: Import
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 自定义群组级项目模板 **(PREMIUM)**

创建项目时，您可以从模板列表中选择<!--[从模板列表中选择](../project/working_with_projects.md#项目模板)-->。
这些模板（例如 GitLab Pages 或 Ruby）使用模板中包含的文件的副本填充新项目。此信息与项目导入/导出<!--[项目导入/导出](../project/settings/import_export.md)-->使用的信息相同，可以帮助您更快地启动新项目。

您可以可用模板的自定义列表<!--[自定义列表](../project/working_with_projects.md#自定义项目模板)-->，您群组中的所有项目都具有相同的列表。为此，您需要使用要用作模板的项目来填入子组。

您还可以配置实例的自定义模板<!--[实例的自定义模板](../admin_area/custom_project_templates.md)-->。

## 设置群组级项目模板

要在群组中设置自定义项目模板，请将包含项目模板的子组添加到群组设置：

1. 在群组中，创建一个子组<!--[子组](subgroups/index.md)-->。
1. [将项目添加到新子组](index.md#添加项目到群组) 作为模板。
1. 在群组的左侧菜单中，转到 **设置 > 通用**。
1. 展开 **自定义项目模板** 并选择子组。

下次群组成员创建项目时，他们可以选择子组中的任何项目。

嵌套子组中的项目不包括在模板列表中。

## 哪些项目可用作模板

- 如果除 **GitLab Pages** 和 **Security & Compliance** 之外的所有项目功能都设置为 **具有访问权限的任何人**，则任何登录用户都可以选择公开和内部项目作为新项目的模板。
- 私有项目只能由项目成员的用户选择。

### 示例结构

以下是项目模板的示例群组/项目结构，假定为 `myorganization`：

```plaintext
# GitLab instance and group
gitlab.cn/myorganization/
    # Subgroups
    internal
    tools
    # Subgroup for handling project templates
    websites
        templates
            # Project templates
            client-site-django
            client-site-gatsby
            client-site-html

        # Other projects
        client-site-a
        client-site-b
        client-site-c
        ...
```

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
