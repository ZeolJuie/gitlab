---
type: reference
stage: Plan
group: Product Planning
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 路线图 **(PREMIUM)**

<!--
> - Introduced in GitLab 10.5.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/198062) from GitLab Ultimate to GitLab Premium in 12.9.
> - In [GitLab 12.9](https://gitlab.com/gitlab-org/gitlab/-/issues/5164) and later, the epic bars show epics' title, progress, and completed weight percentage.
> - Milestones appear in roadmaps in [GitLab 12.10](https://gitlab.com/gitlab-org/gitlab/-/issues/6802), and later.
> - Feature flag for milestones visible in roadmaps [removed](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/29641) in GitLab 13.0.
> - In [GitLab 13.2](https://gitlab.com/gitlab-org/gitlab/-/issues/214375) and later, the Roadmap also shows milestones in projects in a group.
> - In [GitLab 13.2](https://gitlab.com/gitlab-org/gitlab/-/issues/212494) and later, milestone bars can be collapsed and expanded.
-->

包含开始日期或截止日期的群组中的史诗和里程碑可以以时间线的形式（即甘特图）进行可视化。路线图页面显示一个群组、其子组之一或其中一个群组中的项目的史诗和里程碑。

在史诗栏上，您可以看到每个史诗的标题、进度和完成权重百分比。
当您将鼠标悬停在史诗栏上时，会出现一个弹出框，其中包含史诗的标题、开始日期、截止日期和已完成的权重。

您可以展开包含子史诗的史诗，在路线图中显示其子史诗。
您可以单击史诗标题旁边的 V 形 (**{chevron-down}**) 以展开和折叠子史诗。

在里程碑栏的顶部，您可以看到它们的标题。
当您将鼠标悬停在里程碑栏或标题上时，会出现一个带有标题、开始日期和截止日期的弹出窗口。您还可以单击 **里程碑** 标题旁边的 V 形 (**{chevron-down}**) 来切换里程碑栏的列表。

![roadmap view](img/roadmap_view_v14_3.png)

## 对路线图进行排序和过滤

> - 按里程碑进行过滤引入于 13.7 版本，在名为 `roadmap_daterange_filter` 的功能标志后默认启用。
> - 按史诗机密性进行过滤引入于 13.9 版本。
> - 按史诗进行过滤引入于 13.11 版本。
> - 按里程碑进行过滤的功能标志移除于 14.5 版本。

WARNING:
您可能无法按里程碑过滤路线图。查看上面的 **版本历史** 注释以了解详细信息。

当您想要浏览路线图时，有多种方法可以通过对史诗进行排序或按对您重要的内容对其进行过滤来使其更容易。

下拉菜单可让您仅显示打开或关闭的史诗。默认情况下，显示所有史诗。

![epics state dropdown](img/epics_state_dropdown_v14_3.png)

您可以通过以下方式在路线图视图中对史诗进行排序：

- 开始日期
- 到期日

每个选项都包含一个按钮，用于在 **升序排列** 和 **降序排列** 之间切换排序顺序。
浏览史诗时，排序选项和顺序仍然存在，包括[史诗列表视图](../epics/index.md)。

您还可以在路线图视图中按史诗的以下信息过滤史诗：

- 作者
- 标签
- 里程碑
- [机密性](../epics/manage_epics.md#使一个史诗保密)
- 史诗
- 您的反应

![roadmap date range in weeks](img/roadmap_filters_v13_11.png)

路线图也可以[在史诗中可视化](../epics/index.md#史诗的路线图)。

## 时间线持续时间

<!--
> - Introduced in GitLab 11.0.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/198062) from GitLab Ultimate to GitLab Premium in 12.9.
-->

### 日期范围预设

> - 引入于 14.3 版本。部署在 `roadmap_daterange_filter` 功能标志后默认禁用。
> - 从 14.3 版本适用于自助管理版。
> - 功能标志 `roadmap_daterange_filter` 移除于 14.5 版本。

路线图提供了三个日期范围选项，每个选项都有预定的时间线持续时间：

- **本季度**：包括当前季度的周数。
- **今年**：包括当前年份的数周或数月。
- **3 年内**：包括过去 18 个月和未来 18 个月（即总共三年）中的周、月或季度。

### 布局预设

根据所选的[日期范围预设](#日期范围预设)，路线图支持以下布局预设：

- **季度**：仅在选择 “3 年内”日期范围时可用。
- **月份**：选择“今年”或 “3 年内”日期范围时可用。
- **周**（默认）：适用于所有日期范围预设。

### 季度

![roadmap date range in quarters](img/roadmap_timeline_quarters.png)

在 **季度** 预设中，路线图显示了具有开始或到期日期 **落在** 当前所选日期范围预设内的史诗和里程碑，其中 **今天** 由时间线中的垂直红线显示。时间线标题上，季度名称下方的子标题代表该季度的月份。

### 月

![roadmap date range in months](img/roadmap_timeline_months.png)

在 **月** 预设中，路线图显示了具有开始或到期日期 **落在** 或 **正在** 当前所选日期范围预设的史诗和里程碑，其中 **今天** 由时间线中的垂直红线显示。 时间线标题上，月份名称下方的子标题表示一周的开始日（星期日）的日期。默认情况下选择此预设。

### 周

![roadmap date range in weeks](img/roadmap_timeline_weeks.png)

在 **周** 预设中，路线图显示了具有开始或到期日期 **落在** 或 **正在** 当前选定日期范围预设的史诗和里程碑，其中 **今天** 由时间线中的垂直红线显示。时间线标题上周名称下方的子标题代表一周中的几天。

## 路线图时间线栏

时间线栏根据开始和截止日期指示史诗或里程碑的大致位置。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
