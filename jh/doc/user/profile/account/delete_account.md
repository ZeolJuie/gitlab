---
type: howto
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 删除一个用户帐户 **(FREE)**

可以通过以下任一方式从实例中删除用户：

- 用户自己。
- 管理员。

NOTE:
删除用户会删除该用户命名空间中的所有项目。

## 作为用户

作为用户，要删除您自己的帐户：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **帐户**。
1. 选择 **删除帐户**。

## 作为管理员 **(FREE SELF)**

作为管理员，要删除用户帐户：

1. 在顶部栏上，选择 **菜单 > 管理员**。
1. 在左侧边栏上，选择 **概览 > 用户**。
1. 选择用户。
1. 在 **帐户** 选项卡下，选择：
    - **删除用户**：仅删除用户但保留其[关联记录](#关联记录)。
    - **删除用户和贡献**：删除用户及其相关记录。

WARNING:
使用 **删除用户和贡献** 选项可能会导致删除比预期更多的数据。有关其他详细信息，请参阅下面的[关联记录](#关联记录)。

## 关联记录

<!--
> - Introduced for issues in [GitLab 9.0](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/7393).
> - Introduced for merge requests, award emoji, notes, and abuse reports in [GitLab 9.1](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/10467).
> - Hard deletion from abuse reports and spam logs was introduced in [GitLab 9.1](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/10273), and from the API in [GitLab 9.3](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/11853).
-->

删除用户有两种选择：

- **删除用户**
- **删除用户和贡献**

使用 **删除用户** 选项时，并非所有关联记录都与用户一起删除。
**未**删除的内容列表：

- 用户创建的议题。
- 用户创建的合并请求。
- 用户创建的注释。
- 用户报告的滥用报告。
- 用户创建的表情符号。

这些记录不会被删除，而是被移动到用户名为“Ghost User”的系统范围内的用户，其唯一目的是充当此类记录的容器。已删除用户所做的任何提交仍会显示原始用户的用户名。

使用 **删除用户和贡献** 选项时，**所有**关联的记录都将被删除。这包括上面提到的所有项目，包括议题、合并请求、注释/评论等。考虑阻止用户<!--[阻止用户](../../admin_area/moderate_users.md#block-a-user)-->或改用 **删除用户** 选项。

当用户从滥用报告<!--[滥用报告](../../admin_area/review_abuse_reports.md)-->或垃圾邮件日志中被删除时，这些关联的记录不会被删除，并且会与该用户唯一拥有的任何群组一起被删除。 通过 API <!--[API](../../../api/users.md#user-deletion)-->或从管理中心删除用户时，管理员也可以请求此操作。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
