---
stage: Create
group: Editor
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# 代码片段 **(FREE)**

使用极狐GitLab 片段，您可以存储和与其他用户共享代码和文本。
您可以在片段中[评论](#评论代码片段)、[克隆](#克隆代码片段)和[使用版本控制](#版本化代码片段)。它们可以[包含多个文件](#添加或删除多个文件)。它们还支持[语法高亮](#文件名称)、[嵌入](#嵌入代码片段)、[下载](#下载代码片段)，并且您可以使用 <!--[snippets API](../api/snippets.md)-->snippets API。

您可以通过用户界面或使用 GitLab Workflow VS Code 扩展<!--[GitLab Workflow VS Code 扩展](project/repository/vscode.md)--> 创建和管理代码片段。

![Example of snippet](img/snippet_intro_v13_11.png)

极狐GitLab 提供了两种类型的片段：

- **个人片段**：独立于任何项目创建。
   您可以为代码片段设置[可见性级别](../public_access/public_access.md)：公共、内部或私有。
- **项目片段**：始终与特定项目相关。
   项目片段可以公开或仅对群组成员可见。

## 创建代码片段

您可以通过多种方式创建代码段，具体取决于您是要创建个人代码段还是项目代码段：

1. 选择要创建的片段类型：
   - **要创建个人代码片段**：在代码片段仪表板上，单击 **新建代码段**，或者：
     - *如果您在项目页面上，*选择顶部导航栏中的加号图标 (**{plus-square-o}**)，然后从 **GitLab** 中或 **您的实例** (self-managed) 同一下拉菜单的部分中，选择 **新建代码片段**。
     - *对于所有其他页面，*选择顶部导航栏中的加号图标 (**{plus-square-o}**)，然后从下拉菜单中选择 **新建代码片段**。
     - 如果您安装了 [GitLab Workflow VS Code 扩展](project/repository/vscode.md)，请使用 [`Gitlab: Create snippet` 命令](https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow#create-snippet)。
   - **要创建项目片段**：转到您的项目页面。选择加号图标 (**{plus-square-o}**)，然后从下拉菜单的 **此项目** 部分选择 **新建代码片段**。
1. 添加**标题**和**说明**。
1. 使用适当的扩展名命名您的 **文件**，例如 `example.rb` 或 `index.html`。具有适当扩展名的文件名显示[语法高亮](#文件名称)。未能添加文件名可能会导致已知的复制粘贴错误。如果您不提供文件名，系统[为您创建一个名称](#文件名称)。
1. （可选）将[多个文件](#添加或删除多个文件)添加到您的代码段中。
1. 选择可见性级别，然后选择 **创建片段**。

创建片段后，您仍然可以[向其中添加更多文件](#添加或删除多个文件)。
在 13.0 及更高版本中，代码段是[默认版本](#版本化代码片段)。

## 发现代码片段

要在极狐GitLab 中发现您可见的所有片段，您可以：

- **查看您可见的所有代码段**：在实例的顶部栏上，选择 **菜单 > 代码片段** 以查看您的代码片段仪表板。
<!--- **访问 GitLab 代码段](https://gitlab.com/dashboard/snippets)** 在 GitLab.com 上查看您的代码段。-->
- **浏览所有公共片段**：在实例的顶部栏上，选择 **菜单 > 代码片段** 并选择 **浏览代码片段** 以查看所有公共片段。
- **查看项目的片段**：在您的项目中，转到 **代码片段**。

## 更改默认的代码片段可见性

默认情况下，项目片段已启用并可用。 要更改其默认可见性：

1. 在您的项目中，转到 **设置**。
1. 展开 **可见性、项目功能、权限** 部分，然后滚动到 **代码片段**。
1. 切换默认可见性，并选择是所有人都可以查看片段，还是仅项目成员可以查看片段。
1. 选择 **保存更改**。

## 版本化代码片段

<!--
> [Introduced](https://gitlab.com/groups/gitlab-org/-/epics/239) in GitLab 13.0.
-->

在 13.0 及更高版本中，代码片段（个人和项目片段）默认启用版本控制。

这意味着所有代码片段都会在创建代码片段时使用默认分支初始化自己的底层仓库。每当保存对代码片段的更改时，都会记录对默认分支的新提交。提交消息是自动生成的。该代码段的仓库只有一个分支。您不能删除此分支，也不能创建其他分支。

原有代码段在 13.0 中自动迁移。他们当前的内容被保存为对片段仓库的初始提交。

## 文件名称

代码片段支持基于为其提供的文件名和扩展名的语法突出显示。虽然您可以提交没有文件名和扩展名的代码片段，但它需要一个有效的名称，以便可以在代码片段的仓库中将内容创建为文件。

如果你没有给片段一个文件名和扩展名，系统会以 `snippetfile<x>.txt` 格式添加一个文件名，其中 `<x>` 表示添加到文件中的一个数字，从 1 开始。如果您添加更多未命名的片段。

当从早期版本升级到 13.0 时，没有支持文件名的现有片段将重命名为兼容格式。例如，如果片段的文件名是 `http://a-weird-filename.me`，它会更改为 `http-a-weird-filename-me` 保存在片段的仓库中。由于片段是按 ID 存储的，更改它们的文件名会中断指向片段的直接或嵌入链接。

## 添加或删除多个文件

> 引入于 13.5 版本。

一个片段最多可以支持 10 个文件，这有助于将相关文件放在一起，例如：

- 包含脚本及其输出的片段。
- 包含 HTML、CSS 和 JavaScript 代码的片段。
- 带有 `docker-compose.yml` 文件及其关联的 `.env` 文件的片段。
- 一个 `gulpfile.js` 文件和一个 `package.json` 文件，它们一起可用于引导项目并管理其依赖项。

如果您的代码段需要 10 个以上的文件，我们建议您创建一个 [wiki](project/wiki/index.md)。Wiki 可用于所有订阅级别的项目，[群组 wiki](project/wiki/group.md) 可用于[专业版](https://about.gitlab.cn/pricing/)。

具有多个文件的片段在片段列表<!--[片段列表](https://gitlab.com/dashboard/snippets)-->中显示文件计数：

![Example of snippet](img/snippet_tooltip_v13_10.png)

您可以使用 Git（因为它们是 Git 仓库的[版本](#版本化代码片段)）、<!--[Snippets API](../api/snippets.md)-->Snippets API 和 GitLab UI 来管理代码段。

要通过 GitLab UI 将新文件添加到您的代码段：

1. 转到 GitLab UI 中的代码段。
1. 选择右上角的 **编辑**。
1. 选择 **添加另一个文件**。
1. 在提供的表单字段中将您的内容添加到文件中。
1. 选择 **保存更改**。

要通过 GitLab UI 从代码片段中删除文件：

1. 转到 GitLab UI 中的代码段。
1. 选择右上角的 **编辑**。
1. 在您要删除的每个文件的文件名旁边选择 **删除文件**。
1. 选择 **保存更改**。

## 克隆代码片段

不是将片段复制到本地文件，您可能希望克隆片段以保留其与仓库的关系，以便您可以根据需要接收或进行更新。选择片段上的 **克隆** 按钮以显示要使用 SSH 或 HTTPS 克隆的 URL：

![Clone snippet](img/snippet_clone_button_v13_0.png)

您可以提交对克隆片段的更改，并将更改推送到极狐GitLab。

## 嵌入代码片段

可以在任何网站上共享和嵌入公共片段。您可以在多个位置重复使用极狐GitLab 代码段，并且对源的任何更改都会反映在嵌入的代码段中。嵌入后，用户可以下载它，或以原始格式查看代码段。

嵌入片段：

1. 确认您的代码段是公开可见的：
    - *如果是项目片段*，则该项目必须是公开的。
    - 该片段是公开可见的。
    - 在 **项目 > 设置 > 权限** 中，片段权限设置为 **具有访问权限的任何人**。
1. 在您的代码片段的 **嵌入** 部分，选择 **复制**，复制可以添加到任何网站或博客文章的单行脚本。例如：

   ```html
   <script src="https://gitlab.com/namespace/project/snippets/SNIPPET_ID.js"></script>
   ```

1. 将您的脚本添加到您的文件中。

嵌入的片段显示一个标题，显示：

- 文件名（如果已定义）。
- 片段大小。
- GitLab 的链接。
- 实际的片段内容。

<!--
例如：

<script src="https://gitlab.com/gitlab-org/gitlab-foss/snippets/1717978.js"></script>
-->

## 下载代码片段

您可以下载片段的原始内容。默认情况下，它们以 Linux 风格的行结尾 (`LF`) 下载。如果要保留原始行尾，则需要添加参数 `line_ending=raw`（例如：`https://gitlab.cn/snippets/SNIPPET_ID/raw?line_ending=raw`）。如果使用 GitLab Web 界面创建代码段，则原始行结尾类似于 Windows（`CRLF`）。

## 评论代码片段

使用片段，您可以参与有关该代码段的对话，这可以鼓励用户协作。

<!--
## Mark snippet as spam **(FREE SELF)**

Administrators on self-managed GitLab instances can mark snippets as spam.

Prerequisites:

- You must be the administrator for your instance.
- [Akismet](../integration/akismet.md) spam protection must be enabled on the instance.

To do this task:

1. On the top bar, select **Menu > Projects** and find your project.
1. On the left sidebar, select **Snippets**.
1. Select the snippet you want to report as spam.
1. Select **Submit as spam**.

GitLab forwards the spam to Akismet.
-->

## 故障排查

### 代码片段限制

- 不支持二进制文件。
- 不支持创建或删除分支。仅使用默认分支。
- 代码段仓库中不支持 Git 标签。
- 片段的仓库限制为 10 个文件。尝试推送超过 10 个文件会导致错误。
- 用户在 GitLab UI 上看不到修订，但存在更新问题。
- <!--[片段的最大大小](../administration/snippets/index.md#snippets-content-size-limit)--> 默认为 50 MB。
- 不支持 Git LFS。

### 减小代码片段仓库大小

由于版本化片段被视为命名空间存储大小<!--[命名空间存储大小](../user/admin_area/settings/account_and_limit_settings.md)-->的一部分，因此建议使片段的仓库尽可能小。

有关压缩仓库的工具的更多信息，请参阅有关 [减少仓库大小](../user/project/repository/reducing_the_repo_size_using_git.md) 的文档。

### 无法在片段文本框中输入文本

如果文件名字段后的文本区域被禁用并阻止您创建新代码段，请使用以下解决方法：

1. 为您的代码段输入标题。
1. 滚动到 **文件** 字段的底部，然后选择 **添加另一个文件**，显示第二组字段以添加第二个文件。
1. 在第二个文件的文件名字段中，输入文件名以避免已知的复制粘贴错误。
1. 在第二个文件的文本区域中输入任何字符串。
1. 滚动回第一个文件名，然后选择 **删除文件**。
1. 创建文件的其余部分，完成后选择 **创建代码片段**。
