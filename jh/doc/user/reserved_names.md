---
stage: Manage
group: Workspace
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 保留的项目和群组名称

并非所有项目和群组名称都被允许，因为它们会与极狐GitLab 使用的现有路由冲突。

不允许用作组名或项目名的词列表见 [`path_regex.rb`文件](https://gitlab.cn/gitlab-cn/gitlab/-/blob/master/lib/gitlab/path_regex.rb)下的 `TOP_LEVEL_ROUTES`、`PROJECT_WILDCARD_ROUTES` 和 `GROUP_ROUTES` 列表：

- `TOP_LEVEL_ROUTES`：为用户名或顶级群组保留的名称。
- `PROJECT_WILDCARD_ROUTES`：为子组或项目保留的名称。
- `GROUP_ROUTES`：为所有组或项目保留的名称。

## 保留的项目名称

目前无法创建具有以下名称的项目：

- `\-`
- `badges`
- `blame`
- `blob`
- `builds`
- `commits`
- `create`
- `create_dir`
- `edit`
- `environments/folders`
- `files`
- `find_file`
- `gitlab-lfs/objects`
- `info/lfs/objects`
- `new`
- `preview`
- `raw`
- `refs`
- `tree`
- `update`
- `wikis`

## 保留的群组名称

目前，以下名称保留为顶级群组：

- `\-`
- `.well-known`
- `404.html`
- `422.html`
- `500.html`
- `502.html`
- `503.html`
- `admin`
- `api`
- `apple-touch-icon-precomposed.png`
- `apple-touch-icon.png`
- `assets`
- `dashboard`
- `deploy.html`
- `explore`
- `favicon.ico`
- `favicon.png`
- `files`
- `groups`
- `health_check`
- `help`
- `import`
- `jwt`
- `login`
- `oauth`
- `profile`
- `projects`
- `public`
- `robots.txt`
- `s`
- `search`
- `sitemap`
- `sitemap.xml`
- `sitemap.xml.gz`
- `slash-command-logo.png`
- `snippets`
- `unsubscribes`
- `uploads`
- `users`
- `v2`

以下群组名不能用作子组名：

- `\-`
