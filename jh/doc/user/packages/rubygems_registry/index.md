---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 软件包库中的 Ruby gems **(FREE)**

> 引入于 13.10 版本。

WARNING:
Ruby gems 软件包库正在开发中，由于功能有限，尚未准备好用于生产。<!--This [epic](https://gitlab.com/groups/gitlab-org/-/epics/3200) details the remaining
work and timelines to make it production ready.-->

您可以在项目的软件包库中发布 Ruby gem，然后在需要将它们用作依赖项时安装它们。尽管您可以将 gem 推送到软件包库，但不能从库里安装它们。但是，您可以直接从软件宝库的 UI 下载 `gem` 文件，或者使用 API<!--[API](../../../api/packages/rubygems.md#download-a-gem-file)-->。

<!--
For documentation of the specific API endpoints that the Ruby gems and Bundler package manager
clients use, see the [Ruby gems API documentation](../../../api/packages/rubygems.md).
-->

## 启用 Ruby gems 库

Ruby gems 库背后有一个默认禁用的功能标志。可以访问 GitLab Rails 控制台的 GitLab 管理员可以为您的实例启用此库。

启用：

```ruby
Feature.enable(:rubygem_packages)
```

禁用：

```ruby
Feature.disable(:rubygem_packages)
```

为特定项目启用或禁用：

```ruby
Feature.enable(:rubygem_packages, Project.find(1))
Feature.disable(:rubygem_packages, Project.find(2))
```

## 创建 Ruby gem

如果您在创建 Ruby gem 时需要帮助，请参阅 [RubyGems 文档](https://guides.rubygems.org/make-your-own-gem/)。

## 向软件包库进行身份验证

在推送到软件包库之前，您必须进行身份验证。

为此，您可以使用：

- [个人访问令牌](../../../user/profile/personal_access_tokens.md)，范围设置为 `api`。
- 部署令牌<!--[部署令牌](../../project/deploy_tokens/index.md)-->，其范围设置为 `read_package_registry`、`write_package_registry` 或两者均有。
- [CI 作业令牌](#使用-ci-作业令牌进行身份验证)。

### 使用个人访问令牌或部署令牌进行身份验证

要使用个人访问令牌进行身份验证，请创建或编辑 `~/.gem/credentials` 文件并添加：

```ini
---
https://gitlab.example.com/api/v4/projects/<project_id>/packages/rubygems: '<your token>'
```

- `<your token>` 必须是您的个人访问令牌或部署令牌的令牌值。
- 您的项目 ID 在您的项目主页上。

### 使用 CI 作业令牌进行身份验证

要在 [GitLab CI/CD](../../../ci/index.md) 中使用 RubyGems 命令，您可以使用 `CI_JOB_TOKEN` 代替个人访问令牌或部署令牌。

例如：

```yaml
# assuming a my_gem.gemspec file is present in the repository with the version currently set to 0.0.1
image: ruby

run:
  before_script:
    - mkdir ~/.gem
    - echo "---" > ~/.gem/credentials
    - |
      echo "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/rubygems: '${CI_JOB_TOKEN}'" >> ~/.gem/credentials
    - chmod 0600 ~/.gem/credentials # rubygems requires 0600 permissions on the credentials file
  script:
    - gem build my_gem
    - gem push my_gem-0.0.1.gem --host ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/rubygems
```

您还可以在检入极狐GitLab 的 `~/.gem/credentials` 文件中使用 `CI_JOB_TOKEN`：

```ini
---
https://gitlab.example.com/api/v4/projects/${env.CI_PROJECT_ID}/packages/rubygems: '${env.CI_JOB_TOKEN}'
```

## 推送 Ruby gem

先决条件：

- 您必须[向软件包库进行身份验证](#向软件包库进行身份验证)。
- 允许的最大 gem 大小为 3 GB。

要推送您的 gem，请运行如下命令：

```shell
gem push my_gem-0.0.1.gem --host <host>
```

请注意，`<host>` 是您在设置身份验证时使用的 URL。例如：

```shell
gem push my_gem-0.0.1.gem --host https://gitlab.example.com/api/v4/projects/1/packages/rubygems
```

此消息表示 gem 上传成功：

```plaintext
Pushing gem to https://gitlab.example.com/api/v4/projects/1/packages/rubygems...
{"message":"201 Created"}
```

要查看已发布的 gem，请转到您项目的 **软件包和镜像库** 页面。推送到极狐GitLab 的 Gems 不会立即显示在您项目的 Packages UI 中。处理 gem 最多可能需要 10 分钟。

### 推送具有相同名称或版本的 gem

如果已存在具有相同名称和版本的包，您可以推送 gem。
两者都在 UI 中可见且可访问。但是，只有最近推送的 gem 用于安装。

## 安装 Ruby gem

GitLab 的 Ruby gems 库正在开发中，尚未准备好用于生产。您不能从库中安装 Gems。但是，您可以直接从 UI 或使用 API<!--[API](../../../api/packages/rubygems.md#download-a-gem-file)--> 下载 `.gem` 文件。
