---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 软件包库中的 Maven 包 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/5811) in GitLab 11.3.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/221259) from GitLab Premium to GitLab Free in 13.3.
-->

在项目的软件包库中发布 [Maven](https://maven.apache.org) 产物。
然后，在需要将它们用作依赖项时安装它们。

<!--
For documentation of the specific API endpoints that the Maven package manager
client uses, see the [Maven API documentation](../../../api/packages/maven.md).
-->

## 构建 Maven 包

本节介绍如何安装 Maven 和构建包。

如果您已经使用 Maven 并且知道如何构建自己的包，请转到[下一节](#使用-maven-对软件包库进行身份验证)。

Maven 仓库也适用于 Gradle。要设置 Gradle 项目，请参阅 [Gradle 入门](#使用-gradle-构建-java-项目)。

### 安装 Maven

所需的最低版本是：

- Java 11.0.5+
- Maven 3.6+

按照 [maven.apache.org](https://maven.apache.org/install.html) 上的说明为您的本地开发环境下载并安装 Maven。安装完成后，通过运行以下命令验证您可以在终端中使用 Maven：

```shell
mvn --version
```

输出应类似于：

```shell
Apache Maven 3.6.1 (d66c9c0b3152b2e69ee9bac180bb8fcc8e6af555; 2019-04-04T20:00:29+01:00)
Maven home: /Users/<your_user>/apache-maven-3.6.1
Java version: 12.0.2, vendor: Oracle Corporation, runtime: /Library/Java/JavaVirtualMachines/jdk-12.0.2.jdk/Contents/Home
Default locale: en_GB, platform encoding: UTF-8
OS name: "mac os x", version: "10.15.2", arch: "x86_64", family: "mac"
```

### 创建项目

按照以下步骤创建可发布到极狐GitLab 软件包库的 Maven 项目。

1. 打开您的终端并创建一个目录来存储项目。
1. 在新目录中，运行此 Maven 命令以初始化一个新包：

   ```shell
   mvn archetype:generate -DgroupId=com.mycompany.mydepartment -DartifactId=my-project -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
   ```

   参数说明：

   - `DgroupId`：标识您的包的唯一字符串。遵循 [Maven 命名约定](https://maven.apache.org/guides/mini/guide-naming-conventions.html)。    
   - `DartifactId`：`JAR` 的名称，附加到 `DgroupId` 的末尾。
   - `DarchetypeArtifactId`：用于创建项目初始结构的原型。
   - `DinteractiveMode`：使用批处理模式创建项目（可选）。

此消息表示项目已成功设置：

```shell
...
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  3.429 s
[INFO] Finished at: 2020-01-28T11:47:04Z
[INFO] ------------------------------------------------------------------------
```

在您运行命令的文件夹中，应显示一个新目录。
目录名称应与 `DartifactId` 参数匹配，在本例中为 `my-project`。

## 使用 Gradle 构建 Java 项目

本节介绍如何安装 Gradle 和初始化 Java 项目。

如果您已经使用 Gradle 并且知道如何构建自己的包，请转到[下一节](#使用-maven-对软件包库进行身份验证)。

### 安装 Gradle

如果要创建新的 Gradle 项目，则必须安装 Gradle。按照 [gradle.org](https://gradle.org/install/) 上的说明为您的本地开发环境下载和安装 Gradle。

在您的终端中，通过运行以下命令验证您是否可以使用 Gradle：

```shell
gradle -version
```

要使用现有的 Gradle 项目，在项目目录中，在 Linux 上执行 `gradlew`，或在 Windows 上执行 `gradlew.bat`。

输出应类似于：

```plaintext
------------------------------------------------------------
Gradle 6.0.1
------------------------------------------------------------

Build time:   2019-11-18 20:25:01 UTC
Revision:     fad121066a68c4701acd362daf4287a7c309a0f5

Kotlin:       1.3.50
Groovy:       2.5.8
Ant:          Apache Ant(TM) version 1.10.7 compiled on September 1 2019
JVM:          11.0.5 (Oracle Corporation 11.0.5+10)
OS:           Windows 10 10.0 amd64
```

### 创建 Java 项目

<!--
按照以下步骤创建可发布到极狐GitLab 软件包库的 Maven 项目。
-->

1. 打开您的终端并创建一个目录来存储项目。
1. 从这个新目录，运行这个 Maven 命令来初始化一个新包：

   ```shell
   gradle init
   ```

   输出应该是：

   ```plaintext
   Select type of project to generate:
     1: basic
     2: application
     3: library
     4: Gradle plugin
   Enter selection (default: basic) [1..4]
   ```

1. 输入`3`来创建一个新的库项目。输出应该是：

   ```plaintext
   Select implementation language:
     1: C++
     2: Groovy
     3: Java
     4: Kotlin
     5: Scala
     6: Swift
   ```

1. 输入 `3` 以创建一个新的 Java 库项目。输出应该是：

   ```plaintext
   Select build script DSL:
     1: Groovy
     2: Kotlin
   Enter selection (default: Groovy) [1..2]
   ```

1. 输入 `1`，创建一个在 Groovy DSL 中描述的新 Java 库项目。输出应该是：

   ```plaintext
   Select test framework:
     1: JUnit 4
     2: TestNG
     3: Spock
     4: JUnit Jupiter
   ```

1. 输入 `1`，使用 JUnit 4 测试库初始化项目。输出应该是：

   ```plaintext
   Project name (default: test):
   ```

1. 输入项目名称或按 Enter，使用目录名称作为项目名称。

## 使用 Maven 对软件包库进行身份验证

要对软件包库进行身份验证，您需要以下其中一项：

- 一个[个人访问令牌](../../../user/profile/personal_access_tokens.md)，范围设置为 `api`。
- 一个部署令牌<!--[部署令牌](../../project/deploy_tokens/index.md)-->，其范围设置为 `read_package_registry`、`write_package_registry` 或两者都有。
- [CI_JOB_TOKEN](#使用-maven-中的-ci-作业令牌进行身份验证)。

### 在 Maven 中使用个人访问令牌进行身份验证

要使用个人访问令牌，请将此部分添加到您的 [`settings.xml`](https://maven.apache.org/settings.html) 文件中。

`name` 必须是 `Private-Token`。

```xml
<settings>
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Private-Token</name>
            <value>REPLACE_WITH_YOUR_PERSONAL_ACCESS_TOKEN</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```

### 在 Maven 中使用部署令牌进行身份验证

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/213566) deploy token authentication in GitLab 13.0.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/221259) from GitLab Premium to GitLab Free in 13.3.
-->

要使用部署令牌，请将此部分添加到您的 [`settings.xml`](https://maven.apache.org/settings.html) 文件中。

`name` 必须是 `Deploy-Token`。

```xml
<settings>
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Deploy-Token</name>
            <value>REPLACE_WITH_YOUR_DEPLOY_TOKEN</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```

### 使用 Maven 中的 CI 作业令牌进行身份验证

要使用 CI 作业令牌进行身份验证，请将此部分添加到您的 [`settings.xml`](https://maven.apache.org/settings.html) 文件中。

`name` 必须是 `Job-Token`。

```xml
<settings>
  <servers>
    <server>
      <id>gitlab-maven</id>
      <configuration>
        <httpHeaders>
          <property>
            <name>Job-Token</name>
            <value>${env.CI_JOB_TOKEN}</value>
          </property>
        </httpHeaders>
      </configuration>
    </server>
  </servers>
</settings>
```

阅读更多关于 [如何使用 GitLab CI/CD 创建 Maven 包](#使用-gitlab-cicd-创建-maven-软件包)。

## 使用 Gradle 对软件包库进行身份验证

要对软件包库进行身份验证，您需要个人访问令牌或部署令牌。

- 如果您使用[个人访问令牌](../../../user/profile/personal_access_tokens.md)，请将范围设置为 `api`。
- 如果您使用部署令牌<!--[部署令牌](../../project/deploy_tokens/index.md)-->，请将范围设置为 `read_package_registry`、`write_package_registry` 或两者都有。

### 在 Gradle 中使用个人访问令牌进行身份验证

创建一个文件`~/.gradle/gradle.properties`，内容如下：

```groovy
gitLabPrivateToken=REPLACE_WITH_YOUR_PERSONAL_ACCESS_TOKEN
```

将 `repositories` 部分添加到您的 [`build.gradle`](https://docs.gradle.org/current/userguide/tutorial_using_tasks.html)
文件：

```groovy
repositories {
    maven {
        url "https://gitlab.example.com/api/v4/groups/<group>/-/packages/maven"
        name "GitLab"
        credentials(HttpHeaderCredentials) {
            name = 'Private-Token'
            value = gitLabPrivateToken
        }
        authentication {
            header(HttpHeaderAuthentication)
        }
    }
}
```

### 在 Gradle 中使用部署令牌进行身份验证

要使用部署令牌进行身份验证，请将 `repositories` 部分添加到您的 [`build.gradle`](https://docs.gradle.org/current/userguide/tutorial_using_tasks.html) 文件：

```groovy
repositories {
    maven {
        url "https://gitlab.example.com/api/v4/groups/<group>/-/packages/maven"
        name "GitLab"
        credentials(HttpHeaderCredentials) {
            name = 'Deploy-Token'
            value = '<deploy-token>'
        }
        authentication {
            header(HttpHeaderAuthentication)
        }
    }
}
```

### 在 Gradle 中使用 CI 作业令牌进行身份验证

要使用 CI 作业令牌进行身份验证，请将 `repositories` 部分添加到您的 [`build.gradle`](https://docs.gradle.org/current/userguide/tutorial_using_tasks.html) 文件：

```groovy
repositories {
    maven {
        url "${CI_API_V4_URL}/groups/<group>/-/packages/maven"
        name "GitLab"
        credentials(HttpHeaderCredentials) {
            name = 'Job-Token'
            value = System.getenv("CI_JOB_TOKEN")
        }
        authentication {
            header(HttpHeaderAuthentication)
        }
    }
}
```

## 将极狐GitLab 端点用于 Maven 包

要将极狐GitLab 端点用于 Maven 包，请选择一个选项：

- **项目级**：要将 Maven 包发布到项目，请使用项目级端点。要安装 Maven 包，当您的 Maven 包很少并且它们不在同一个极狐GitLab 群组中时，请使用项目级端点。
- **群组级**：当您想从同一个极狐GitLab 群组中的许多不同项目安装软件包时，使用群组级端点。
- **实例级**：当您想要从不同的极狐GitLab 群组或在它们自己的命名空间中安装许多软件包时，请使用实例级端点。

您选择的选项决定了您添加到“pom.xml”文件中的设置。

在所有情况下，要发布包，您需要：

- `distributionManagement` 部分中的项目特定 URL。
- `repository` 和 `distributionManagement` 部分。

### 项目级 Maven 端点

Maven 中 `pom.xml` 的相关 `repository` 部分应该如下所示：

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </repository>
</repositories>
<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </repository>
  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

Gradle 中的相应部分是：

```groovy
repositories {
    maven {
        url "https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven"
        name "GitLab"
    }
}
```

- `id` 是您[在 `settings.xml` 中定义的](#使用-maven-对软件包库进行身份验证)。
- `PROJECT_ID` 是您的项目 ID，您可以在项目主页上查看。
- 用您的域名替换 `gitlab.example.com`。
- 要检索产物，请使用项目的 URL-encoded<!--[URL-encoded](../../../api/index.md#namespaced-path-encoding)--> 路径（如 `group%2Fproject`）或项目的 ID（如 `42`）。但是，只有项目的 ID 可以用于发布。

### 群组级 Maven 端点

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/8798) in GitLab 11.7.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/221259) from GitLab Premium to GitLab Free in 13.3.
-->

如果您依赖多个包，则将每个包的唯一 URL 包含在 `repository` 部分中可能效率低下。相反，您可以将群组级别端点用于存储在一个极狐GitLab 群组中的所有 Maven 包。只有您有权访问的软件包可供下载。

群组级端点适用于任何包名称，因此与[实例级端点](#实例级-maven-端点)相比，您在命名方面具有更大的灵活性。
但是，极狐GitLab 不保证群组内包名的唯一性。您可以有两个具有相同包名称和包版本的项目。此时，极狐GitLab 识别更新的。

此例显示了您的 `pom.xml` 文件的相关 `repository` 部分。
您仍然需要一个项目特定的 URL 来在 `distributionManagement` 部分发布一个包：

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/groups/GROUP_ID/-/packages/maven</url>
  </repository>
</repositories>
<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </repository>
  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

对于 Gradle，相应的 `repositories` 部分将如下所示：

```groovy
repositories {
    maven {
        url "https://gitlab.example.com/api/v4/groups/GROUP_ID/-/packages/maven"
        name "GitLab"
    }
}
```

- 对于 `id`，使用您[在 `settings.xml`中定义的](#使用-maven-对软件包库进行身份验证)。
- 对于 `GROUP_ID`，请使用您的群组ID，您可以在群组主页上查看该 ID。
- 对于 `PROJECT_ID`，请使用您的项目 ID，您可以在项目主页上查看该 ID。
- 用您的域名替换 `gitlab.example.com`。
- 要检索产物，请使用群组的 URL-encoded<!--[URL-encoded](../../../api/index.md#namespaced-path-encoding)--> 路径（如 `group%2Fsubgroup`）或群组的 ID（如 `12`）。

### 实例级 Maven 端点

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/8274) in GitLab 11.7.
> - [Moved](https://gitlab.com/gitlab-org/gitlab/-/issues/221259) from GitLab Premium to GitLab Free in 13.3.
-->

如果您依赖多个包，则将每个包的唯一 URL 包含在 `repository` 部分中可能效率低下。相反，您可以为极狐GitLab 中存储的所有 Maven 包使用实例级端点。您可以访问的所有软件包都可供下载。

**仅与项目路径相同的包**由实例级端点公开。

| 项目 | 包 | 实例级端点可用 |
| ------- | ------- | --------------------------------- |
| `foo/bar`           | `foo/bar/1.0-SNAPSHOT`           | Yes |
| `gitlab-org/gitlab` | `foo/bar/1.0-SNAPSHOT`           | No  |
| `gitlab-org/gitlab` | `gitlab-org/gitlab/1.0-SNAPSHOT` | Yes |

这个例子展示了你的 `pom.xml` 的 `repository` 部分的相关性。
您仍然需要在 `distributionManagement` 部分中有一个特定于项目的 URL。

```xml
<repositories>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/packages/maven</url>
  </repository>
</repositories>
<distributionManagement>
  <repository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </repository>
  <snapshotRepository>
    <id>gitlab-maven</id>
    <url>https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven</url>
  </snapshotRepository>
</distributionManagement>
```

Gradle 中相应的仓库部分如下所示：

```groovy
repositories {
    maven {
        url "https://gitlab.example.com/api/v4/packages/maven"
        name "GitLab"
    }
}
```

- `id` 是您[在 `settings.xml` 中定义的](#使用-maven-对软件包库进行身份验证)。
- `PROJECT_ID` 是您的项目 ID，您可以在项目主页上查看。
- 用您的域名替换 `gitlab.example.com`。
- 要检索产物，请使用项目的 URL-encoded<!--[URL-encoded](../../../api/index.md#namespaced-path-encoding)--> 路径（如 `group%2Fproject`）或项目的 ID（如 `42`）。但是，只有项目的 ID 可以用于发布。

## 发布包

在您设置了[远端和身份验证](#使用-maven-对软件包库进行身份验证)，以及[配置您的项目](#将极狐gitlab-端点用于-maven-包)后，将 Maven 包发布到您的项目。

### 使用 Maven 发布

要使用 Maven 发布包：

```shell
mvn deploy
```

如果部署成功，则应显示构建成功消息：

```shell
...
[INFO] BUILD SUCCESS
...
```

该消息还应显示包已发布到正确的位置：

```shell
Uploading to gitlab-maven: https://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven/com/mycompany/mydepartment/my-project/1.0-SNAPSHOT/my-project-1.0-20200128.120857-1.jar
```

### 使用 Gradle 发布

使用 Gradle 发布包：

1. 在插件部分添加 Gradle 插件 [`maven-publish`](https://docs.gradle.org/current/userguide/publishing_maven.html)：

   ```groovy
   plugins {
       id 'java'
       id 'maven-publish'
   }
   ```

1. 添加一个 `publishing` 部分：

   ```groovy
   publishing {
       publications {
           library(MavenPublication) {
               from components.java
           }
       }
       repositories {
           maven {
               url "https://gitlab.example.com/api/v4/projects/<PROJECT_ID>/packages/maven"
               credentials(HttpHeaderCredentials) {
                   name = "Private-Token"
                   value = gitLabPrivateToken // the variable resides in ~/.gradle/gradle.properties
               }
               authentication {
                   header(HttpHeaderAuthentication)
               }
           }
       }
   }
   ```

1. 将 `PROJECT_ID` 替换为您的项目 ID，该 ID 可以在您的项目主页上找到。

1. 运行发布任务：

   ```shell
   gradle publish
   ```

现在导航到您项目的 **软件包与镜像库** 页面并查看已发布的产物。

### 发布具有相同名称或版本的包

当您发布与现有包具有相同名称和版本的包时，新的包文件将添加到现有包中。您仍然可以使用 UI 或 API 来访问和查看现有包的旧文件。

要删除这些较旧的包版本，请考虑使用包 API 或 UI。

#### 不允许重复的 Maven 包

> 引入于 13.9 版本。

为了防止用户发布重复的 Maven 包，您可以使用 GraphQl API<!--[GraphQl API](../../../api/graphql/reference/index.md#packagesettings)--> 或 UI。

在用户界面中：

1. 对于您的群组，转到 **设置 > 软件包与镜像库**。
1. 展开 **软件包库** 部分。
1. 打开 **不允许重复** 开关。
1. 可选。要允许某些重复的包，在 **例外** 框中，输入与您要允许的包的名称和/或版本匹配的正则表达式 pattern。

您的更改会自动保存。

## 安装软件包

要从极狐GitLab 软件包库安装软件包，您必须配置[远端和身份验证](#使用-maven-对软件包库进行身份验证)。
完成后，您可以从项目、群组或命名空间安装软件包。

如果多个包具有相同的名称和版本，则在安装包时，将检索最近发布的包。

### 使用 Maven `mvn install`

要使用 `mvn install` 安装软件包：

1. 手动将依赖项添加到您的项目 `pom.xml` 文件中。要添加之前创建的示例，XML：

   ```xml
   <dependency>
     <groupId>com.mycompany.mydepartment</groupId>
     <artifactId>my-project</artifactId>
     <version>1.0-SNAPSHOT</version>
   </dependency>
   ```

1. 在您的项目中，运行以下命令：

   ```shell
   mvn install
   ```

该消息应显示正在从软件包库下载软件包：

```shell
Downloading from gitlab-maven: http://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven/com/mycompany/mydepartment/my-project/1.0-SNAPSHOT/my-project-1.0-20200128.120857-1.pom
```

### 使用 Maven `mvn dependency:get`

您可以直接使用 Maven 命令安装软件包。

1. 在您的项目目录中，运行：

   ```shell
   mvn dependency:get -Dartifact=com.nickkipling.app:nick-test-app:1.1-SNAPSHOT
   ```

该消息应显示正在从软件包库下载软件包：

```shell
Downloading from gitlab-maven: http://gitlab.example.com/api/v4/projects/PROJECT_ID/packages/maven/com/mycompany/mydepartment/my-project/1.0-SNAPSHOT/my-project-1.0-20200128.120857-1.pom
```

NOTE:
在 UI 中，在 Maven 的软件包库页面上，您可以查看和复制这些命令。

### 使用 Gradle

将 [dependency](https://docs.gradle.org/current/userguide/declaring_dependencies.html) 添加到依赖项部分的 `build.gradle` 中：

```groovy
dependencies {
    implementation 'com.mycompany.mydepartment:my-project:1.0-SNAPSHOT'
}
```

## 删除软件包

对于您的项目，转到 **软件包与镜像库 > 软件包库**。

要删除软件包，请单击红色垃圾桶图标，或者从软件包详细信息中单击 **删除** 按钮。

## 使用 GitLab CI/CD 创建 Maven 软件包

将仓库配置为使用 Maven 软件包库后，您可以配置 GitLab CI/CD 以自动构建新包。

### 使用 Maven 和 GitLab CI/CD 创建 Maven 包

每次 `main` 分支更新时，您都可以创建一个新包。

1. 创建一个 `ci_settings.xml` 文件，作为 Maven 的 `settings.xml` 文件。

1. 添加具有您在 `pom.xml` 文件中定义的相同 ID 的 `server` 部分。例如，使用 `gitlab-maven` 作为 ID：

   ```xml
   <settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
     xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
     <servers>
       <server>
         <id>gitlab-maven</id>
         <configuration>
           <httpHeaders>
             <property>
               <name>Job-Token</name>
               <value>${env.CI_JOB_TOKEN}</value>
             </property>
           </httpHeaders>
         </configuration>
       </server>
     </servers>
   </settings>
   ```

1. 确保您的 `pom.xml` 文件包含以下内容。您可以让 Maven 使用[预定义的 CI/CD 变量](../../../ci/variables/predefined_variables.md)，如本例所示，或者您可以硬编码服务器的主机名和项目的 ID。

   ```xml
   <repositories>
     <repository>
       <id>gitlab-maven</id>
       <url>${env.CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}/packages/maven</url>
     </repository>
   </repositories>
   <distributionManagement>
     <repository>
       <id>gitlab-maven</id>
       <url>${CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}/packages/maven</url>
     </repository>
     <snapshotRepository>
       <id>gitlab-maven</id>
       <url>${CI_API_V4_URL}/projects/${env.CI_PROJECT_ID}/packages/maven</url>
     </snapshotRepository>
   </distributionManagement>
   ```

1. 在您的 `.gitlab-ci.yml` 文件中添加一个 `deploy` 作业：

   ```yaml
   deploy:
     image: maven:3.6-jdk-11
     script:
       - 'mvn deploy -s ci_settings.xml'
     only:
       - main
   ```

1. 将这些文件推送到您的仓库。

下次运行 `deploy` 作业时，它会将 `ci_settings.xml` 复制到用户的 home 位置。在此例中：

- 用户是 `root`，因为作业在 Docker 容器中运行。
- Maven 使用配置的 CI/CD 变量。

### 使用 Gradle 和 GitLab CI/CD 创建 Maven 包

您可以在每次 `main` 分支更新时创建一个包。

1. 使用 [Gradle 中的 CI 作业令牌](#在-gradle-中使用-ci-作业令牌进行身份验证)进行身份验证。

1. 在您的 `.gitlab-ci.yml` 文件中添加一个 `deploy` 作业：

   ```yaml
   deploy:
     image: gradle:6.5-jdk11
     script:
       - 'gradle publish'
     only:
       - main
   ```

1. 将文件提交到您的仓库。

当流水线成功时，软件包被创建。

### 版本验证

版本字符串使用以下正则表达式进行验证。

```ruby
\A(?!.*\.\.)[\w+.-]+\z
```

您可以使用正则表达式并在[此正则表达式编辑器](https://rubular.com/r/rrLQqUXjfKEoL6)上尝试您的版本字符串。

## 故障排查

为了提高性能，Maven 缓存与包相关的文件。如果遇到问题，请使用以下命令清除缓存：

```shell
rm -rf ~/.m2/repository
```

如果您使用 Gradle，请运行以下命令清除缓存：

```shell
rm -rf ~/.gradle/caches
```

### 查看网络跟踪日志

如果您在使用 Maven 仓库时遇到问题，您可能需要查看网络跟踪日志。

例如，尝试使用 PAT 令牌在本地运行 `mvn deploy` 并使用以下选项：

```shell
mvn deploy \
-Dorg.slf4j.simpleLogger.log.org.apache.maven.wagon.providers.http.httpclient=trace \
-Dorg.slf4j.simpleLogger.log.org.apache.maven.wagon.providers.http.httpclient.wire=trace
```

WARNING:
当您设置这些选项时，所有网络请求都会被记录并生成大量输出。

### 有用的 Maven 命令行选项

在使用 GitLab CI/CD 执行任务时，您可以使用一些 [Maven 命令行选项](https://maven.apache.org/ref/current/maven-embedder/cli.html)。

- 文件传输进度可能会使 CI 日志难以阅读。在 [3.6.1](https://maven.apache.org/docs/3.6.1/release-notes.html#User_visible_Changes) 中添加了选项 `-ntp,--no-transfer-progress`。或者，查看 `-B,--batch-mode` [或较低级别的日志记录更改。](https://stackoverflow.com/questions/21638697/disable-maven-download-progress-indication)

- 指定在哪里可以找到 `pom.xml` 文件（`-f,--file`）：

   ```yaml
   package:
     script:
       - 'mvn --no-transfer-progress -f helloworld/pom.xml package'
   ```

- 指定在哪里可以找到用户设置 (`-s,--settings`) 而非[默认位置](https://maven.apache.org/settings.html)，也可以使用 `-gs,--global-settings` 选项：

   ```yaml
   package:
     script:
       - 'mvn -s settings/ci.xml package'
   ```

### 验证您的 Maven 设置

如果您在 CI/CD 中遇到与 `settings.xml` 文件相关的问题，请尝试添加额外的脚本任务或作业以 [验证有效设置](https://maven.apache.org/plugins/maven-help-plugin/effective-settings-mojo.html)。

帮助插件还可以提供[系统属性](https://maven.apache.org/plugins/maven-help-plugin/system-mojo.html)，包括环境变量：

```yaml
mvn-settings:
  script:
    - 'mvn help:effective-settings'

package:
  script:
    - 'mvn help:system'
    - 'mvn package'
```

## 支持的 CLI 命令

GitLab Maven 存储库支持以下 Maven CLI 命令：

- `mvn deploy`：将您的包发布到软件包库。
- `mvn install`：安装在您的 Maven 项目中指定的包。
- `mvn dependency:get`：安装特定的包。
