---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 极狐GitLab 通用软件包存储库 **(FREE)**

> - 引入于 13.5 版本。
> - 部署在功能标志后默认启用。
> - 可以为每个项目启用或禁用它。
> - 推荐用于生产。
> - 对于自助自我管理的实例，GitLab 管理员可以选择[禁用它](#在软件包库中启用或禁用通用包)。

WARNING:
您可能无法使用此功能。查看上面的**版本历史**注释以了解详细信息。

在项目的软件包库中发布通用文件，如发布二进制文件。然后，在需要将它们用作依赖项时安装它们。

## 向软件包库进行身份验证

要对软件包库进行身份验证，您需要一个个人访问令牌<!--[个人访问令牌](../../../api/index.md#personalproject-access-tokens)-->，[CI/CD 作业令牌](../../../ci/jobs/ci_job_token.md)或部署令牌<!--[部署令牌](../../project/deploy_tokens/index.md)-->。

<!--
除了标准 API 身份验证机制之外，通用包 API 还允许使用 HTTP 基本身份验证进行身份验证，以便与不支持其他可用机制的工具一起使用。`user-id` 没有被检查并且可以是任何值，并且 `password` 必须是一个[personal access token](../../../api/index.md#personalproject-access-tokens)、[CI/CD 作业令牌](../../../ci/jobs/ci_job_token.md) 或 [deploy token](../../project/deploy_tokens/index.md) .
-->

## 发布包文件

发布包文件时，如果该包不存在，则会创建该包。

先决条件：

- 您必须使用 API 进行身份验证<!--[使用 API 进行身份验证](../../../api/index.md#authentication)-->。如果使用部署令牌进行身份验证，则必须使用 `write_package_registry` 范围进行配置。如果使用个人访问令牌或项目访问令牌进行身份验证，则必须使用 `api` 范围进行配置。

```plaintext
PUT /projects/:id/packages/generic/:package_name/:package_version/:file_name?status=:status
```

| 参数          | 类型            | 是否必需 | 描述                                                                                                                      |
| -------------------| --------------- | ---------| -------------------------------------------------------------------------------------------------------------------------------- |
| `id`               | integer/string  | yes      | ID 或项目的 URL 编码路径<!--[项目的 URL 编码路径](../../../api/index.md#namespaced-path-encoding)-->。                                              |
| `package_name`     | string          | yes      | 包名。只能包含小写字母（`az`）、大写字母（`AZ`）、数字（`0-9`）、点（`.`）、连字符（`-`）或下划线（`_`） .
| `package_version`  | string          | yes      | 包版本。以下正则表达式验证了这一点：`\A(\.?[\w\+-]+\.?)+\z`。 您可以在 [Rubular](https://rubular.com/r/aNCV0wG5K14uq8) 上测试您的版本字符串。
| `file_name`        | string          | yes      | 文件名。它只能包含小写字母（`az`）、大写字母（`AZ`）、数字（`0-9`）、点（`.`）、连字符（`-`）或下划线（`_`）。
| `status`           | string          | no       | 包状态。可以是 `default`（默认）或 `hidden`。隐藏的包不会出现在 UI 或<!--[包 API 列表端点](../../../api/packages.md)-->包 API 列表端点中。
| `select`           | string          | no       | 响应负载。默认情况下，响应为空。 有效值为：`package_file`。 `package_file` 返回由此请求创建的包文件记录的详细信息。

在请求正文中提供文件上下文。

使用个人访问令牌的示例请求：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     --upload-file path/to/file.txt \
     "https://gitlab.example.com/api/v4/projects/24/packages/generic/my_package/0.0.1/file.txt?status=hidden"
```

没有属性 `select` 的示例响应：

```json
{
  "message":"201 Created"
}
```

具有属性 `select = package_file` 的示例响应：

```json
{
  "id": 1,
  "package_id": 1,
  "created_at": "2021-10-12T12:05:23.387Z",
  "updated_at": "2021-10-12T12:05:23.387Z",
  "size": 0,
  "file_store": 1,
  "file_md5": null,
  "file_sha1": null,
  "file_name": "file.txt",
  "file": {
    "url": "/6b/86/6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b/packages/26/files/36/file.txt"
  },
  "file_sha256": "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
  "verification_retry_at": null,
  "verified_at": null,
  "verification_failure": null,
  "verification_retry_count": null,
  "verification_checksum": null,
  "verification_state": 0,
  "verification_started_at": null,
  "new_file_path": null
}
```

### 发布具有相同名称或版本的包

当您发布与现有包具有相同名称和版本的包时，新的包文件将添加到现有包中。您仍然可以使用 UI 或 API 来访问和查看现有包的旧文件。要删除这些较旧的包修订版，请考虑使用包 API 或 UI。

#### 不允许重复的通用包

> 引入于 13.12 版本。

为了防止用户发布重复的通用包，您可以使用 GraphQl API<!--[GraphQl API](../../../api/graphql/reference/index.md#packagesettings)--> 或 UI。

在用户界面中：

1. 对于您的群组，转到 **设置 > 软件包与镜像库**。
1. 展开 **软件包库** 部分。
1. 打开 **不允许重复** 开关。
1. 可选。要允许某些重复的包，请在 **例外** 框中输入与要允许的包的名称和/或版本匹配的正则表达式 pattern。

您的更改会自动保存。

## 下载包文件

下载一个包文件。

如果多个包具有相同的名称、版本和文件名，则检索最新的包。

先决条件：

- 您需要使用 API 进行身份验证<!--[使用 API 进行身份验证](../../../api/index.md#authentication)-->。如果使用部署令牌进行身份验证，则必须使用 `read_package_registry` 和/或 `write_package_registry` 范围进行配置。

```plaintext
GET /projects/:id/packages/generic/:package_name/:package_version/:file_name
```

| 参数         | 类型            | 是否必需 | 描述                                                                         |
| -------------------| --------------- | ---------| ------------------------------------------------------------------------------------|
| `id`               | integer/string  | yes      | ID 或项目的 URL 编码路径<!--[项目的 URL 编码路径](../../../api/index.md#namespaced-path-encoding)-->。 |
| `package_name`     | string          | yes      | 包名称。                                                                  |
| `package_version`  | string          | yes      | 包版本。                                                               |
| `file_name`        | string          | yes      | 文件名称。                                                                     |

文件上下文在响应正文中提供。响应内容类型是 `application/octet-stream`。

使用个人访问令牌的示例请求：

```shell
curl --header "PRIVATE-TOKEN: <your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/24/packages/generic/my_package/0.0.1/file.txt"
```

使用 HTTP 基本身份验证的示例请求：

```shell
curl --user "user:<your_access_token>" \
     "https://gitlab.example.com/api/v4/projects/24/packages/generic/my_package/0.0.1/file.txt"
```

## 使用 CI/CD 发布通用包

要使用 [GitLab CI/CD](../../../ci/index.md) 中的通用包，您可以使用 `CI_JOB_TOKEN` 代替命令中的个人访问令牌。

示例：

```yaml
image: curlimages/curl:latest

stages:
  - upload
  - download

upload:
  stage: upload
  script:
    - 'curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file path/to/file.txt "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/my_package/0.0.1/file.txt"'

download:
  stage: download
  script:
    - 'wget --header="JOB-TOKEN: $CI_JOB_TOKEN" ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/my_package/0.0.1/file.txt'
```

在 PowerShell 中使用 Windows 运行程序时，您必须在 `upload` 和 `download` 阶段使用 `Invoke-WebRequest` 或 `Invoke-RestMethod` 而不是 `curl`。

例如：

```yaml
upload:
  stage: upload
  script:
    - Invoke-RestMethod -Headers @{ "JOB-TOKEN"="$CI_JOB_TOKEN" } -InFile path/to/file.txt -uri "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/my_package/0.0.1/file.txt" -Method put
```

### 在软件包库中启用或禁用通用包

对通用包的支持正在开发中，但已准备好用于生产。
它部署在**默认启用**的功能标志后面。[可以访问 GitLab Rails 控制台的 GitLab 管理员](../../../administration/feature_flags.md) 可以选择禁用它。

启用：

```ruby
# For the instance
Feature.enable(:generic_packages)
# For a single project
Feature.enable(:generic_packages, Project.find(<project id>))
```

禁用：

```ruby
# For the instance
Feature.disable(:generic_packages)
# For a single project
Feature.disable(:generic_packages, Project.find(<project id>))
```

<!--
### 通用包示例项目

The [Write CI-CD Variables in Pipeline](https://gitlab.com/guided-explorations/cfg-data/write-ci-cd-variables-in-pipeline) project contains a working example you can use to create, upload, and download generic packages in GitLab CI/CD.

It also demonstrates how to manage a semantic version for the generic package: storing it in a CI/CD variable, retrieving it, incrementing it, and writing it back to the CI/CD variable when tests for the download work correctly.
-->