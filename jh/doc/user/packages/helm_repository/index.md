---
stage: Package
group: Package
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 软件包库中的 Helm charts **(FREE)**

> 引入于 14.1 版本。

WARNING:
Helm chart 库正在开发中，由于功能有限，尚未准备好用于生产。<!--This [epic](https://gitlab.com/groups/gitlab-org/-/epics/6366) details the remaining
work and timelines to make it production ready.-->

在项目的软件包库中发布 Helm chart。然后在需要将它们用作依赖项时安装它们。

<!--
For documentation of the specific API endpoints that Helm package manager
clients use, see the [Helm API documentation](../../../api/packages/helm.md).
-->

## 构建 Helm 包

在 Helm 文档中阅读有关这些主题的更多信息：

- [创建您自己的 Helm chart](https://helm.sh/docs/intro/using_helm/#creating-your-own-charts)
- [将 Helm chart 打包成 chart 存档](https://helm.sh/docs/helm/helm_package/#helm-package)

## 对 Helm 库进行身份验证

要对 Helm 库进行身份验证，您需要：

- 个人访问令牌<!--[个人访问令牌](../../../api/index.md#personalproject-access-tokens)-->，其范围设置为`api`。
- 部署令牌<!--[部署令牌](../../project/deploy_tokens/index.md)-->，其范围设置为 `read_package_registry`、`write_package_registry` 或两者均有。
- [CI/CD 作业令牌](../../../ci/jobs/ci_job_token.md)。

## 发布软件包

NOTE:
您可以发布具有重复名称或版本的 Helm chart。如果存在重复项，系统总是返回最新版本的 chart。

构建完成后，可以使用 `curl` 或 `helm cm-push` 将 chart 上传到所需的 channel：

- 使用 `curl`：

  ```shell
  curl --request POST \
       --form 'chart=@mychart-0.1.0.tgz' \
       --user <username>:<access_token> \
       https://gitlab.example.com/api/v4/projects/<project_id>/packages/helm/api/<channel>/charts
  ```

   - `<username>`：GitLab 用户名或部署令牌用户名。
   - `<access_token>`：个人访问令牌或部署令牌。
   - `<project_id>`：项目 ID（如 `42`）或 URL-encoded<!--[URL-encoded](../../../api/index.md#namespaced-path-encoding)--> 项目路径（如`group%2Fproject`）。
   - `<channel>`：channel 的名称（如 `stable`）。

- 使用 [`helm cm-push`](https://github.com/chartmuseum/helm-push/#readme) 插件：

  ```shell
  helm repo add --username <username> --password <access_token> project-1 https://gitlab.example.com/api/v4/projects/<project_id>/packages/helm/<channel>
  helm cm-push mychart-0.1.0.tgz project-1
  ```

   - `<username>`：GitLab 用户名或部署令牌用户名。
   - `<access_token>`：个人访问令牌或部署令牌。
   - `<project_id>`：项目 ID（如 `42`）。
   - `<channel>`：channel 的名称（如 `stable`）。

## 使用 CI/CD 发布 Helm 包

要通过 [GitLab CI/CD](../../../ci/index.md) 自动发布 Helm 包，您可以使用 `CI_JOB_TOKEN` 代替命令中的个人访问令牌。

例如：

```yaml
image: curlimages/curl:latest
 
stages:
  - upload
 
upload:
  stage: upload
  script:
    - 'curl --request POST --user gitlab-ci-token:$CI_JOB_TOKEN --form "chart=@mychart-0.1.0.tgz" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/helm/api/<channel>/charts"'
```

- `<username>`：GitLab 用户名或部署令牌用户名。
- `<access_token>`：个人访问令牌或部署令牌。
- `<channel>`：channel 的名称（如 `stable`）。

## 安装软件包

NOTE:
当请求一个包时，系统只考虑最近创建的 300 个包。
对于每个包，只返回最近的包文件。

要安装最新版本的 chart，请使用以下命令：

```shell
helm repo add --username <username> --password <access_token> project-1 https://gitlab.example.com/api/v4/projects/<project_id>/packages/helm/<channel>
helm install my-release project-1/mychart
```

- `<username>`：GitLab 用户名或部署令牌用户名。
- `<access_token>`：个人访问令牌或部署令牌。
- `<project_id>`：项目 ID（如 `42`）。
- `<channel>`：channel 的名称（如 `stable`）。

如果先前已添加仓库，则可能需要运行：

```shell
helm repo update
```

使用最新可用的 chart 更新 Helm 客户端。

有关更多信息，请参阅[使用 Helm](https://helm.sh/docs/intro/using_helm/)。

## 故障排查

### 上传后，chart 在软件包库中不可见

检查 Sidekiq 日志<!--[Sidekiq 日志](../../../administration/logs.md#sidekiqlog)-->是否有任何相关错误。如果您看到 `Validation failed: Version is invalid`，则表示您的 `Chart.yaml` 文件中的版本不符合 [Helm Chart 版本规范](https://helm.sh/docs/topics/charts/#charts-and-versioning)。
要修复错误，请使用正确的版本语法并再次上传 chart。

### `helm push` 导致错误

Helm 3.7 为 `helm-push` 插件引入了重大更改。您可以更新 [Chart Museum plugin](https://github.com/chartmuseum/helm-push/#readme)，使用 `helm cm-push`。
