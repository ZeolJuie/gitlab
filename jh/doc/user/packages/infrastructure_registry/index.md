---
stage: Configure
group: Configure
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 基础设施库 **(FREE)**

> 引入于 14.0 版本。

使用 GitLab 基础设施库，您可以将 GitLab 项目用作基础设施包的私有库。您可以使用 GitLab CI/CD 创建和发布包，然后可以从其他私有项目中使用这些包。

## 查看软件包

要查看项目或组中的包：

1. 进入项目或群组。
1. 转至 **软件包与镜像库 > 基础设施库**。

您可以在此页面上搜索、排序和过滤包。

当您查看群组中的包时：

- 显示发布到群组及其项目的所有包。
- 仅显示您可以访问的项目。
- 如果项目是私有的，或者您不是该项目的成员，则不会显示。

有关如何创建和上传包的信息，请查看您的包类型的文档：

- [Terraform modules](../terraform_module_registry/index.md)

## 使用 GitLab CI/CD 构建包

要使用 [GitLab CI/CD](../../../ci/index.md) 构建包，您可以使用 [`CI_JOB_TOKEN` 预定义变量](../../../ci/variables/predefined_variables.md)。

您可以用来开始的 CI/CD 模板位于[此仓库](https://gitlab.com/gitlab-jh/gitlab/-/tree/master/lib/gitlab/ci/templates)。

了解有关使用 CI/CD 构建的更多信息：

- [Terraform modules](../terraform_module_registry/index.md#使用-cicd-发布-terraform-module)

如果使用 CI/CD 构建包，则在查看包详细信息时可以找到扩展的活动信息：

![Package CI/CD activity](../package_registry/img/package_activity_v12_10.png)

您可以看到发布包的流水线以及提交和触发它的用户。但是，每个包的历史记录仅限于五个更新。

## 下载包

要下载软件包：

1. 转至 **软件包与镜像库 > 基础设施库**。
1. 选择要下载的包的名称。
1. 在 **活动** 部分，选择您要下载的包的名称。

## 删除包

在基础设施库中发布包后，您将无法对其进行编辑。相反，您必须删除并重新创建它。

要删除包，您必须具有合适的权限。

您可以使用 API<!--[API](../../../api/packages.md#delete-a-project-package)--> 或 UI 删除包。

要在 UI 中从您的群组或项目中删除包：

1. 转至 **软件包与镜像库 > 基础设施库**。
1. 找到要删除的包的名称。
1. 选择 **删除**。

包被永久删除。

## 禁用基础设施库

基础设施库会自动启用。

对于自助管理实例，管理员可以[禁用](../../../administration/packages/index.md) **软件包与镜像库**，将从侧边栏中删除此菜单项。

您还可以删除特定项目的基础设施库：

1. 在您的项目中，转到 **设置 > 通用**。
1. 展开 **可见性、项目功能、权限** 部分并关闭 **包**（灰色）。
1. 选择 **保存修改**。

要重新启用它，请按照上述相同的步骤将其打开（蓝色）。
