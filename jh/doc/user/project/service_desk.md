---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.cn/handbook/engineering/ux/technical-writing/#assignments
---

# 服务台 **(标准版)**

> 在 13.2 版本中移至标准版

服务台是一个服务模块，它可以在不借助任何外部工具的情况下，使您的团队通过电子邮件与任何外部方联系。

借助服务台，您可以为客户提供高效的电子邮件支持。 他们可以通过电子邮件向您发送错误报告、功能请求或一般反馈。它们最终都会作为新的议题出现在您的项目中。反过来，您的团队可以直接从项目中做出响应。

由于服务台内置于极狐GitLab 中，因此消除了多种工具和外部集成的复杂性和低效性。这大大缩短了从反馈到软件更新的周期时间。

<!--
点击[GitLab 服务台](https://about.gitlab.com/blog/2017/05/09/demo-service-desk/)查看功能视频演示。
-->

## 如何工作


服务台使人们可以在您的极狐GitLab 实例中创建议题，而无需提供他们自己的账号信息。

它为最终用户在项目中创建问题提供了一个唯一的电子邮件地址。后续内容可以通过极狐GitLab 界面或通过电子邮件发送。最终用户只能通过电子邮件看到。

例如，假设您为 iOS 或 Android 开发游戏。代码库托管在您的极狐GitLab 实例中，构建和部署使用极狐GitLab CI/CD。

服务台是这样工作的：

1. 您为付费客户提供特定项目的电子邮件地址，用户可以直接从应用程序向您发送电子邮件。
1. 他们发送的每封电子邮件都会在相应的项目中产生一个议题。
1. 您的团队成员导航到服务台问题跟踪器，在那里他们可以查看新的技术支持请求并在相关议题中做出响应。
1. 您的团队与客户来回多次沟通以了解请求。
1. 您的团队开始致力于代码编写以解决客户的问题。
1. 当您的团队完成功能实现时，合并请求随即被处理并自动关闭议题。
1. 客户的请求通过电子邮件处理，无需访问您的极狐GitLab 实例。
1. 您的团队不必奔波来回于您的客户，这节省了大量时间。

## 配置服务台

拥有项目维护者或者更高权限的用户才可以配置服务台。

服务台议题是[机密](issues/confidential_issues.md)的，因此它们仅对项目成员可见。<!--在 极狐 GitLab 11.7 中，我们更新了生成的电子邮件地址格式。 旧格式仍支持，因此现有别名或联系人仍然有效。-->

如果您的存储库中有模版<!--[模版](description_templates.md)-->，您可以从选择器菜单中选择一个，将其附加到所有服务台议题。

通过如下方法在您的项目中开启服务台：

1. (仅用于自有实例) 为 GitLab 实例设置接收电子邮件。<!--[设置接收电子邮件](../../administration/incoming_email.md#set-it-up)。-->我们推荐使用电子邮件子寻址<!--[电子邮件子寻址](../../administration/incoming_email.md#电子邮件子寻址)-->，但您也可以使用万能邮箱<!--[万能邮箱](../../administration/incoming_email.md#catch-all-mailbox)-->。
1. 在项目的左边栏中，选择 **设置 > 通用**，然后展开 **服务台**。
1. 开启 **激活服务台** 按钮。 此时会显示一个唯一的电子邮件地址，用于将议题通过电子邮件发送到该项目。

现在服务台已经在该项目中开启！要在项目中访问它，请在左侧边栏中选择 **议题 > 服务台**.

WARNING:
**无论用户对您的极狐GitLab 实例的访问级别如何**，他们都可以使用服务台电子邮件地址在此项目中创建议题。

我们推荐采用如下方法来提供项目的安全性：

- 在您的电子邮件系统为服务台邮件地址配置一个别名，以后方便更改。
- 在您的实例中开启 Akismet<!--[开启 Akismet](../../integration/akismet.md)--> 添加垃圾邮件检查。那些未被阻止的垃圾电子邮件可能会导致创建许多垃圾邮件议题。

在您的实例中只有项目维护者或更高权限<!--[权限](../permissions.md)-->才能看到这个唯一的邮件地址。在外部使用电子邮件别名时，最终用户（议题创建者）无法看到信息注释中显示的内部电子邮件地址。

### 使用自定义邮件模版

> - 在 12.7 版本的专业版中引入。
> - 在 13.2 版本中该功能转移到标准版。

在以下情况下会向作者发送电子邮件：

- 用户使用服务台提交新议题。
- 在服务台议题上创建了一个新注释。

您可以使用模版来定制化邮件内容。将您的模板保存在存储库中的 `.gitlab/service_desk_templates/` 目录中。

借助服务台，您可以将模板用于：

- [感谢电子邮件](#感谢电子邮件)
- [新笔记邮件](#新笔记邮件)
- [新服务台议题](#新服务台议题)

#### 感谢电子邮件

当用户通过服务台提交议题时，极狐GitLab 会发送 **感谢电子邮件**。您必须将模板文件命名为 "thank_you.md"。

您可以在电子邮件中使用下面这些占位符：

- `%{ISSUE_ID}`：议题 ID
- `%{ISSUE_PATH}`：附加议题 ID 的项目路径

因为服务台议题被创建为[机密](issues/confidential_issues.md)的（只有项目成员可以看到它们），回复电子邮件不包含议题链接。

#### 新笔记邮件

当用户提交的议题收到新评论时，极狐GitLab 会发送一封新的注释电子邮件。您必须将模板文件命名为 "new_note.md"。

您可以在电子邮件中使用下面这些占位符：

- `%{ISSUE_ID}`：议题 ID
- `%{ISSUE_PATH}`：附加议题 ID 的项目路径
- `%{NOTE_TEXT}`：笔记内容

#### 新服务台议题

您可以为 **每个项目** 选择一个议题描述模板<!--[议题描述模板](description_templates.md#create-an-issue-template)-->，他们会附加到每个新的服务台议题的描述中。

议题描述模板位于您存储库的 `.gitlab/issue_templates/` 目录中。

通过如下方法在您的项目中使用自定义议题描述模版：

1. 创建一个描述模版<!--[创建一个描述模版](description_templates.md#create-an-issue-template)-->。
1. 导航到 **设置 > 通用 > 服务台**.
1. 从下拉 **附加到所有服务台议题的模版**，选择您的模板。

### 使用自定义邮件显示名称

> 从 12.8 版本引入。 

您可以自定义电子邮件显示名称。 从服务台发送的电子邮件在`发件人`标题中具有此名称。 默认显示名称是`GitLab 支持机器人`。

要编辑自定义电子邮件显示名称：

1. 在项目中，进入 **设置 > 通用 > 服务台** 。
1. 在 **邮箱显示名称** 中输入新的名字。
1. 选择 **保存更改**.

### 使用自定义邮箱地址 **(标准版 自管理)**

> - 在 13.0 版本的专业版中引入。
> - 在 13.8 版本中移除功能标志。

可以自定义服务台使用的电子邮件地址。为此，您必须同时配置[自定义邮箱](#配置自定义邮箱)和[自定义后缀](#配置自定义邮箱后缀)。

#### 配置自定义邮箱

<!--
NOTE:
在 GitLab.cn 上，自定义邮箱已经配置了 `contact-project+%{key}@incoming.gitlab.cn` 作为电子邮件地址，因此您只需在项目设置中配置[自定义后缀](#配置自定义邮箱后缀)。 
-->

使用`service_desk_email` 配置，您可以自定义服务台使用的邮箱。允许您通过在项目设置中配置[自定义后缀](#配置自定义邮箱后缀)来为服务台提供单独的电子邮件地址。

<!--
The address must include the +%{key} placeholder within the 'user'
portion of the address, before the @. This is used to identify the project
where the issue should be created.
-->
`address` 必须包含 'user' 中`+%{key}` 占位符`@`之前的地址部分，用于标示应在哪个项目中创建议题。

NOTE:
`service_desk_email` 和 `incoming_email` 应该使用不同的邮箱。这点很重要，因为从 `service_desk_email` 邮箱中的电子邮件是由不同的工作人员处理的，它无法识别 `incoming_email` 电子邮件。

要使用 IMAP 为服务台配置自定义邮箱，请将以下片段完整添加到您的配置文件中：

- 从源代码安装配置示例：

  ```yaml
  service_desk_email:
    enabled: true
    address: "project_contact+%{key}@example.com"
    user: "project_support@example.com"
    password: "[REDACTED]"
    host: "imap.gmail.com"
    port: 993
    ssl: true
    start_tls: false
    log_path: "log/mailroom.log"
    mailbox: "inbox"
    idle_timeout: 60
    expunge_deleted: true
  ```

- 以 Ominbus 方式安装配置示例：

  ```ruby
  gitlab_rails['service_desk_email_enabled'] = true
  gitlab_rails['service_desk_email_address'] = "project_contact+%{key}@gmail.com"
  gitlab_rails['service_desk_email_email'] = "project_support@gmail.com"
  gitlab_rails['service_desk_email_password'] = "[REDACTED]"
  gitlab_rails['service_desk_email_mailbox_name'] = "inbox"
  gitlab_rails['service_desk_email_idle_timeout'] = 60
  gitlab_rails['service_desk_email_log_file'] = "/var/log/gitlab/mailroom/mail_room_json.log"
  gitlab_rails['service_desk_email_host'] = "imap.gmail.com"
  gitlab_rails['service_desk_email_port'] = 993
  gitlab_rails['service_desk_email_ssl'] = true
  gitlab_rails['service_desk_email_start_tls'] = false
  ```

<!--
这里的配置项目与[incoming email](../../administration/incoming_email.md#set-it-up)的配置是相同的。
-->

##### Microsoft Graph

> 在 13.11 版本中引入。

服务台可以配置为使用 Microsoft Graph API 而不是 IMAP 读取 Microsoft Exchange Online 邮箱。<!--参看[文档](../../administration/incoming_email.md#microsoft-graph)。-->

- 以 Ominbus 方式安装配置示例：

  ```ruby
  gitlab_rails['service_desk_email_enabled'] = true
  gitlab_rails['service_desk_email_address'] = "project_contact+%{key}@example.onmicrosoft.com"
  gitlab_rails['service_desk_email_email'] = "project_contact@example.onmicrosoft.com"
  gitlab_rails['service_desk_email_mailbox_name'] = "inbox"
  gitlab_rails['service_desk_email_log_file'] = "/var/log/gitlab/mailroom/mail_room_json.log"
  gitlab_rails['service_desk_email_inbox_method'] = 'microsoft_graph'
  gitlab_rails['service_desk_email_inbox_options'] = {
   'tenant_id': '<YOUR-TENANT-ID>',
   'client_id': '<YOUR-CLIENT-ID>',
   'client_secret': '<YOUR-CLIENT-SECRET>',
   'poll_interval': 60  # Optional
  }
  ```

以源代码方式安装暂时不支持 Microsoft Graph API 配置。<!--相关细节请参考[议题](https://gitlab.com/gitlab-org/gitlab/-/issues/326169)。-->

#### 配置自定义邮箱后缀

配置[自定义邮箱](#配置自定义邮箱)后，您可以在项目的服务台设置中设置自定义后缀。它只能包含小写字母 (`a-z`)、数字 (`0-9`) 或下划线 (`_`)。

配置后，自定义后缀会创建一个新的服务台电子邮件地址，其中包括 `service_desk_email_address` 设置和格式：`<project_full_path>-<custom_suffix>`

比如 `mygroup/myproject` 项目服务台设置的配置如下：

- 电子邮件后缀为 `support`.
- 服务台邮箱地址配置为`contact+%{key}@example.com`.

这个项目的服务台邮箱地址就是：`contact+mygroup-myproject-support@example.com`。同时 incoming email<!--[incoming email](../../administration/incoming_email.md)--> 地址也是可以正常工作的。

如果不配置自定义后缀，将使用默认的项目标识来标识项目。您可以在项目设置中看到该电子邮件地址。

## 使用服务台

您可以使用服务台[创建议题](#做为最终用户议题创建者)或[回复一个议题](#做为议题回复者)。

在这些议题中，您还可以看到我们的好邻居[技术支持机器人](#机器人用户)。

### 做为最终用户（议题创建者）

> 支持额外的 email headers 引入于 14.6 版本。
> 在早期版本中，服务台电子邮件地址必须位于 “To” 字段中。

要创建服务台议题，最终用户不需要了解有关极狐GitLab 实例的任何信息。他们只是向他们提供的地址发送一封电子邮件，然后收到一封确认收到的电子邮件：

![服务台开启](img/service_desk_confirmation_email.png)

这也为最终用户提供了取消订阅的选项。

如果他们不选择退订，则添加到该问题的任何新评论都将作为电子邮件发送：

![服务台回复邮件](img/service_desk_reply.png)

议题中会显示所有通过邮件做出的回复。

<!--
For information about headers used for treating email, see
[the incoming email documentation](../../administration/incoming_email.md#accepted-headers).
-->

### 做为议题回复者

对于议题的回复者来说，一切都和其它极狐GitLab 议题一样。回复者可以在其中查看通过客户支持请求创建的问题，并过滤它们或与之交互。

![服务台议题追踪器](img/service_desk_issue_tracker.png)

来自最终用户的消息会显示为来自特殊的机器人用户<!--[机器人用户](../../subscriptions/self_managed/index.md#billable-users)-->。您可以像在极狐GitLab 中一样阅读和编写评论：

![服务台议题线程](img/service_desk_thread.png)

NOTE:
- 项目的可见性（私有、内部、公共）不影响服务台。
- 项目路径，包括其组或命名空间，显示在电子邮件中。

### 机器人用户

在后台，服务台是由特殊的机器人用户创建议题的。该用户不计入许可证限制计数。
