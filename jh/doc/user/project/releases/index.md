---
type: reference, howto
stage: Release
group: Release
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 发布 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/41766) in GitLab 11.7.
-->

在极狐GitLab 中，发布使您可以为用户创建项目的快照，包括安装包和发行说明。您可以在任何分支上创建极狐GitLab 发布。创建发布还会创建一个 [Git 标签](https://git-scm.com/book/en/v2/Git-Basics-Tagging) 来标记源代码中的发布点。

WARNING:
删除与发布关联的 Git 标签也会删除发布。

一个发布可以包括：

- 仓库源代码的快照。
- 从作业产物创建的[通用包](../../packages/generic_packages/index.md)。
- 与您的代码的已发布版本相关联的其他元数据。
- 发行说明。

当您[创建发布](#创建发布)时：

- 极狐GitLab 自动存档源代码并将其与发布相关联。
- 极狐GitLab 会自动创建一个 JSON 文件，列出发布中的所有内容，以便您可以比较和审核发布。这个文件叫做 [release evidence](#release-evidence)。

创建发布时或之后，您可以：

- 添加发行说明。
- 为与发布关联的 Git 标签添加一条消息。
- [将里程碑与其关联](#将里程碑与发布相关联)。
- 附加 [release assets](#release-assets)，例如 Runbook 或包。

## 查看发布

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/36667) in GitLab 12.8.
-->

要查看发布列表：

- 在左侧边栏上，选择 **部署 > 发布**，或

- 在项目概览页面上，如果至少存在一个发布，请单击发布数。

  ![Number of Releases](img/releases_count_v13_2.png "Incremental counter of Releases")

  - 在公开项目中，这个数字对所有用户都是可见的。
  - 在私有项目中，具有报告者或更高权限的用户可以看到此数字。

### 排序发布

要按 **发布日期** 或 **创建日期** 对版本进行排序，请从排序顺序下拉列表中进行选择。要在升序或降序之间切换，请选择 **排序顺序**。

![Sort Releases dropdown button](img/releases_sort_v13_6.png)

## 创建发布

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/32812) in GitLab 12.9. Releases can be created directly in the GitLab UI.
-->

您可以使用以下方式创建发布：

- [在 CI/CD 流水线中使用作业](#使用-cicd-作业创建发布)。
- [在发布页面](#在发布页面创建发布)。
- [在标签页面](#在标签页面创建发布)。

<!--
- Using the [Releases API](../../../api/releases/index.md#create-a-release).
-->

我们建议将发布版本作为 CI/CD 流水线中的最后一步之一。

先决条件：

- 您必须至少具有项目的开发人员角色。<!--For more information, read
[Release permissions](#release-permissions).-->

### 在发布页面创建发布

要在“发布”页面中创建发布：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **部署 > 发布** 并选择 **新建发布**。
1. 从 [**标签名称**](#标签名称) 下拉菜单中，可以：
   - 选择现有的 Git 标签。 选择已与发布关联的现有标签会导致验证错误。
   - 输入新的 Git 标签名称。
      1. 从 **创建自** 下拉列表中，选择在创建新标签时使用的分支或提交 SHA。
1. 可选。输入有关发布的其他信息，包括：
   - [标题](#标题)。
   - [里程碑](#将里程碑与发布相关联)。
   - [Release notes](#release-notes-描述)。
   - [Asset 链接](#链接)。
1. 选择 **创建发布**。

### 在标签页面创建发布

要在“标签”页面中创建发布，请将发布说明添加到现有或新的 Git 标签。

要将发行说明添加到新的 Git 标签：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **仓库 > 标签**。
1. 选择 **新建标签**。
1. 可选。在 **消息** 文本框中输入标记消息。
1. 在 **发布说明** 文本框中，输入发布说明。 您可以使用 Markdown 并将文件拖放到此文本框中。
1. 选择 **创建标签**。

要编辑现有 Git 标签的发布说明：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **仓库 > 标签**。
1. 选择 **编辑发布说明** (**{pencil}**)。
1. 在 **发布说明** 文本框中，输入发行说明。 您可以使用 Markdown 并将文件拖放到此文本框中。
1. 选择 **保存修改**。

## 使用 CI/CD 作业创建发布

> 引入于 12.7 版本。

您可以通过在作业定义中使用 [`release` 关键字](../../../ci/yaml/index.md#release) 直接将发布创建为 GitLab CI/CD 流水线的一部分。

仅当作业处理没有错误时才会创建发布。如果 Rails API 在发布创建期间返回错误，则发布作业将失败。

### `release` 关键字的 CI/CD 示例

要在推送 Git 标签时创建发布，或通过转到 **仓库 > 标签** 在 UI 中添加 Git 标签时：

```yaml
release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG                  # Run this job when a tag is created manually
  script:
    - echo "running release_job"
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: 'Created using the release-cli $EXTRA_DESCRIPTION'  # $EXTRA_DESCRIPTION must be defined
    tag_name: '$CI_COMMIT_TAG'                                       # elsewhere in the pipeline.
    ref: '$CI_COMMIT_TAG'
    milestones:
      - 'm1'
      - 'm2'
      - 'm3'
    released_at: '2020-07-15T08:00:00Z'  # Optional, is auto generated if not defined, or can use a variable.
    assets: # Optional, multiple asset links
      links:
        - name: 'asset1'
          url: 'https://example.com/assets/1'
        - name: 'asset2'
          url: 'https://example.com/assets/2'
          filepath: '/pretty/url/1' # optional
          link_type: 'other' # optional
```

要在将提交推送或合并到默认分支时自动创建发布，请使用使用变量定义的新 Git 标签：

```yaml
prepare_job:
  stage: prepare                                              # This stage must run before the release stage
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                             # Do not run this job when a tag is created manually
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH             # Run this job when commits are pushed or merged to the default branch
  script:
    - echo "EXTRA_DESCRIPTION=some message" >> variables.env  # Generate the EXTRA_DESCRIPTION and TAG environment variables
    - echo "TAG=v$(cat VERSION)" >> variables.env             # and append to the variables.env file
  artifacts:
    reports:
      dotenv: variables.env                                   # Use artifacts:reports:dotenv to expose the variables to other jobs

release_job:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: prepare_job
      artifacts: true
  rules:
    - if: $CI_COMMIT_TAG
      when: never                                  # Do not run this job when a tag is created manually
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH  # Run this job when commits are pushed or merged to the default branch
  script:
    - echo "running release_job for $TAG"
  release:
    name: 'Release $TAG'
    description: 'Created using the release-cli $EXTRA_DESCRIPTION'  # $EXTRA_DESCRIPTION and the $TAG
    tag_name: '$TAG'                                                 # variables must be defined elsewhere
    ref: '$CI_COMMIT_SHA'                                            # in the pipeline. For example, in the
    milestones:                                                      # prepare_job
      - 'm1'
      - 'm2'
      - 'm3'
    released_at: '2020-07-15T08:00:00Z'  # Optional, is auto generated if not defined, or can use a variable.
    assets:
      links:
        - name: 'asset1'
          url: 'https://example.com/assets/1'
        - name: 'asset2'
          url: 'https://example.com/assets/2'
          filepath: '/pretty/url/1' # optional
          link_type: 'other' # optional
```

NOTE:
`before_script` 或 `script` 中设置的环境变量不可用于在同一作业中扩展。阅读有关潜在地使变量可用于扩展的更多信息。

### 使用自定义 SSL CA 证书颁发机构

您可以使用 `ADDITIONAL_CA_CERT_BUNDLE` CI/CD 变量来配置自定义 SSL CA 证书颁发机构，用于在 `release-cli` 通过 API 使用带有自定义证书的 HTTPS 创建版本时验证对等方。
`ADDITIONAL_CA_CERT_BUNDLE` 值应包含 [X.509 PEM 公钥证书的文本表示](https://tools.ietf.org/html/rfc7468#section-5.1) 或 `path/to/file` 包含证书颁发机构。
例如，要在 `.gitlab-ci.yml` 文件中配置此值，请使用以下命令：

```yaml
release:
  variables:
    ADDITIONAL_CA_CERT_BUNDLE: |
        -----BEGIN CERTIFICATE-----
        MIIGqTCCBJGgAwIBAgIQI7AVxxVwg2kch4d56XNdDjANBgkqhkiG9w0BAQsFADCB
        ...
        jWgmPqF3vUbZE0EyScetPJquRFRKIesyJuBFMAs=
        -----END CERTIFICATE-----
  script:
    - echo "Create release"
  release:
    name: 'My awesome release'
    tag_name: '$CI_COMMIT_TAG'
```

`ADDITIONAL_CA_CERT_BUNDLE` 值也可以配置为 UI 中的自定义变量<!--[UI 中的自定义变量](../../../ci/variables/index.md#custom-cicd-variables)-->，或者作为 `file`，需要证书的路径；或作为变量，需要证书的文本表示。

### `release-cli` 命令行

`release` 节点下的条目被转换为 Bash 命令并发送到 Docker 容器，其中包含 release-cli<!--[release-cli](https://gitlab.com/gitlab-org/release-cli)-->。
您还可以直接从 `script` 条目中调用 `release-cli`。

例如，如果您使用前面描述的 YAML：

```shell
release-cli create --name "Release $CI_COMMIT_SHA" --description "Created using the release-cli $EXTRA_DESCRIPTION" --tag-name "v${MAJOR}.${MINOR}.${REVISION}" --ref "$CI_COMMIT_SHA" --released-at "2020-07-15T08:00:00Z" --milestone "m1" --milestone "m2" --milestone "m3" --assets-link "{\"name\":\"asset1\",\"url\":\"https://example.com/assets/1\",\"link_type\":\"other\"}
```

### 在单个流水线中创建多个发布

一个流水线可以有多个 `release` 作业，例如：

```yaml
ios-release:
  script:
    - echo "iOS release job"
  release:
    tag_name: v1.0.0-ios
    description: 'iOS release v1.0.0'

android-release:
  script:
    - echo "Android release job"
  release:
    tag_name: v1.0.0-android
    description: 'Android release v1.0.0'
```

<!--
### Release assets as Generic packages

You can use [Generic packages](../../packages/generic_packages/index.md) to host your release assets.
For a complete example, see the [Release assets as Generic packages](https://gitlab.com/gitlab-org/release-cli/-/tree/master/docs/examples/release-assets-as-generic-package/)
project.
-->

## 即将发布

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/38105) in GitLab 12.1.
-->

您可以使用 Releases API<!--[Releases API](../../../api/releases/index.md#upcoming-releases)--> 提前创建发布。
当您设置未来的 `released_at` 日期时，发布标签旁边会显示 **即将发布** 徽章。当 `released_at` 日期和时间过去后，徽章会自动移除。

![An upcoming release](img/upcoming_release_v12_7.png)

## 编辑发布

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/26016) in GitLab 12.6. Asset link editing was [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/9427) in GitLab 12.10.
-->

只有至少具有开发人员角色的用户才能编辑版本。
阅读有关[发布权限](#发布权限)的更多信息。

要编辑发布的详细信息：

1. 在左侧边栏上，选择 **部署 > 发布**。
1. 在您要修改的版本的右上角，单击 **编辑此发布**（铅笔图标）。
1. 在 **编辑发布** 页面上，更改发布的详细信息。
1. 单击 **保存修改**。

您可以编辑版本标题、注释、关联的里程碑和 assets 链接。
要更改发布日期，请使用 Releases API<!--[Releases API](../../../api/releases/index.md#update-a-release)-->。

## 将里程碑与发布相关联

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/29020) in GitLab 12.5.
> - [Updated](https://gitlab.com/gitlab-org/gitlab/-/issues/39467) to edit milestones in the UI in GitLab 13.0.
-->

您可以将一个发布与一个或多个项目里程碑<!--[项目里程碑](../milestones/index.md#project-milestones-and-group-milestones)-->相关联。

[专业版](https://about.gitlab.com/pricing/)客户可以指定群组里程碑<!--[群组里程碑](../milestones/index.md#project-milestones-and-group-milestones)-->与发布相关联。

您可以在用户界面中执行此操作，或者通过在对 Releases API<!--[Releases API](../../../api/releases/index.md#create-a-release)--> 的请求中包含一个 `milestones` 数组。

在用户界面中，要将里程碑与发布相关联：

1. 在左侧边栏上，选择 **部署 > 发布**。
1. 在您要修改的发布的右上角，单击 **编辑此发布**（铅笔图标）。
1. 从 **里程碑** 列表中，选择要关联的每个里程碑。您可以选择多个里程碑。
1. 单击 **保存修改**。

在 **部署 > 发布** 页面上，**里程碑** 列在顶部，以及有关里程碑中议题的统计信息。

![A Release with one associated milestone](img/release_with_milestone_v12_9.png)

在 **议题 > 里程碑** 页面上以及单击此页面上的里程碑时，也可以看到发布。

下面是一个里程碑示例，分别是没有发布、一个发布和两个发布。

![Milestones with and without Release associations](img/milestone_list_with_releases_v12_5.png)

NOTE:
子组的项目发布不能与超级组的里程碑相关联。<!--To learn
more, read issue #328054,
[Releases cannot be associated with a supergroup milestone](https://gitlab.com/gitlab-org/gitlab/-/issues/328054).-->

## 创建发布时收到通知

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/26001) in GitLab 12.4.
-->

当为您的项目创建新发布时，您可以通过电子邮件收到通知。

要订阅发布通知：

1. 在左侧边栏上，选择 **项目信息**。
1. 点击 **通知设置**（铃铛图标）。
1. 在列表中，单击 **自定义**。
1. 选中 **新建版本** 复选框。
1. 关闭对话框进行保存。

## 通过设置部署冻结来防止意外发布

> - 引入于 13.0 版本。
> - 通过 UI 删除冻结期的能力引入于 14.3 版本。

通过设置*部署冻结*期<!--[*部署冻结*期](../../../ci/environments/deployment_safety.md)-->，在您指定的时间段内防止意外的生产发布。
部署冻结有助于减少自动化部署时的不确定性和风险。

维护者可以在用户界面中设置一个部署冻结窗口，或者通过使用 Freeze Periods API<!--[Freeze Periods API](../../../api/freeze_periods.md)--> 来设置一个 `freeze_start` 和一个 `freeze_end`，定义为 [crontab](https://crontab.guru/) 条目。

如果正在执行的作业在冻结期内，GitLab CI/CD 会创建一个名为 `$CI_DEPLOY_FREEZE` 的环境变量。

为了防止部署作业执行，在你的 `.gitlab-ci.yml` 中创建一个 `rules` 条目，例如：

```yaml
deploy_to_production:
  stage: deploy
  script: deploy_to_prod.sh
  rules:
    - if: $CI_DEPLOY_FREEZE == null
```

要在 UI 中设置部署冻结窗口，请完成以下步骤：

1. 用具有维护者角色的用户身份登录极狐GitLab。
1. 在左侧边栏上，选择 **项目信息**。
1. 在左侧导航菜单中，导航至 **设置 > CI/CD**。
1. 滚动到 **部署冻结**。
1. 点击 **展开** 查看部署冻结表。
1. 点击 **添加部署冻结** 打开部署冻结窗口。
1. 输入所需部署冻结期的开始时间、结束时间和时区。
1. 在窗口中点击 **添加部署冻结**。
1. 部署冻结保存后，您可以通过选择编辑按钮（**{pencil}**）对其进行编辑，并通过选择删除按钮（**{remove}**）将其删除。

   ![Deploy freeze modal for setting a deploy freeze period](img/deploy_freeze_v14_3.png)

如果项目包含多个冻结期，则所有冻结期都适用。如果它们重叠，则冻结覆盖整个重叠期。

<!--
For more information, see [Deployment safety](../../../ci/environments/deployment_safety.md).
-->

## 发布字段

创建或编辑发布时，以下字段可用。

### 标题

在创建或编辑发布时，可以使用 **发布标题** 字段自定义发布标题。如果未提供标题，则使用发布的标签名称。

### 标签名称

发布标签名称应包含发布版本。极狐GitLab 使用[语义版本控制](https://semver.org/)发布我们的版本，我们建议您也这样做，使用 `(Major).(Minor).(Patch)`<!--，详见 [GitLab Policy for Versioning](../../../policy/maintenance.md#versioning)-->。

例如，对于版本 `10.5.7`：

- `10` 代表主要版本。主要版本是 `10.0.0`，但通常称为 `10.0`。
- `5` 代表小版本。次要版本是 `10.5.0`，但通常称为 `10.5`。
- `7` 表示补丁编号。

版本号的任何部分都可以是多个数字，例如 `13.10.11`。

### Release notes 描述

每个发布都有一个描述。您可以添加任何您喜欢的文本，但我们建议您包含一个更改日志来描述您的版本内容。这有助于用户快速扫描您发布的每个版本之间的差异。

[Git 的标记消息](https://git-scm.com/book/en/v2/Git-Basics-Tagging) 和 release notes 描述无关。说明支持 Markdown<!--[Markdown](../../markdown.md)-->。

### Release assets

一个版本包含以下类型的 assets：

- [源代码](#源代码)
- [链接](#链接)

#### 源代码

极狐GitLab 从给定的 Git 标签自动生成 `zip`、`tar.gz`、`tar.bz2` 和 `tar` 归档源代码。这些是只读 assets。

#### 链接

链接是可以指向您喜欢的任何内容的任何 URL：文档、构建的二进制文件或其他相关材料。这些可以是来自实例的内部或外部链接。
作为 assets 的每个链接都具有以下属性：

| 属性   | 描述                            | 必需 |
| ----        | -----------                            | ---      |
| `name`      | 链接的名称。                  | Yes      |
| `url`       | 下载文件的 URL。            | Yes      |
| `filepath`  | 指向 `url` 的重定向链接。有关更多信息，请参阅[此部分](#发布-assets-的永久链接)。 | No       |
| `link_type` | 用户可以通过 `url` 下载的内容类型。有关更多信息，请参阅[此部分](#链接类型)。 | No       |

##### 发布 assets 的永久链接

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/27300) in GitLab 12.9.
-->

与发布相关的 assets 可通过永久 URL 访问。
系统始终将此 URL 重定向到实际 assets 位置，因此即使 assets 移动到不同位置，您也可以继续使用相同的 URL。这是在链接创建<!--[链接创建](../../../api/releases/links.md#create-a-link)-->或更新时<!--[更新时](../../../api/releases/links.md#update-a-link)-->定义的。

URL 的格式为：

```plaintext
https://host/namespace/project/releases/:release/downloads/:filepath
```

<!--
如果你在 `gitlab-org` 命名空间和 `gitlab.com` 上的 `gitlab-runner` 项目中有 `v11.9.0-rc2` 版本的资产，例如：

```json
{
  "name": "linux amd64",
  "filepath": "/binaries/gitlab-runner-linux-amd64",
  "url": "https://gitlab-runner-downloads.s3.amazonaws.com/v11.9.0-rc2/binaries/gitlab-runner-linux-amd64",
  "link_type": "other"
}
```

This asset has a direct link of:

```plaintext
https://gitlab.com/gitlab-org/gitlab-runner/releases/v11.9.0-rc2/downloads/binaries/gitlab-runner-linux-amd64
```

The physical location of the asset can change at any time and the direct link remains unchanged.
-->

##### 链接类型

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/207257) in GitLab 13.1.
-->

四种类型的链接是 “Runbook”、“Package”、“Image” 和 “Other”。
`link_type` 参数接受以下四个值之一：

- `runbook`
- `package`
- `image`
- `other`（默认）

此字段对 URL 没有影响，它仅用于项目的发布页面中的可视化区分。

##### 使用通用包来附加二进制文件

您可以使用通用包<!--[通用包](../../packages/generic_packages/index.md)-->存储来自发布或标签流水线的任何产物，也可用于将二进制文件附加到单个发布条目。
基本上，您需要：

1. 将产物推送到通用软件包库<!--[将产物推送到通用软件包库](../../packages/generic_packages/index.md#publish-a-package-file)-->。
1. [将软件包链接附加到发布](#链接)。

以下示例生成发布 assets，将它们作为通用包发布，然后创建发布：

```yaml
stages:
  - build
  - upload
  - release

variables:
  # Package version can only contain numbers (0-9), and dots (.).
  # Must be in the format of X.Y.Z, i.e. should match /\A\d+\.\d+\.\d+\z/ regular expresion.
  # See https://docs.gitlab.com/ee/user/packages/generic_packages/#publish-a-package-file
  PACKAGE_VERSION: "1.2.3"
  DARWIN_AMD64_BINARY: "myawesomerelease-darwin-amd64-${PACKAGE_VERSION}"
  LINUX_AMD64_BINARY: "myawesomerelease-linux-amd64-${PACKAGE_VERSION}"
  PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/myawesomerelease/${PACKAGE_VERSION}"

build:
  stage: build
  image: alpine:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - mkdir bin
    - echo "Mock binary for ${DARWIN_AMD64_BINARY}" > bin/${DARWIN_AMD64_BINARY}
    - echo "Mock binary for ${LINUX_AMD64_BINARY}" > bin/${LINUX_AMD64_BINARY}
  artifacts:
    paths:
      - bin/

upload:
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/${DARWIN_AMD64_BINARY} "${PACKAGE_REGISTRY_URL}/${DARWIN_AMD64_BINARY}"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file bin/${LINUX_AMD64_BINARY} "${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}"

release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"${DARWIN_AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${DARWIN_AMD64_BINARY}\"}" \
        --assets-link "{\"name\":\"${LINUX_AMD64_BINARY}\",\"url\":\"${PACKAGE_REGISTRY_URL}/${LINUX_AMD64_BINARY}\"}"
```

在传递给 `release-cli` 之前，PowerShell 用户可能需要使用 `-assets-link` 和 `ConvertTo-Json` 对 JSON 字符串中的双引号 `"` 进行转义，并带有 `` ` ``（反引号）。
例如：

```yaml
release:
  script:
    - $env:asset = "{`"name`":`"MyFooAsset`",`"url`":`"https://gitlab.com/upack/artifacts/download/$env:UPACK_GROUP/$env:UPACK_NAME/$($env:GitVersion_SemVer)?contentOnly=zip`"}"
    - $env:assetjson = $env:asset | ConvertTo-Json
    - release-cli create --name $CI_COMMIT_TAG --description "Release $CI_COMMIT_TAG" --ref $CI_COMMIT_TAG --tag-name $CI_COMMIT_TAG --assets-link=$env:assetjson
```

NOTE:
不建议将[作业产物](../../../ci/pipelines/job_artifacts.md)链接直接附加到发布，因为产物是短暂的，用于在同一流水线中传递数据。这意味着它们可能会过期或有人可能会手动删除它们。

<!--
#### 新功能和总功能的数量 **(FREE SAAS)**

> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/235618) in GitLab 13.5.

On [GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/releases), you can view the number of new and total features in the project.

![Feature count](img/feature_count_v14_6.png "Number of features in a release")

The totals are displayed on [shields](https://shields.io/) and are generated per release by
[a Rake task in the `www-gitlab-com` repo](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/lib/tasks/update_gitlab_project_releases_page.rake).

| Item    | Formula |
| ------  | ------ |
| `New features`   | Total count of release posts across all tiers for a single release in the project. |
| `Total features` | Total count of release posts in reverse order for all releases in the project. |

The counts are also shown by license tier.

| Item    | Formula |
| ------  | ------ |
| `New features`   | Total count of release posts across a single tier for a single release in the project. |
| `Total features` | Total count of release posts across a single tier in reverse order for all releases in the project. |
-->

## Release evidence

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/26019) in GitLab 12.6.
-->

每次创建发布时，系统都会生成与其相关的数据的快照。
此数据保存在 JSON 文件中，称为 *release evidence*。该功能包括测试产物和链接的里程碑，以促进内部流程，如外部审计。

要访问 release evidence，请在发布页面上，单击 **凭证集** 标题下列出的 JSON 文件的链接。

您还可以使用 API<!--[使用 API](../../../api/releases/index.md#collect-release-evidence)--> 为现有发布生成 release evidence。因此，每个发布都可以有多个 release evidence 快照。您可以在发布页面上查看 release evidence 及其详细信息。

禁用议题跟踪器后，无法下载 release evidence。

下面是一个 release evidence 对象的例子：

```json
{
  "release": {
    "id": 5,
    "tag_name": "v4.0",
    "name": "New release",
    "project": {
      "id": 20,
      "name": "Project name",
      "created_at": "2019-04-14T11:12:13.940Z",
      "description": "Project description"
    },
    "created_at": "2019-06-28 13:23:40 UTC",
    "description": "Release description",
    "milestones": [
      {
        "id": 11,
        "title": "v4.0-rc1",
        "state": "closed",
        "due_date": "2019-05-12 12:00:00 UTC",
        "created_at": "2019-04-17 15:45:12 UTC",
        "issues": [
          {
            "id": 82,
            "title": "The top-right popup is broken",
            "author_name": "John Doe",
            "author_email": "john@doe.com",
            "state": "closed",
            "due_date": "2019-05-10 12:00:00 UTC"
          },
          {
            "id": 89,
            "title": "The title of this page is misleading",
            "author_name": "Jane Smith",
            "author_email": "jane@smith.com",
            "state": "closed",
            "due_date": "nil"
          }
        ]
      },
      {
        "id": 12,
        "title": "v4.0-rc2",
        "state": "closed",
        "due_date": "2019-05-30 18:30:00 UTC",
        "created_at": "2019-04-17 15:45:12 UTC",
        "issues": []
      }
    ],
    "report_artifacts": [
      {
        "url":"https://gitlab.example.com/root/project-name/-/jobs/111/artifacts/download"
      }
    ]
  }
}
```

### 收集 release evidence **(PREMIUM SELF)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/199065) in [GitLab Premium](https://about.gitlab.com/pricing/) 12.10.
-->

创建发布时，会自动收集 release evidence。要在任何其他时间启动收集，请使用 API 调用<!--[API 调用](../../../api/releases/index.md#collect-release-evidence)-->。您可以为一次发布多次收集 release evidence。

在发布页面上可以看到 release evidence 快照以及时间戳。

### 包括报告产物作为 release evidence **(ULTIMATE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/32773) in [GitLab Ultimate](https://about.gitlab.com/pricing/) 13.2.
-->

创建发布时，如果[作业产物](../../../ci/yaml/index.md#artifactsreports)包含在最后运行的流水线中，它们会自动包含在发布中作为 release evidence。

尽管作业产物通常会过期，但  release evidence 中包含的产物不会过期。

要启用作业产物收集，您需要同时指定：

1. [`artifacts:paths`](../../../ci/yaml/index.md#artifactspaths)
1. [`artifacts:reports`](../../../ci/yaml/index.md#artifactsreports)

```yaml
ruby:
  script:
    - gem install bundler
    - bundle install
    - bundle exec rspec --format progress --format RspecJunitFormatter --out rspec.xml
  artifacts:
    paths:
      - rspec.xml
    reports:
      junit: rspec.xml
```

如果流水线成功运行，当您创建发布时，`rspec.xml` 文件将保存为 release evidence。

如果您[计划 release evidence 收集](#计划-release-evidence-收集)，一些产物可能在收集时已经过期。为了避免这种情况，您可以使用 [`artifacts:expire_in`](../../../ci/yaml/index.md#artifactsexpire_in) 关键字。

### 计划 release evidence 收集

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/23697) in GitLab 12.8.
-->

在 API 中：

- 如果您指定未来的 `released_at` 日期，则该版本将成为 **未来发布**，并在发布日期收集 release evidence。在此之前您不能收集 release evidence。
- 如果您使用过去的 `released_at` 日期，则不会收集任何 release evidence。
- 如果您未指定 `released_at` 日期，则在创建发布之日收集 release evidence。

## 发布权限

> 创建、更新和删除操作的权限窗口修复于 14.1 版本

### 查看发布并下载 assets

> 访客角色的访问更改于 14.5 版本。

- 具有报告者或以上角色的用户，具有对项目发布的 read 和下载权限。
- 具有访客角色的用户具有对项目发布的 read 和下载访问权限。包括相关的 Git 标签名称、发布描述、发布的作者信息。但是，其他与仓库相关的信息，例如[源代码](#源代码)、[release evidence](#release-evidence) 已被编辑。

### 创建、更新、删除发布及其 assets

- 具有开发者及以上角色的用户具有对项目发布和 assets 的写入权限。
- 如果发布与[受保护标签](../protected_tags.md)相关联，则用户也必须[允许创建受保护标签](../protected_tags.md#配置受保护的标签)。

作为发布权限控制的示例，您可以通过带有通配符 (`*`) 的标记保护，只允许维护者或以上权限创建、更新和删除发布 ，并在 **允许创建** 列中设置 **维护者**。

## 发布命令行

<!--
> [Introduced](https://gitlab.com/gitlab-org/release-cli/-/merge_requests/6) in GitLab 12.10.
-->

Release CLI 是一个命令行工具，用于从命令行或 GitLab CI/CD 配置文件 `.gitlab-ci.yml` 管理 GitLab 发布。

使用它，您可以直接通过终端创建、更新、修改和删除发布。

<!--
Read the [Release CLI documentation](https://gitlab.com/gitlab-org/release-cli/-/blob/master/docs/index.md)
for details.
-->

## 发布指标 **(ULTIMATE)**

> 引入于专业版 13.9 版本。

通过导航到 **群组 > 分析 > CI/CD**，可以获得群组级发布指标。
这些指标包括：

- 群组内发布总数
- 群组中至少有一个发布的项目的百分比

<!--
## 工作示例项目

The Guided Exploration project [Utterly Automated Software and Artifact Versioning with GitVersion](https://gitlab.com/guided-explorations/devops-patterns/utterly-automated-versioning) demonstrates:

- Using GitLab releases.
- Using the GitLab `release-cli`.
- Creating a generic package.
- Linking the package to the release.
- Using a tool called [GitVersion](https://gitversion.net/) to automatically determine and increment versions for complex repositories.

You can copy the example project to your own group or instance for testing. More details on what other GitLab CI patterns are demonstrated are available at the project page.
-->

## 故障排查

### 在创建、更新或删除版本及其 assets 时出现 `403 Forbidden` 或 `Something went wrong while creating a new release` 错误

如果发布与[受保护的标签](../protected_tags.md)相关联，则 UI/API 请求可能会导致授权失败。
确保用户或服务/机器人帐户也被允许[创建受保护的标签](../protected_tags.md#configuring-protected-tags)。

有关更多信息，请参阅[发布权限](#发布权限)。
