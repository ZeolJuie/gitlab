---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: concepts
---

# 合并请求工作流 **(FREE)**

有两种主要方法可以使用极狐GitLab 合并请求工作流：

1. 在单个仓库中使用[受保护分支](../protected_branches.md)。
1. 使用权威项目的分支。

## 受保护分支工作流

使用受保护的分支流，每个人都在同一个项目中工作。

项目维护者获得维护者角色，普通开发者获得开发者角色。

维护者将权威分支标记为“受保护”。

开发人员将功能分支推送到项目并创建合并请求，以审核他们的功能分支并将其合并到受保护的分支之一。

默认情况下，只有具有维护者角色的用户才能将更改合并到受保护的分支中。

**优点**

- 更少的项目意味着更少的混乱。
- 开发者只需要考虑一个远端仓库。

**缺点**

- 每个新项目所需的受保护分支的手动设置

## 派生工作流

通过派生工作流，维护者获得维护者角色，普通开发者获得报告者权限访问权威仓库，这禁止他们向其推送任何更改。

开发者创建权威项目的分支并将他们的功能分支推送到他们自己的分支。

要将他们的更改放入默认分支，他们需要跨分支创建合并请求。

**优点**

- 在适当配置的群组中，新项目会自动获得常规开发人员所需的访问限制：为新项目配置授权的手动步骤更少。

**缺点**

- 该项目需要保持他们的派生是最新的，这需要更高级的 Git 技能（管理多个远端）。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
