---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
description: "How to create merge requests in GitLab."
disqus_identifier: 'https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html'
---

# 创建合并请求 **(FREE)**

有许多不同的方法可以创建合并请求。

## 从合并请求列表

您可以在合并请求列表中创建合并请求。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧菜单中，选择 **合并请求**。
1. 在右上角，选择 **新建合并请求**。
1. 选择源分支和目标分支，然后**比较分支并继续**。
1. 填写字段并选择 **创建合并请求**。

NOTE:
合并请求是围绕一对一 (1:1) 分支关系设计的。 一次只能有一个打开的合并请求与给定的目标分支相关联。

## 从议题

<!--
You can [create a merge request from an issue](../repository/web_editor.md#create-a-new-branch-from-an-issue).
-->
您可以从议题创建合并请求。

## 当添加、编辑或上传文件时

您可以在向仓库添加、编辑或上传文件时创建合并请求。

1. 添加、编辑或上传文件到存储库。
1. 在 **提交消息** 中，输入提交的原因。
1. 选择**目标分支** 或通过键入名称（不包含空格、大写字母或特殊字符）来创建新分支。
1. 选择 **Start a new merge request with these changes** 复选框或切换。 仅当目标与源分支不同或源分支受保护时，此复选框或切换才可见。
1. 选择**提交更改**。

## 当创建分支时

您可以在创建分支时创建合并请求。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧菜单中，选择 **仓库 > 分支**。
1. 输入分支名称并选择 **新建分支**。
1. 在文件列表上方，在右侧，选择 **创建合并请求**。合并请求被创建，目标是默认分支。
1. 填写字段并选择 **创建合并请求**。

## 在本地使用 Git 命令时

您可以通过在本地计算机上运行 Git 命令来创建合并请求。

1. 创建分支：

   ```shell
   git checkout -b my-new-branch
   ```

1. 创建、编辑或删除文件。暂存并提交它们：

   ```shell
   git add .
   git commit -m "My commit message"
   ```

1. 将你的分支推送到极狐GitLab<!--[将你的分支推送到 GitLab](../../../gitlab-basics/start-using-git.md#send-changes-to-gitlabcom)-->：

   ```shell
   git push origin my-new-branch
   ```

   极狐GitLab 会直接用链接提示您创建合并请求：

   ```plaintext
   ...
   remote: To create a merge request for docs-new-merge-request, visit:
   remote:   https://gitlab.example.com/my-group/my-project/merge_requests/new?merge_request%5Bsource_branch%5D=my-new-branch
   ```

1. 复制链接并将其粘贴到浏览器中。

您可以在通过命令行推送时，向命令添加其他[标志](../push_options.md)以减少通过 UI 手动编辑合并请求的需要。

## 当在派生项目中工作时

您可以在您的派生项目创建一个合并请求，贡献主项目。

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 选择您的仓库分支。
1. 在左侧菜单中，转到 **合并请求**，然后选择 **新建合并请求**。
1. 在 **源分支** 下拉列表框中，选择您派生仓库中的分支作为源分支。
1. 在 **目标分支** 下拉列表框中，选择上游仓库中的分支作为目标分支。您可以设置[默认目标项目](#设置默认目标项目)来更改默认目标分支（如果您在派生项目中工作，这可能很有用）。
1. 选择 **比较分支并继续**。
1. 选择 **提交合并请求**。

在您的工作合并后，如果你不打算对上游项目做出任何其他贡献，你可以取消您的派生项目与其上游项目的关系。转到 **设置 > 高级设置**并删除派生关系<!--[删除派生关系](../settings/index.md#removing-a-fork-relationship)-->。

<!--
For more information, [see the forking workflow documentation](../repository/forking_workflow.md).
-->

## 通过发送邮件

<!--
> The format of the generated email address changed in GitLab 11.7.
  The earlier format is still supported so existing aliases
  or contacts still work.
-->

您可以通过向极狐GitLab 发送电子邮件来创建合并请求。
合并请求目标分支是项目的默认分支。

先决条件：

- 极狐GitLab 管理员必须配置接收电子邮件<!--[接收电子邮件](../../../administration/incoming_email.md)-->。
- 极狐GitLab 管理员必须配置通过电子邮件回复<!--[通过电子邮件回复](../../../administration/reply_by_email.md)-->。

要通过发送电子邮件创建合并请求：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧菜单中，选择 **合并请求**。
1. 在右上角，选择 **通过电子邮件向该项目发送新的合并请求**。显示电子邮件地址。复制这个地址。确保您将此地址保密。
1. 打开一封电子邮件并撰写包含以下信息的消息：

    - **To** 行是您复制的电子邮件地址。
    - 主题行是源分支名称。
    - 消息体是合并请求描述。

1. 发送电子邮件。

创建合并请求。

### 通过电子邮件创建合并请求时添加附件

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/22723) in GitLab 11.5.
-->

您可以通过将补丁作为电子邮件的附件添加到合并请求中。所有文件名以 `.patch` 结尾的附件都被视为补丁，并按名称排序。

补丁的组合大小可以是 2 MB。

如果主题行的源分支名不存在，则从仓库的 HEAD 或指定的目标分支创建。您可以使用 [`/target_branch` 快速操作](../quick_actions.md) 指定目标分支。如果源分支已经存在，补丁将应用在它之上。

## 设置默认目标项目

除非涉及派生，否则合并请求具有相同的源项目和目标项目。创建新的合并请求时，创建项目的派生可能会导致以下任一情况：

- 您的目标是上游项目（您派生的项目和默认选项）。
- 您的目标是您自己的派生项目。

要让来自派生项目的合并请求默认针对您自己的派生项目（而不是上游项目），您可以更改默认值。

1. 在顶部栏上，选择 **菜单 > 项目**。
1. 在左侧菜单中，选择 **设置 > 通用 > 合并请求**。
1. 在 **目标项目** 部分，选择要用于默认目标项目的选项。
1. 选择 **保存更改**。
