---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 压缩和合并 **(FREE)**

<!--
> - [Moved](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/18956) from GitLab Premium to GitLab Free in 11.0.
-->

通过压缩和合并，您可以将所有合并请求的提交合并为一个并保留干净的历史记录。

压缩允许您在接受合并请求时整理分支的提交历史记录。它将合并请求中的所有更改作为单个提交应用，然后使用为项目设置的合并方法合并该提交。

换句话说，压缩合并请求会将一长串提交：

![List of commits from a merge request](img/squash_mr_commits.png)

变为合并的单个提交：

![A squashed commit followed by a merge commit](img/squash_squashed_commit.png)

NOTE:
本示例中的压缩提交之后是合并提交，因为此仓库的合并方法使用合并提交。您可以在 **项目设置 > 通用 > 合并请求 > 合并方法 > 快进式合并** 中禁用合并提交。

压缩提交的默认提交消息取自合并请求标题。
您可以编辑压缩提交的默认消息<!--[编辑压缩提交的默认消息](commit_templates.md)-->。

也可以在合并合并请求之前进行自定义。

![A squash commit message editor](img/squash_mr_message.png)

压缩也适用于快进式合并策略，请参阅 [压缩和快进式合并](#压缩和快进式合并) 了解更多详细信息。

## 用例

在功能分支上工作时，有时您想提交当前的进度，但并不真正关心提交消息。那些 “work in progress 提交” 不一定包含重要信息，因此您宁愿不将它们包含在目标分支中。

使用压缩和合并，当合并请求准备好合并时，您所要做的就是在按下合并之前，启用压缩以将合并请求中的提交合并为单个提交。

这样，您的基础分支的历史记录保持干净，并带有有意义的提交消息，并且：

- 如有必要，[还原](revert_changes.md)更简单。
- 合并的分支保留完整的提交历史。

## 为合并请求启用压缩

任何可以创建或编辑合并请求的人都可以选择在合并请求表单上压缩它。用户可以在创建合并请求时选中或清除复选框：

![Squash commits checkbox on edit form](img/squash_edit_form.png)

提交合并请求后，仍然可以通过编辑合并请求描述来启用或禁用压缩和合并：

1. 滚动到合并请求页面的顶部，然后单击 **编辑**。
1. 向下滚动到合并请求表单的末尾，然后选中复选框 **接受合并请求时压缩提交**。

然后可以在接受合并请求时覆盖此设置。
在合并请求部件的末尾，在 **合并** 按钮旁边，可以选中或取消选中 **压缩提交** 复选框：

![Squash commits checkbox on accept merge request form](img/squash_mr_widget.png)

请注意，根据项目的[压缩提交选项](#压缩提交选项)配置，压缩和合并可能不可用。

## 压缩提交的提交元数据

压缩提交具有以下元数据：

- 消息：压缩提交的消息，或自定义消息。
- 作者：合并请求的作者。
- 提交者：发起压缩的用户。

## 压缩和快进式合并

当项目启用了[快进合并设置](fast_forward_merge.md#启用快进式合并)时，合并请求必须能够在不压缩的情况下快进。这是因为压缩仅在接受合并请求时可用，因此合并请求可能需要在压缩之前变基，即使压缩本身可以被视为等同于变基。

## 压缩提交选项

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/17613) in GitLab 13.2.
> - Deployed behind a feature flag, disabled by default.
> - [Enabled by default](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/39382) in GitLab 13.3.
> - Enabled on GitLab.com.
> - Can be enabled per project.
> - Recommended for production use.
-->

> - 部署在功能标志后面，默认禁用
>  - 可以按项目启用
>  - 推荐用于生产使用

使用压缩提交选项，您可以为您的项目配置压缩和合并的行为。
要进行设置，请导航到您项目的 **设置 > 通用** 并展开 **合并请求**。
您可以从这些选项中进行选择，这些选项会影响提交给您的项目的现有和新的合并请求：

- **不允许**：用户不能在合并前立即使用压缩和合并压缩所有提交。用于启用或禁用它的复选框未选中并且对用户隐藏。
- **允许**：用户可以在合并请求的基础上启用压缩和合并。默认情况下未选中（禁用）复选框，但允许用户启用它。
- **鼓励**：用户可以在合并请求的基础上启用压缩和合并。默认情况下选中（启用）复选框以鼓励其使用，但允许用户禁用它。
- **必需**：对所有合并请求启用压缩和合并，因此它总是被执行。启用或禁用它的复选框被选中并对用户隐藏。

创建合并请求和编辑现有请求的描述时，会显示“压缩和合并”复选框，除非“压缩提交选项”设置为 **不允许** 或 **需要**。

NOTE:
如果您的项目设置为**不允许**压缩和合并，用户仍然可以选择通过命令行压缩本地提交，并在合并之前强制推送到他们的远端分支。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
