---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto
---

# 允许跨派生项目的合并请求协作 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/17395) in GitLab 10.6.
-->

当用户从派生项目打开合并请求时，他们可以选择允许上游成员在源分支上与他们协作。这允许上游项目的成员在合并之前进行小的修复或变基分支，减少接受外部贡献的来回次数。

此功能可用于跨可公开访问的派生项目的合并请求。

为合并请求启用后，对项目目标分支具有合并访问权限的成员将被授予对合并请求源分支的写入权限。

## 启用来自上游成员的提交编辑

在 13.7 和之后的版本，此设置默认启用。它可以由具有开发人员角色的用户更改源项目。启用后，上游成员可以重试合并请求的流水线和作业：

1. 在创建或编辑合并请求时，滚动到 **贡献**，然后选中 **允许具有合并到目标分支权限的成员提交** 复选框。
1. 完成创建合并请求。

创建合并请求后，合并请求部件会显示一条消息：**允许可以合并的成员添加提交。**

## 作为上游成员推送到派生

如果合并请求的创建者启用了上游成员的贡献，您可以直接推送到派生仓库的分支。

假如：

- 派生的项目 URL 是 `git@gitlab.cn:thedude/awesome-project.git`。
- 合并请求的分支是 `update-docs`。

要查找和处理来自派生项目的更改：

1. 打开合并请求页面，选择**概览**标签。
1. 滚动到合并请求部件，然后选择 **检出分支**：
   ![Check out branch button](img/commit-button_v13_12.png)
1. 在模态窗口中，为步骤 1 选择 **{copy-to-clipboard}** (**Copy**) ，将 `git fetch` 和 `git checkout` 指令复制到剪贴板。将命令（举例如下）粘贴到您的终端中：

   ```shell
   git fetch git@gitlab.cn:thedude/awesome-project.git update-docs
   git checkout -b thedude-awesome-project-update-docs FETCH_HEAD
   ```

   这些命令从派生的项目中获取分支，并基于获取的分支创建本地分支。

1. 对分支的本地副本进行更改，然后提交它们。
1. 在您的终端中，将您的本地更改推送回派生项目。此命令将本地分支 `thedude-awesome-project-update-docs` 推送到 `git@gitlab.cn:thedude/awesome-project.git` 仓库的 `update-docs` 分支：

   ```shell
   git push git@gitlab.cn:thedude/awesome-project.git thedude-awesome-project-update-docs:update-docs
   ```

   注意两个分支之间的冒号 (`:`)。

## 故障排查

### 派生项目的 MR 页面无法获取流水线状态

当用户派生项目时，派生副本的权限不会从原始项目复制。在上游项目中的成员可以查看或合并合并请求中的更改之前，派生项目的创建者必须授予对派生副本的权限。

要在返回原始项目的派生项目的合并请求页面，查看流水线状态：

1. 创建一个包含所有上游成员的群组。
1. 进入派生项目中的**项目信息 > 成员**页面，邀请新创建的群组加入派生项目。

<!-- ## Troubleshooting
Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
