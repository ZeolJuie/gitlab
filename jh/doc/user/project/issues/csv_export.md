---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 导出议题到 CSV **(FREE)**

<!--
> Moved to GitLab Free in 12.10.
-->

您可以将问题从极狐GitLab 导出为 CSV 文件，这些文件将作为附件发送到您的默认通知电子邮件地址。

**将问题导出到 CSV** 使您和您的团队能够将从问题中收集的所有数据导出到 **comma-separated values** (CSV) 文件，以纯文本形式存储表格数据。

> *CSV 是一种将数据从一个程序获取到另一个程序的便捷方式，其中一个程序无法读取其他程序的正常输出。* [参考](https://www.quora.com/What-is-a-CSV-file-and-its-uses)

<!-- vale gitlab.Spelling = NO -->

CSV 文件可用于任何绘图仪或基于电子表格的程序，例如 Microsoft Excel、Open Office Calc 或 Google Sheets。

<!-- vale gitlab.Spelling = YES -->

以下是将议题导出为 CSV 文件的一些用途：

- 制作议题快照以供离线分析，或与可能不在极狐GitLab 中的其他团队交流。
- 从 CSV 数据创建图表、图形和图表。
- 出于审计或共享原因以任何其他格式呈现数据。
- 将其他地方的议题导入极狐GitLab 之外的系统。
- 长期问题的数据分析，同时创建多个快照。
- 使用长期数据收集问题中给出的相关反馈，并根据实际指标改进您的产品。

## 选择要包含的议题

选择项目后，您可以从议题页面使用搜索栏，以及全部/打开/关闭选项卡缩小要导出的问题的范围。返回的所有议题都将导出，包括第一页上未显示的议题。

![CSV export button](img/csv_export_button_v12_9.png)

极狐GitLab 要求您确认导出的议题数量和电子邮件地址，然后准备电子邮件。

![CSV export modal dialog](img/csv_export_modal.png)

## 排序

导出的议题始终按 `Issue ID` 排序。

## 格式

数据使用逗号作为列分隔符进行编码，如果需要，使用 `"` 来引用字段，并使用换行符分隔行。第一行包含标题，下表列出了标题以及值的描述：

| 列            | 描述 |
|-------------------|-------------|
| Issue ID          | 议题 `iid` |
| URL               | 极狐GitLab 上议题的链接 |
| Title             | 议题 `title` |
| State             | `Open` 或 `Closed` |
| Description       | 议题 `description` |
| Author            | 议题作者的完整名称 |
| Author Username   | 作者的用户名，省略了 `@` 符号 |
| Assignee          | 议题指派人的完整名称 |
| Assignee Username | 指派人的用户名，省略了 `@` 符号 |
| Confidential      | `Yes` 或 `No` |
| Locked            | `Yes` 或 `No` |
| Due Date          | 格式为 `YYYY-MM-DD` |
| Created At (UTC)  | 格式为 `YYYY-MM-DD HH:MM:SS` |
| Updated At (UTC)  | 格式为 `YYYY-MM-DD HH:MM:SS` |
| Milestone         | 议题里程碑的标题 |
| Weight            | 议题权重 |
| Labels            | 用 `,` 连接的任何标记的标题 |
| Time Estimate     | <!--[Time estimate](../time_tracking.md#estimates) in seconds-->时间估计，以秒为单位 |
| Time Spent        | <!--[Time spent](../time_tracking.md#time-spent) in seconds-->时间花费，以秒为单位 |
| Epic ID           | 父史诗的 ID **(ULTIMATE)**<!--, introduced in 12.7--> |
| Epic Title        | 父史诗的标题 **(ULTIMATE)**<!--, introduced in 12.7--> |

## 限制

- 将问题导出到 CSV，在群组的议题列表中不可用。
- 议题作为电子邮件附件发送，导出限制为 15 MB，以确保在一系列电子邮件提供商之间成功交付。如果您达到了限制，我们建议在导出前缩小搜索范围，或许可以分别导出打开和关闭的问题。
- CSV 文件是纯文本文件。这意味着导出的 CSV 文件不包含任何议题附件。
