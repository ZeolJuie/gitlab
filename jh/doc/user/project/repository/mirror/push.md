---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/workflow/repository_mirroring.html'
---

# 推送镜像 **(FREE)**

> 引入于 13.5 版本：LFS 通过 HTTPS 支持。

*推送镜像*是一个下游仓库，它[镜像](index.md) 对上游仓库所做的提交。推送镜像被动地接收对上游仓库所做提交的副本。为防止镜像与上游仓库分叉，请勿将提交直接推送到下游镜像。改为将提交推送到上游仓库。

虽然[拉取镜像](pull.md)定期从上游仓库检索更新，但推送镜像仅在以下情况下接收更改：

- 提交被推送到上游极狐GitLab 仓库。
- 管理员[强制更新镜像](index.md#强制更新)。

当您将更改推送到上游仓库时，推送镜像会收到它：

- 在五分钟内。
- 在一分钟内，如果您启用 **仅镜像受保护的分支**。

在分支分叉的情况下，**镜像的仓库**部分会显示错误。

## 配置推送镜像

要为现有项目设置推送镜像：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > 仓库**。
1. 展开 **镜像仓库**。
1. 输入存储库 URL。
1. 在 **镜像方向** 下拉列表中，选择 **推送**。
1. 选择 **验证方式**。您可以使用密码或 [SSH 密钥](index.md#ssh-验证)进行身份验证。
1. 如有必要，请选择 **仅镜像受保护的分支**。
1. 如果需要，选择**保留分叉的 refs**。
1. 要保存配置，请选择 **镜像仓库**。

### 通过 API 配置推送镜像

您还可以通过远端镜像 API<!--[远端镜像 API](../../../../api/remote_mirrors.md)--> 创建和修改项目推送镜像。

## 保留分叉的 refs

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/208828) in GitLab 13.0.
-->

默认情况下，如果远端（下游）镜像上的任何引用（分支或标签）与本地仓库不同，上游仓库将覆盖远端上的任何更改：

1. 仓库将 `main` 和 `develop` 分支镜像到远程。
1. 远端镜像的 `develop` 添加了一个新的commit。
1. 下一次推送更新远端镜像以匹配上游仓库。
1. 远端镜像上添加到 `develop` 的新 commit 丢失了。

如果选择**保留分叉的 refs**，则更改的处理方式不同：

1. 跳过远端镜像上 `develop` 分支的更新。
1. 远端镜像上的 `develop` 分支保留了上游仓库不存在的提交。远端镜像中存在的任何 refs（而不是上游）保持不变。
1. 更新标记为失败。

创建镜像后，只能通过远程镜像 API<!--[远程镜像 API](../../../../api/remote_mirrors.md)--> 修改**保留分叉的 refs** 的值。

<!--
## 设置从极狐GitLab 到 GitHub 的推送镜像

To configure a mirror from GitLab to GitHub:

1. Create a [GitHub personal access token](https://docs.github.com/en/github/authenticating-to-github/keeping-your-account-and-data-secure/creating-a-personal-access-token)
   with `public_repo` selected.
1. Enter a **Git repository URL** with this format:
   `https://<your_github_username>@github.com/<your_github_group>/<your_github_project>.git`.
1. For **Password**, enter your GitHub personal access token.
1. Select **Mirror repository**.

The mirrored repository is listed. For example:

```plaintext
https://*****:*****@github.com/<your_github_group>/<your_github_project>.git
```

The repository pushes shortly thereafter. To force a push, select **Update now** (**{retry}**).

## Set up a push mirror from GitLab to AWS CodeCommit

AWS CodeCommit push mirroring is the best way to connect GitLab repositories to
AWS CodePipeline. GitLab is not yet supported as one of their Source Code Management (SCM) providers.
Each new AWS CodePipeline needs significant AWS infrastructure setup. It also
requires an individual pipeline per branch.

If AWS CodeDeploy is the final step of a CodePipeline, you can, instead combine
these tools to create a deployment:

- GitLab CI/CD pipelines.
- The AWS CLI in the final job in `.gitlab-ci.yml` to deploy to CodeDeploy.

NOTE:
GitLab-to-AWS-CodeCommit push mirroring cannot use SSH authentication until [GitLab issue 34014](https://gitlab.com/gitlab-org/gitlab/-/issues/34014) is resolved.

To set up a mirror from GitLab to AWS CodeCommit:

1. In the AWS IAM console, create an IAM user.
1. Add the following least privileges permissions for repository mirroring as an **inline policy**.

   The Amazon Resource Names (ARNs) must explicitly include the region and account. This IAM policy
   grants privilege for mirroring access to two sample repositories. These permissions have
   been tested to be the minimum (least privileged) required for mirroring:

   ```json
   {
       "Version": "2012-10-17",
       "Statement": [
           {
               "Sid": "MinimumGitLabPushMirroringPermissions",
               "Effect": "Allow",
               "Action": [
                   "codecommit:GitPull",
                   "codecommit:GitPush"
               ],
               "Resource": [
                 "arn:aws:codecommit:us-east-1:111111111111:MyDestinationRepo",
                 "arn:aws:codecommit:us-east-1:111111111111:MyDemo*"
               ]
           }
       ]
   }
   ```

1. After the user is created, select the AWS IAM user name.
1. Select the **Security credentials** tab.
1. Under **HTTPS Git credentials for AWS CodeCommit**, select **Generate credentials**.

   NOTE:
   This Git user ID and password is specific to communicating with CodeCommit. Do
   not confuse it with the IAM user ID or AWS keys of this user.

1. Copy or download the special Git HTTPS user ID and password.
1. In the AWS CodeCommit console, create a new repository to mirror from your GitLab repository.
1. Open your new repository, and then select **Clone URL > Clone HTTPS** (not **Clone HTTPS (GRC)**).
1. In GitLab, open the repository to be push-mirrored.
1. On the left sidebar, select **Settings > Repository**, and then expand **Mirroring repositories**.
1. Fill in the **Git repository URL** field using this format:

   ```plaintext
   https://<your_aws_git_userid>@git-codecommit.<aws-region>.amazonaws.com/v1/repos/<your_codecommit_repo>
   ```

   Replace `<your_aws_git_userid>` with the AWS **special HTTPS Git user ID**
   from the IAM Git credentials created earlier. Replace `<your_codecommit_repo>`
   with the name of your repository in CodeCommit.

1. For **Mirror direction**, select **Push**.
1. For **Authentication method**, select **Password**. Fill in the **Password** box
   with the special IAM Git clone user ID **password** created earlier in AWS.
1. Leave the option **Only mirror protected branches** for CodeCommit. It pushes more
   frequently (from every five minutes to every minute).

   CodePipeline requires individual pipeline setups for named branches you want
   to have a AWS CI setup for. Because feature branches with dynamic names are unsupported,
   configuring **Only mirror protected branches** doesn't cause flexibility problems
   with CodePipeline integration. You must also protect all the named branches you
   want to build CodePipelines for.

1. Select **Mirror repository**. You should see the mirrored repository appear:

   ```plaintext
   https://*****:*****@git-codecommit.<aws-region>.amazonaws.com/v1/repos/<your_codecommit_repo>
   ```

To test mirroring by forcing a push, select **Update now** (the half-circle arrows).
If **Last successful update** shows a date, you have configured mirroring correctly.
If it isn't working correctly, a red `error` tag appears, and shows the error message as hover text.
-->

## 设置一个推送镜像到另一个已激活 2FA 的实例

1. 在目标实例上，创建一个具有 `write_repository` 作用域的[个人访问令牌](../../../profile/personal_access_tokens.md)。
1. 在源实例上：
    1. 使用以下格式输入 **Git 存储库 URL**：`https://oauth2@<destination host>/<your_gitlab_group_or_name>/<your_gitlab_project>.git`。
    1. 输入 **密码**。使用在目标实例上创建的 GitLab 个人访问令牌。
    1. 选择 **镜像仓库**。

<!--
## Related topics

- [Remote mirrors API](../../../../api/remote_mirrors.md).
-->