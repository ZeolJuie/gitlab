---
stage: Ecosystem
group: Integrations
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 集成 **(FREE)**

集成允许您将极狐GitLab 与其他应用程序集成。它们类似于插件，在向极狐GitLab 添加功能时，允许有很大的自由度。

## 访问集成

您可以在项目的 **设置 > 集成** 页面下找到可用的集成。

<!--
支持 20 多种集成。选择您要配置的一项。
-->

## 集成列表

单击服务链接以查看更多配置说明和详细信息。

| 服务                                                   | 描述                                                                                  | 服务钩子          |
| --------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------- |
| [禅道](zentao.md)                                   | 使用禅道作为议题跟踪器。                                                           | **{dotted-circle}** No |

<!--
| Service                                                   | Description                                                                                  | Service hooks          |
| --------------------------------------------------------- | -------------------------------------------------------------------------------------------- | ---------------------- |
| [Asana](asana.md)                                         | Add commit messages as comments to Asana tasks.                                              | **{dotted-circle}** No |
| Assembla                                                  | Manage projects.                                                                             | **{dotted-circle}** No |
| [Atlassian Bamboo CI](bamboo.md)                          | Run CI/CD pipelines with Atlassian Bamboo.                                                   | **{check-circle}** Yes |
| [Bugzilla](bugzilla.md)                                   | Use Bugzilla as the issue tracker.                                                           | **{dotted-circle}** No |
| Buildkite                                                 | Run CI/CD pipelines with Buildkite.                                                          | **{check-circle}** Yes |
| Campfire                                                  | Connect to chat.                                                                             | **{dotted-circle}** No |
| [Confluence Workspace](../../../api/services.md#confluence-service) | Replace the link to the internal wiki with a link to a Confluence Cloud Workspace. | **{dotted-circle}** No |
| [Custom issue tracker](custom_issue_tracker.md)           | Use a custom issue tracker.                                                                  | **{dotted-circle}** No |
| [Datadog](../../../integration/datadog.md)                | Trace your GitLab pipelines with Datadog.                                                    | **{check-circle}** Yes |
| [Discord Notifications](discord_notifications.md)         | Send notifications about project events to a Discord channel.                                | **{dotted-circle}** No |
| Drone CI                                                  | Run CI/CD pipelines with Drone.                                                              | **{check-circle}** Yes |
| [Emails on push](emails_on_push.md)                       | Send commits and diff of each push by email.                                                 | **{dotted-circle}** No |
| [EWM](ewm.md)                                             | Use IBM Engineering Workflow Management as the issue tracker.                                | **{dotted-circle}** No |
| [External wiki](../wiki/index.md#link-an-external-wiki)   | Link an external wiki.                                          | **{dotted-circle}** No |
| [Flowdock](../../../api/services.md#flowdock)             | Send notifications from GitLab to Flowdock flows. | **{dotted-circle}** No |
| [GitHub](github.md)                                       | Obtain statuses for commits and pull requests.                                               | **{dotted-circle}** No |
| [Google Chat](hangouts_chat.md)                           | Send notifications from your GitLab project to a room in Google Chat.| **{dotted-circle}** No |
| [irker (IRC gateway)](irker.md)                           | Send IRC messages.                                                                           | **{dotted-circle}** No |
| [Jenkins](../../../integration/jenkins.md)                | Run CI/CD pipelines with Jenkins.                                                            | **{check-circle}** Yes |
| JetBrains TeamCity CI                                     | Run CI/CD pipelines with TeamCity.                                                           | **{check-circle}** Yes |
| [Jira](../../../integration/jira/index.md)                | Use Jira as the issue tracker.                                                               | **{dotted-circle}** No |
| [Mattermost notifications](mattermost.md)                 | Send notifications about project events to Mattermost channels.                              | **{dotted-circle}** No |
| [Mattermost slash commands](mattermost_slash_commands.md) | Perform common tasks with slash commands.                                                    | **{dotted-circle}** No |
| [Microsoft Teams notifications](microsoft_teams.md)       | Receive event notifications.                                                                 | **{dotted-circle}** No |
| Packagist                                                 | Keep your PHP dependencies updated on Packagist.                                             | **{check-circle}** Yes |
| Pipelines emails                                          | Send the pipeline status to a list of recipients by email.                                   | **{dotted-circle}** No |
| [Pivotal Tracker](pivotal_tracker.md)                      | Add commit messages as comments to Pivotal Tracker stories.                                                    | **{dotted-circle}** No |
| [Prometheus](prometheus.md)                               | Monitor application metrics.                                                                 | **{dotted-circle}** No |
| Pushover                                                  | Get real-time notifications on your device.                                                  | **{dotted-circle}** No |
| [Redmine](redmine.md)                                     | Use Redmine as the issue tracker.                                                            | **{dotted-circle}** No |
| [Slack application](gitlab_slack_application.md)          | Use Slack's official GitLab application.                                                     | **{dotted-circle}** No |
| [Slack notifications](slack.md)                           | Send notifications about project events to Slack.                                            | **{dotted-circle}** No |
| [Slack slash commands](slack_slash_commands.md)           | Enable slash commands in workspace.                                                          | **{dotted-circle}** No |
| [Unify Circuit](unify_circuit.md)                         | Receive events notifications.                                                                | **{dotted-circle}** No |
| [Webex Teams](webex_teams.md)                             | Receive events notifications.                                                                | **{dotted-circle}** No |
| [YouTrack](youtrack.md)                                   | Use YouTrack as the issue tracker.                                                           | **{dotted-circle}** No |
-->

## 推送 hooks 限制

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/17874) in GitLab 12.4.
-->

如果单个推送包含对三个以上分支或标签的更改，则不会执行 `push_hooks` 和 `tag_push_hooks` 事件支持的服务。

可以通过 `push_event_hooks_limit` 应用设置<!--[`push_event_hooks_limit` 应用设置](../../../api/settings.md#list-of-settings-that-can-be-accessed-via-api-calls)-->更改支持的分支或标签的数量。

## 项目集成管理

项目集成管理让您可以控制一个实例的所有项目的集成设置。在项目级别，管理员可以选择是继承实例配置还是提供自定义设置。

<!--阅读更多关于[项目集成管理](../../admin_area/settings/project_integration_management.md)。-->

<!--
## 故障排查

Some integrations use service hooks for integration with external applications. To confirm which ones use service hooks, see the [integrations listing](#integrations-listing) above. Learn more about [troubleshooting service hooks](webhooks.md#troubleshoot-webhooks).
-->

### 未初始化的仓库

当您尝试在未初始化的仓库上设置某些集成时，集成失败并显示错误 `Test Failed. Save Anyway`。一些集成使用推送数据来构建测试负载，当项目中尚不存在推送事件时会发生此错误。

要解决此错误，请通过将测试文件推送到项目并再次设置集成来初始化仓库。

<!--
## Contributing to integrations

Because GitLab is open source we can ship with the code and tests for all
plugins. This allows the community to keep the plugins up to date so that they
always work in newer GitLab versions.

For an overview of what integrations are available, please see the
[integrations source directory](https://gitlab.com/gitlab-org/gitlab/-/tree/master/app/models/integrations).

Contributions are welcome!
-->
