---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 受保护的标签 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/10356) in GitLab 9.1.
-->

受保护的标签：

- 允许控制谁有权创建标签。
- 一旦创建，防止意外更新或删除。

每个规则都允许您匹配：

- 一个单独的标签名称。
- 通配符一次控制多个标签。

此功能由[受保护的分支](protected_branches.md)演变而来。

## 谁可以修改受保护的标签

默认情况下：

- 要创建标签，您必须具有维护者角色。
- 没有人可以更新或删除标签。

## 配置受保护的标签

要保护标签，您至少需要具有维护者角色。

1. 转到项目的 **设置 > 仓库**。

1. 从 **标签** 下拉菜单中，选择要保护或键入的标签，然后单击 **创建通配符**。在下面的屏幕截图中，我们选择保护所有匹配 `v*` 的标签：

   ![Protected tags page](img/protected_tags_page_v12_3.png)

1. 从 **允许创建** 下拉列表中，选择有权创建匹配标签的用户，然后单击 **保护**：

   ![Allowed to create tags dropdown](img/protected_tags_permissions_dropdown_v12_3.png)

1. 完成后，受保护的标签会显示在 **受保护的标签** 列表中：

   ![Protected tags list](img/protected_tags_list_v12_3.png)

## 受保护的标签通配符

您可以指定一个保护标签通配符，它会保护所有与通配符匹配的标签。例如：

| 受保护的标签通配符 | 匹配标签                 |
|------------------------|-------------------------------|
| `v*`                   | `v1.0.0`, `version-9.1`       |
| `*-deploy`             | `march-deploy`, `1.0-deploy`  |
| `*gitlab*`             | `gitlab`, `gitlab/v1`         |
| `*`                    | `v1.0.1rc2`, `accidental-tag` |

两个不同的通配符可能匹配同一个标签。 例如，`*-stable` 和 `production-*` 都会匹配一个 `production-stable` 标签。
在这种情况下，如果这些受保护标签中的*任何*具有类似 **允许创建** 的设置，那么 `production-stable` 也会继承此设置。

如果单击受保护标签的名称，系统会显示所有匹配标签的列表：

![Protected tag matches](img/protected_tag_matches.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
