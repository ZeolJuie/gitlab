---
type: index, reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 里程碑 **(FREE)**

极狐GitLab 中的里程碑是一种跟踪议题和合并请求的方法，这些请求是为了在特定时间段内实现更广泛的目标而创建的。

里程碑允许您组织议题并将请求合并到一个有凝聚力的组中，并具有可选的开始日期和可选的截止日期。

## 作为敏捷 sprint 的里程碑

里程碑可以用作敏捷 sprint，以便您可以跟踪与特定 sprint 相关的所有议题和合并请求。要这样做：

1. 设置里程碑开始日期和截止日期以代表敏捷 sprint 的开始和结束。
1. 将里程碑标题设置为您的敏捷 sprint 的名称，例如 `November 2018 sprint`。
1. 通过从议题的右侧边栏中关联所需的里程碑，将议题添加到您的敏捷 sprint。

## 作为发布的里程碑

同样，里程碑可以用作发布。要这样做：

1. 设置里程碑截止日期以代表您的发布日期，并将里程碑开始日期留空。
1. 将里程碑标题设置为您发布的版本，例如 `Version 9.4`。
1. 通过从议题的右侧边栏中关联所需的里程碑，将议题添加到您的版本中。

此外，您可以将里程碑与发布功能<!--[发布功能](../releases/index.md#associate-milestones-with-a-release)-->集成。

## 项目里程碑和群组里程碑

您只能将 **项目里程碑** 分配给该项目中的议题或合并请求。
要查看项目里程碑列表，请在项目中转到 **{issues}** **议题 > 里程碑**。

您可以将 **群组里程碑** 分配给该群组中任何项目的任何议题或合并请求。
要查看群组里程碑列表，请在群组中转到 **{issues}** **议题 > 里程碑**。

您还可以在仪表板里程碑列表中查看您有权访问的所有里程碑。
要查看您有权访问的项目里程碑和组里程碑，请选择顶部栏中的 **菜单 > 里程碑**。

<!--
有关项目和组里程碑 API 的信息，请参阅：

- [Project Milestones API](../../../api/milestones.md)
- [Group Milestones API](../../../api/group_milestones.md)
-->

NOTE:
如果您在一个群组中并选择 **议题 > 里程碑**，极狐GitLab 会显示群组里程碑和该群组中项目的里程碑。
如果您在一个项目中并选择 **议题 > 里程碑**，极狐GitLab 将仅显示该项目的里程碑。

## 创建里程碑

NOTE:
创建里程碑需要开发者或更高的权限级别。

### 新建项目里程碑

要创建项目里程碑：

1. 在项目中，转到 **{issues}** **议题 > 里程碑**。
1. 选择 **新建里程碑**。
1. 输入标题、可选描述、可选开始日期和可选截止日期。
1. 选择 **新建里程碑**。

![New project milestone](img/milestones_new_project_milestone.png)

### 新建群组里程碑

要创建群组里程碑：

1. 在小组中，转到 **{issues}** **议题 > 里程碑**。
1. 选择 **新建里程碑**。
1. 输入标题、可选描述、可选开始日期和可选截止日期。
1. 选择 **新建里程碑**。

![New group milestone](img/milestones_new_group_milestone_v13_5.png)

## 编辑里程碑

NOTE:
编辑里程碑需要开发者或更高的权限级别。

要编辑里程碑：

1. 在项目或群组中，转至 **{issues}** **议题 > 里程碑**。
1. 选择里程碑的标题。
1. 选择 **编辑**。

您可以通过选择 **删除** 按钮来删除里程碑。

### 将项目里程碑提升为群组里程碑

如果您要扩展群组中的项目数量，您可能希望在该群组的项目之间共享相同的里程碑。您还可以将项目里程碑提升为群组里程碑，以便将它们提供给同一群组中的其他项目。

从项目里程碑列表页面，您可以将项目里程碑提升为群组里程碑。
这会将具有相同名称的该组中所有项目的所有项目里程碑合并为一个群组里程碑。以前分配给这些项目里程碑之一的所有议题和合并请求都分配给新的群组里程碑。此操作无法撤消并且更改是永久性的。

WARNING:
对于 12.4 及更早版本，当您将项目里程碑提升为群组里程碑时，某些信息会丢失。并非项目里程碑视图上的所有功能都在群组里程碑视图中可用。如果将项目里程碑提升为群组里程碑，则会失去这些功能。访问[里程碑视图](#里程碑视图)，了解群组里程碑视图中缺少哪些功能。

![Promote milestone](img/milestones_promote_milestone.png)

## 从侧边栏分配里程碑

每个议题和合并请求都可以分配一个里程碑。在侧边栏中的每个议题和合并请求页面上都可以看到里程碑。它们也在议题看板上可见。从侧边栏中，您可以为对象分配或取消分配里程碑。您还可以在评论中使用[快速操作](../quick_actions.md)执行。[如前所述](#项目里程碑和群组里程碑)，对于给定的议题或合并请求，可以选择项目里程碑和群组里程碑并将其分配给对象。

## 按里程碑过滤议题和合并请求

### 在列表页面中过滤

从项目和群组议题/合并请求列表页面，您可以按群组和项目里程碑过滤<!--[过滤](../../search/index.md#issues-and-merge-requests)-->。

### 在议题看板中过滤

从[项目议题看板](../issue_board.md)，您可以按群组里程碑和项目里程碑进行过滤：

- 搜索和过滤栏<!--[搜索和过滤栏](../../search/index.md#issue-boards)-->
- 议题看板配置<!--[议题看板配置](../issue_board.md#configurable-issue-boards)-->

从[群组议题看板](../issue_board.md#群组议题看板)，您可以仅按群组里程碑进行过滤：

- 搜索和过滤栏<!--[搜索和过滤栏](../../search/index.md#issue-boards)-->
- 议题看板配置<!--[议题看板配置](../issue_board.md#configurable-issue-boards)-->

### 特殊里程碑过滤器

按里程碑过滤时，除了选择特定的项目里程碑或群组里程碑外，您还可以选择特殊的里程碑过滤器。

- **无**：显示没有指定里程碑的议题或合并请求。
- **任何**：显示具有指定里程碑的议题或合并请求。
- **即将到来的**：显示已分配为开放的里程碑并在未来具有最近截止日期的议题或合并请求。
- **已开始**：显示具有开放分配里程碑且开始日期早于今天的议题或合并请求。

## 里程碑视图

里程碑视图显示标题和描述。

在这些选项卡下方还有显示以下内容的选项卡：

- **问题**：显示分配给里程碑的所有议题。它们显示在如下三列中：
   - 未启动的议题（开放和未分配）
   - 处理中的议题（开放和已分配）
   - 已完成的议题（已关闭）
- **合并请求**：显示分配给里程碑的所有合并请求。它们显示在如下四列中：
   - 正在进行的工作（开放和未分配）
   - 等待合并（开放和未分配）
   - 拒绝（已关闭）
   - 合并
- **参与者**：显示分配给里程碑的议题的所有指派人。
- **标记**：显示分配给里程碑的议题中使用的所有标记。

### 项目燃尽图

对于项目里程碑，里程碑视图中有一个[燃尽图](burndown_and_burnup_charts.md)，显示完成里程碑的进度。

![burndown chart](img/burndown_chart_v13_6.png)

### 群组燃尽图

对于群组里程碑，里程碑视图中有一个[燃尽图](burndown_and_burnup_charts.md)，显示完成里程碑的进度。

### 里程碑侧边栏

里程碑视图上的里程碑侧栏显示以下内容：

- 完成百分比，计算方法为已关闭议题的数量除以议题总数。
- 开始日期和截止日期。
- 在分配给里程碑的所有议题和合并请求上花费的总时间。
- 分配给里程碑的所有议题的总议题权重。

![Project milestone page](img/milestones_project_milestone_page_sidebar_v13_11.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
