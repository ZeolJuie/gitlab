---
type: reference
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 燃尽图和燃起图 **(PREMIUM)**

[燃尽](#燃尽图)和[燃起](#燃起图)图显示完成里程碑的进度。

![burndown and burnup chart](img/burndown_and_burnup_charts_v13_6.png)

## 燃尽图 **(PREMIUM)**

<!--
> - [Added](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/6495) to GitLab 11.2 for group milestones.
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/6903) [fixed burndown charts](#fixed-burndown-charts) in GitLab 13.6.
> - Moved to GitLab Premium in 13.9.
-->

燃尽图显示了里程碑过程中的议题数量。

![burndown chart](img/burndown_chart_v13_6.png)

一目了然，您会看到完成给定里程碑的当前状态。
如果没有此图，您将不得不组织里程碑中的数据并自己绘制它以获得相同的感受。

系统为您绘制并以清晰美观的图表形式呈现。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview, check the video demonstration on [Mapping work versus time with burndown charts](https://www.youtube.com/watch?v=zJU2MuRChzs).
-->

查看项目的燃尽图：

1. 在项目中，导航到 **议题 > 里程碑**。
1. 从列表中选择一个里程碑。

查看群组的燃尽图：

1. 在一个群组中，导航到 **议题 > 里程碑**。
1. 从列表中选择一个里程碑。

### 燃尽图用例

燃尽图通常用于跟踪和分析里程碑的完成情况。因此，它们的用例与[您将里程碑分配给的用途](index.md)相关联。

例如，假设您在一家大公司领导一个开发团队，并且您遵循以下工作流程：

- 您的公司设定了本季度的目标，在即将发布的主要版本中为您的应用程序提供 10 项新功能。
- 您创建一个里程碑，并提醒您的团队，将该里程碑分配给作为您应用程序启动一部分的每个新议题和合并请求。
- 每周，您打开里程碑，可视化进度，找出差距，并帮助您的团队完成工作。
- 每个月，您都会与您的主管联系，并从燃尽图显示该里程碑的进度。
- 到本季度末，您的团队成功实现了该里程碑的 100%，因为它在整个季度都得到了密切关注。

### 燃尽图如何工作

每个项目或群组里程碑都有一个燃尽图，这些里程碑被赋予了 **开始日期** 和 **截止日期**。

NOTE:
您可以[提升项目里程碑](index.md#将项目里程碑提升为群组里程碑)为群组里程碑，并且仍然可以看到它们的 **燃尽图**，尊重许可限制。

该图指示项目在整个里程碑中的进度（针对分配给它的议题）。

特别是，它显示了在里程碑的相应时期内的给定日期有多少议题已经或仍在打开。

也可以切换燃尽图以显示给定日期的累积未解决议题权重。使用此功能时，请确保已正确分配议题权重，因为没有权重的未解决议题会在累积值上添加为零。

### 固定的燃尽图

对于 13.6 版本之前创建的里程碑，燃尽图有一个额外的切换开关，可以在旧视图和固定视图之间切换。

| 旧视图 | 固定视图 |
| ----- | ----- |
| ![Legacy burndown chart](img/burndown_chart_legacy_v13_6.png) | ![Fixed burndown chart, showing a jump when a lot of issues were added to the milestone](img/burndown_chart_fixed_v13_6.png) |

**固定燃尽**图跟踪里程碑活动的完整历史，从其创建一直到里程碑到期。里程碑到期日过后，从里程碑中删除的议题不再影响燃尽图。

**旧版燃尽图**跟踪议题的创建时间和上次关闭时间，而不是它们的完整历史记录。对于每一天，旧版燃尽图获取未解决的议题数量和当天创建的议题数量，并减去当天关闭的议题数量。
在开始日期之前创建并分配里程碑（并在开始日期保持打开状态）的议题，被视为已在开始日期打开。
因此，当里程碑开始日期更改时，每天打开的议题数量可能会更改。
重新打开的议题被视为在上次关闭后的第二天打开。

## 燃起图 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/6903) in GitLab 13.6.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/268350) in GitLab 13.7.
> - Moved to GitLab Premium in 13.9.
-->

燃起图显示里程碑的分配和完成的工作。

![burnup chart](img/burnup_chart_v13_6.png)

查看项目的燃起图：

1. 在项目中，导航到 **议题 > 里程碑**。
1. 从列表中选择一个里程碑。

查看群组的燃起图：

1. 在一个群组中，导航到 **议题 > 里程碑**。
1. 从列表中选择一个里程碑。

### 燃起图如何工作

燃起图对总工作和已完成工作使用不同的行。总数行显示范围何时缩小或添加到里程碑。已完成的工作是对已关闭议题的计数。

燃起图可以显示里程碑每天的议题总数或总权重。使用图上方的切换按钮在总权重和权重之间切换。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
