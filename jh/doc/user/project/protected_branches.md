---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, howto
---

# 受保护的分支 **(FREE)**

在极狐GitLab 中，权限基本上是围绕对仓库和分支具有读或写权限的想法定义的。为了对某些分支施加进一步的限制，它们可以受保护。

默认情况下，您的仓库的默认分支受保护。

## 谁可以修改受保护的分支

当分支受到保护时，默认会对该分支实施以下限制。

| 操作                  | 谁可以操作                                                    |
|:-------------------------|:------------------------------------------------------------------|
| 保护分支        | 仅维护者。                                                |
| 推送到分支       | 管理员或任何具有**已允许**权限的用户。 (1) |
| 强制推送到分支 | 没有人。                                                           |
| 删除分支        | 没有人。                                                          |

1. 具有开发人员角色的用户可以在群组中创建项目，但可能不允许首次推送到[默认分支](repository/branches/default.md)。

### 设置默认分支保护级别

管理员可以在管理中心<!--[管理中心](../admin_area/settings/visibility_and_access_controls.md#protect-default-branches)-->设置默认分支保护级别。

## 配置受保护的分支

先决条件：

- 您必须至少具有维护者角色。

要保护分支：

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 从 **分支** 下拉菜单中，选择要保护的分支。
1. 从 **允许合并** 列表中，选择可以合并到此分支的角色或群组。在专业版中，您还可以添加用户。
1. 从 **允许推送** 列表中，选择可以推送到此分支的角色、群组或用户。在专业版中，您还可以添加用户。
1. 选择 **保护**。

受保护分支显示在受保护分支列表中。

## 使用通配符配置多个受保护分支

先决条件：

- 您必须至少具有维护者角色。

同时保护多个分支：

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 从 **分支** 下拉菜单中，输入分支名称和通配符。
    例如：

   | 通配符保护分支 | 匹配分支                                      |
   |---------------------------|--------------------------------------------------------|
   | `*-stable`                | `production-stable`, `staging-stable`                  |
   | `production/*`            | `production/app-server`, `production/load-balancer`    |
   | `*gitlab*`                | `gitlab`, `gitlab/staging`, `master/gitlab/production` |

1. 从 **允许合并** 列表中，选择可以合并到此分支的角色或群组。在专业版中，您还可以添加用户。
1. 从 **允许推送** 列表中，选择可以推送到此分支的角色、群组或用户。在专业版中，您还可以添加用户。
1. 选择 **保护**。

受保护分支显示在受保护分支列表中。

## 创建受保护的分支

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/53361) in GitLab 11.9.
-->

具有开发人员或更高角色的用户可以创建受保护的分支。

先决条件：

- **允许推送** 设置为 **没有人**
- **允许合并** 设置为 **开发者**。

您只能使用 UI 或 API 创建受保护的分支。
这可以防止您从命令行或 Git 客户端应用程序意外创建分支。

要通过用户界面创建新分支：

1. 转到 **仓库 > 分支**。
1. 选择 **新建分支**。
1. 填写分支名称并选择现有分支、标记或提交，作为新分支的基础。只接受现有的受保护分支和已经在受保护分支中的提交。

## 要求每个人提交受保护分支的合并请求

您可以强制每个人提交合并请求，而不是让他们直接检入受保护的分支。与 [GitLab 工作流](../../topics/gitlab_flow.md) 等类似的工作流兼容。

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 从 **分支** 下拉菜单中，选择要保护的分支。
1. 从 **允许合并** 列表中，选择 **开发人员 + 维护人员**。
1. 从 **允许推送** 列表中，选择 **没有人**。
1. 选择 **保护**。

## 允许所有人直接推送到受保护的分支

您可以允许具有写访问权限的每个人推送到受保护的分支。

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 从 **分支** 下拉菜单中，选择要保护的分支。
1. 从 **允许推送** 列表中，选择 **开发人员 + 维护人员**。
1. 选择 **保护**。

## 允许部署密钥推送到受保护的分支

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/30769) in GitLab 13.7.
> - This feature was selectively deployed in GitLab.com 13.7, and may not be available for all users.
> - This feature is available for all users in GitLab 13.9.
-->

您可以允许<!--[部署密钥](deploy_keys/index.md)-->部署密钥的所有者推送到受保护的分支。
即使用户不是相关项目的成员，部署密钥也有效。但是，部署密钥的所有者必须至少具有对项目的读取权限。

先决条件：

- 部署密钥必须为您的项目启用<!--[为您的项目启用](deploy_keys/index.md#how-to-enable-deploy-keys)-->。
- 部署密钥必须对您的项目仓库具有写访问权限<!--[写访问权限](deploy_keys/index.md#deploy-keys-permissions)-->。

要允许部署密钥推送到受保护的分支：

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 从 **分支** 下拉菜单中，选择要保护的分支。
1. 从 **允许推送** 列表中，选择部署密钥。
1. 选择 **保护**。

部署密钥在 **允许合并** 下拉列表中不可用。

## 允许强制推送受保护的分支

> - 引入于 13.10 版本，在功能标志后默认禁用。
> - 功能标志移除于 14.0 版本。

WARNING:
您可能无法使用此功能。查看上面的**版本历史**注释以了解详细信息。

您可以允许强制推送<!--[强制推送](../../topics/git/git_rebase.md#force-push)-->到受保护的分支。

要保护新分支并启用强制推送：

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 从 **分支** 下拉菜单中，选择要保护的分支。
1. 从 **允许推送** 和**允许合并** 列表中，选择您想要的设置。
1. 要允许所有具有推送访问权限的用户强制推送，请打开 **允许强制推送** 开关。
1. 要拒绝更改 `CODEOWNERS` 文件中列出的文件的代码推送，请打开 **需要代码所有者的批准** 开关。
1. 选择 **保护**。

要在已经受保护的分支上启用强制推送：

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 在受保护分支列表中，打开分支旁边的 **允许强制推送** 开关。

启用后，可以推送到此分支的成员也可以强制推送。

## 要求受保护分支的代码所有者批准 **(PREMIUM)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/13251) in GitLab Premium 12.4.
> - [In](https://gitlab.com/gitlab-org/gitlab/-/issues/35097) in [GitLab Premium](https://about.gitlab.com/pricing/) 13.5 and later, users and groups who can push to protected branches do not have to use a merge request to merge their feature branches. This means they can skip merge request approval rules.
-->

对于受保护的分支，您可以要求至少获得[代码所有者](code_owners.md)的一项批准。

要保护新分支并启用代码所有者的批准：

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 从 **分支** 下拉菜单中，选择要保护的分支。
1. 从 **允许推送** 和 **允许合并** 列表中，选择您想要的设置。
1. 打开 **需要代码所有者的批准** 开关。
1. 选择 **保护**。

要在已受保护的分支上启用代码所有者的批准：

1. 转到您的项目并选择 **设置 > 仓库**。
1. 展开 **受保护的分支**。
1. 在受保护分支列表中，打开分支旁边的 **需要代码所有者批准** 开关。

启用后，这些分支的所有合并请求都需要每个匹配规则的代码所有者批准，然后才能合并。
此外，如果匹配规则，则拒绝直接推送到受保护分支。

任何未在 `CODEOWNERS` 文件中指定的用户都不能推送指定文件或路径的更改，除非他们被特别允许。
您不必限制开发人员直接推送到受保护的分支。相反，您可以限制推送到某些需要代码所有者审核的文件。

在专业版 13.5 及更高版本中，允许推送到受保护分支的用户和群组不需要合并请求来合并其功能分支。
因此，他们可以跳过合并请求批准规则，包括代码所有者。

## 在受保护的分支上运行流水线

合并或推送到受保护分支的权限定义了用户是否可以运行 CI/CD 流水线并对作业执行操作。

<!--
See [Security on protected branches](../../ci/pipelines/index.md#pipeline-security-on-protected-branches)
for details about the pipelines security model.
-->

## 删除受保护的分支

具有维护者角色及更高级别的用户可以使用 GitLab Web 界面手动删除受保护的分支：

1. 转到 **仓库 > 分支**。
1. 在要删除的分支旁边，选择 **删除** 按钮 (**{remove}**)。
1. 在确认对话框中，输入分支名称并选择 **删除受保护的分支**。

您只能从 UI 中删除受保护的分支。
这可以防止您从命令行或 Git 客户端应用程序意外删除分支。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
