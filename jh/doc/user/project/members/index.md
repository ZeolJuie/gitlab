---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 项目成员 **(FREE)**

成员是有权访问您的项目的用户和群组。

每个成员都有一个角色，这决定了他们在项目中可以做什么。

## 将用户添加到项目

将用户添加到项目中，以便他们成为成员并有权执行操作。

先决条件：

- 您必须有维护者或拥有者角色<!--[维护者或拥有者角色](../../permissions.md)-->。

将用户添加到项目：

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 在 **邀请成员** 选项卡中，在 **GitLab 成员或电子邮件地址** 下，输入用户名或电子邮件地址。在 13.11 及更高版本中，您可以[用模态窗口替换此表单](#添加成员模态窗口)。
1. 选择一个角色<!--[角色](../../permissions.md)-->。
1. （可选）选择一个访问过期日期。在该日期，用户无法再访问该项目。
1. 选择 **邀请**。

如果用户有极狐GitLab 账号，他们将被添加到成员列表中。如果您使用了电子邮件地址，则用户会收到一封电子邮件。

如果邀请未被接受，极狐GitLab 会在两天、五天和十天后发送提醒电子邮件。未接受的邀请将在 90 天后自动删除。

如果用户没有极狐GitLab 账号，系统会提示他们使用发送邀请的电子邮件地址创建一个帐户。

## 将群组添加到项目

将群组添加到项目时，群组中的每个用户都可以访问该项目。每个用户的访问权限基于：

- 他们在群组中分配的角色。
- 邀请群组时选择的最大角色。

先决条件：

- 您必须有维护者或拥有者角色<!--[维护者或拥有者角色](../../permissions.md)-->。

要将群组添加到项目：

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 在 **邀请群组** 选项卡上，在 **选择要邀请的组** 下，选择一个群组。
1. 为群组中的用户选择最高的最大角色<!--[角色](../../permissions.md)-->。
1. （可选）选择一个访问过期日期。在该日期，用户无法再访问该项目。
1. 选择 **邀请**。

该群组的成员不会显示在 **成员** 选项卡上。
**成员** 选项卡显示：

- 直接分配给项目的成员。
- 如果项目是在群组命名空间<!--[命名空间](../../group/index.md#namespaces)-->中创建的，显示其中群组的成员。

## 从另一个项目导入用户

您可以将其它项目的用户导入到您自己的项目中。用户保留与您从中导入它们的项目相同的权限。

先决条件：

- 您必须有维护者或拥有者角色<!--[维护者或拥有者角色](../../permissions.md)-->。

要导入用户：

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 在 **邀请成员** 选项卡上，在面板底部，选择 **导入**。
1. 选择项目。您只能查看您是维护者的项目。
1. 选择 **导入项目成员**。

将显示一条成功消息，新成员现在显示在列表中。

## 继承成员

当您的项目属于一个群组时，群组成员从该群组继承他们的角色。

![Project members page](img/project_members_v14_4.png)

在此例中：

- 三名成员有权访问该项目。
- **用户 0** 是一名报告者，并从包含该项目的 **demo** 群组继承了他们的角色。
- **用户 1** 直接属于该项目。在 **源** 列中，他们被列为 **直接成员**。
- **管理员** 是所有群组的所有者<!--[所有者](../../permissions.md)-->和成员。他们从 **demo** 群组继承了他们的角色。

如果用户是项目的直接成员，则可以更新到期日期。如果成员资格是从父组继承的，则只能从父组本身更新到期日期。

## 从项目中删除成员

如果用户是项目的直接成员，您可以将其删除。
如果成员资格是从父组继承的，则只能从父组本身删除该成员。

先决条件：

- 您必须有所有者角色<!--[所有者角色](../../permissions.md)-->。
- （可选）从分配给他们的所有议题和合并请求中取消指派成员。

要从项目中删除成员：

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 在要移除的项目成员旁边，选择 **移除成员** **{remove}**。
1. （可选）在确认框中，选择 **同时从相关的议题和合并请求中取消指派此用户** 复选框。
1. 为防止私有项目泄露敏感信息，请确认用户没有派生私有仓库。现有派生继续从上游项目接收更改。您可能还想配置您的项目，以防止群组中的项目从群组外派生<!--[从群组外派生](../../group/index.md#prevent-project-forking-outside-group)-->。
1. 选择 **删除成员**。

## 过滤和排序成员

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/21727) in GitLab 12.6.
> - [Improved](https://gitlab.com/groups/gitlab-org/-/epics/4901) in GitLab 13.9.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/299954) in GitLab 13.10.
-->

您可以过滤和排序项目中的成员。

### 显示继承的成员

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 在 **筛选成员** 框中，选择 `成员` `=` `继承`。
1. 按 Enter。

![Project members filter inherited](img/project_members_filter_inherited_v14_4.png)

### 显示直接成员

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 在 **筛选成员** 框中，选择 `成员` `=` `直接`。
1. 按 Enter。

![Project members filter direct](img/project_members_filter_direct_v14_4.png)

### 搜索

您可以按姓名、用户名或电子邮件搜索成员。

![Project members search](img/project_members_search_v14_4.png)

### 排序

您可以按升序或降序，根据 **账号**、**已授予访问**、**最大角色** 或 **上次登录** 对成员进行排序。

![Project members sort](img/project_members_sort_v14_4.png)

## 请求访问项目

用户可以申请成为项目的成员。

1. 转到您想加入的项目。
1. 根据项目名称，选择 **申请权限**。

![Request access button](img/request_access_button.png)

一封电子邮件将发送给最近活跃的项目维护者。
最多通知十名项目维护者。任何项目维护者都可以批准或拒绝请求。

如果项目没有任何维护者，通知将发送给项目组的最近活跃的所有者。

如果您在请求获得批准之前改变主意，请选择 **取消权限申请**。

## 阻止用户请求访问项目

您可以阻止用户请求访问项目。

先决条件：

- 您必须是项目所有者

1. 转到项目并选择 **设置 > 通用**。
1. 展开 **可见性，项目功能，权限** 部分。
1. 在 **项目可见性** 下，选择 **用户可以请求访问**。
1. 选择 **保存修改**。

## 与群组共享项目

您可以与整个群组共享一个项目<!--[share a project with an entire group](share_project_with_groups.md)-->，而不是一个一个地添加用户。

### 添加成员模态窗口

> - 引入于 13.11 版本
> - 在功能标记后部署<!--[Deployed behind a feature flag](../../feature_flags.md)-->，默认禁用。
> - 推荐生产使用
> - 用按钮替换现有表单，以打开模态窗口。
> - 要在自助管理实例中使用，需要请求管理员[启用它](#启用或禁用模态窗口)。 **(FREE SELF)**

<!--
> - Enabled on GitLab.com.
-->

WARNING:
您可能无法使用此功能。查看上面的 **版本历史** 注释以了解详细信息。

在 13.11 版本中，您可以选择替换表单，以使用模态窗口添加成员。
启用此功能后添加成员：

1. 转到您的项目并选择 **项目信息 > 成员**。
1. 选择 **邀请成员**。
1. 输入电子邮件地址并选择角色。
1. （可选）选择 **访问到期日期**。
1. 选择 **邀请**。

### 启用或禁用模态窗口 **(FREE SELF)**

添加成员的模态窗口正在开发中，可供生产使用。它部署在**默认禁用**的功能标志后面。
<!--[可以访问 GitLab Rails 控制台的管理员](../../../administration/feature_flags.md)-->可以访问 GitLab Rails 控制台的管理员可以启用它。

要启用它：

```ruby
Feature.enable(:invite_members_group_modal)
```

要禁用它：

```ruby
Feature.disable(:invite_members_group_modal)
```
