---
stage: 
group: 
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 计划和跟踪工作 **(FREE)**

<!--
Plan your work by creating requirements, issues, and epics. Schedule work
with milestones and track your team's time. Learn how to save time with
quick actions, see how GitLab renders Markdown text, and learn how to
use Git to interact with GitLab.
-->

了解如何使用 Git 与极狐GitLab 交互。

<!--
- [Labels](../user/project/labels.md)
- [Discussions](../user/discussions/index.md)
- [Iterations](../user/group/iterations/index.md)
- [Milestones](../user/project/milestones/index.md)
- [Requirements](../user/project/requirements/index.md)
- [Roadmaps](../user/group/roadmap/index.md)
- [Time tracking](../user/project/time_tracking.md)
- [Wikis](../user/project/wiki/index.md)
- [Markdown](../user/markdown.md)
- [To-Do lists](../user/todos.md)
-->
- [史诗](../user/group/epics/index.md)
- [议题](../user/project/issues/index.md)
- [标记](../user/project/labels.md)
- [使用 Git](../topics/git/index.md)
- [键盘快捷键](../user/shortcuts.md)
- [快速操作](../user/project/quick_actions.md)
