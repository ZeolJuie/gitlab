---
stage: 
group: 
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 部署并发布您的应用 **(FREE)**

在内部或向公众部署您的应用程序。 使用标志以增量方式发布功能。

- [环境和部署](../ci/environments/index.md)
- [发布](../user/project/releases/index.md)
- [功能标志](../operations/feature_flags.md)
