---
stage: Create
group: Source Code
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
description: 'This article describes how to install Git on macOS, Ubuntu Linux and Windows.'
type: howto
---

# 安装 Git **(FREE)**

开始贡献 Gitlab 项目，您必须在电脑上先安装 Git 客户端。

这篇文档展示了如何在 macOS，Ubuntu Linux 和 Windows 上安装 Git 客户端。

同时也可参考在 Git 官方网站上关于[安装 Git](https://git-scm.com/book/zh/v2/%E8%B5%B7%E6%AD%A5-%E5%AE%89%E8%A3%85-Git) 的信息。

## 在 macOS 上使用 Homebrew 包管理器安装 Git

虽然您可以使用 macOS 自带的 Git 版本，或者从项目网站上下载最新版本的 Git 安装，但是我们推荐使用 Homebrew 安装 Git，以获得可供广泛选择的依赖管理的库和应用程序。

如果您不需要访问任何额外的开发库，或者没有大约 15 GB 的磁盘空间来安装 Xcode 和 Homebrew，则使用前面提到的安装方式即可。

### 安装 Xcode

为了构建依赖项，Homebrew 需要 XCode 命令行工具。安装需要在终端中运行：

```shell
xcode-select --install
```

点击 **安装** 来下载和安装。另外，也可以选择在 macOS 应用程序商店中安装整个 [Xcode](https://developer.apple.com/cn/xcode/)。

### 安装 Homebrew

当 Xcode 安装完成后，使用浏览器打开 [Homebrew 网站](https://brew.sh/index_zh-cn)来查看官方的 Homebrew 安装说明。

### 使用 Homebrew 安装 Git

当 Homebrew 安装完成后，你现在已经准备好安装 Git 了。
打开终端并输入以下命令：

```shell
brew install git
```

恭喜！您现在已经通过 Homebrew 完成 Git 安装了。

为验证 Git 在你的系统上能正常运作，运行：

```shell
git --version
```

接下来，添加 SSH 密钥到极狐GitLab<!--[添加 SSH 密钥到 GitLab](../../../ssh/index.md)-->。

## 在 Ubuntu Linux 上安装 Git

在 Ubuntu 和其它 Linux 操作系统上，建议使用内建的包管理器来安装 Git。

打开终端并输入以下命令，从 Git 官方维护的安装包安装最新版的 Git：

```shell
sudo apt-add-repository ppa:git-core/ppa
sudo apt-get update
sudo apt-get install git
```

恭喜！您现在已经通过 Ubuntu 包管理器完成 Git 安装了。

为验证 Git 在你的系统上能正常运作，运行：

```shell
git --version
```

接下来，添加 SSH 密钥到极狐GitLab<!--[添加 SSH 密钥到 GitLab](../../../ssh/index.md)-->。

## 在 Windows 上从 Git 网站安装 Git

打开 [Git 网站](https://git-scm.com) 下载并安装 Git Windows 版本。

接下来，添加 SSH 密钥到极狐GitLab<!--[添加 SSH 密钥到 GitLab](../../../ssh/index.md)-->。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
