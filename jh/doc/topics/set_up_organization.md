---
stage:
group:
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 设置您的组织 **(FREE)**

配置您的组织及其用户。确定用户角色并让每个人都可以访问他们需要的项目。

<!--
- [Members](../user/project/members/index.md)
- [Workspace](../user/workspace/index.md) _(Coming soon)_
- [Groups](../user/group/index.md)
- [User account options](../user/profile/index.md)
- [SSH keys](../ssh/index.md)
- [GitLab.com settings](../user/gitlab_com/index.md)
-->

- [成员](../user/project/members/index.md)
- [SSH 密钥](../ssh/index.md)