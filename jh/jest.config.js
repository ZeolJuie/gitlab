const fs = require('fs');
const path = require('path');
const baseConfig = require('../jest.config.base');

const ROOT_PATH = path.resolve(__dirname, '..');
const SKIP_LIST_CONFIG_PATH = path.join(ROOT_PATH, 'jh/spec/frontend/skip_list.json');

const config = { ...baseConfig('spec/frontend') };

function generateSkipList(globalConfig) {
  let globalIgnoreTestList = Array.isArray(globalConfig.testPathIgnorePatterns)
    ? globalConfig.testPathIgnorePatterns
    : [];

  if (fs.existsSync(SKIP_LIST_CONFIG_PATH)) {
    let skipListJH = JSON.parse(fs.readFileSync(SKIP_LIST_CONFIG_PATH, 'utf-8'));
    skipListJH = skipListJH.by_file;
    globalIgnoreTestList = globalIgnoreTestList.concat(skipListJH);
  }

  return globalIgnoreTestList;
}

const ignoreTestList = generateSkipList(config);

config.setupFilesAfterEnv.push(path.join(ROOT_PATH, 'jh/spec/frontend/setup/jh_test_setup.js'));

module.exports = {
  ...config,
  testPathIgnorePatterns: ignoreTestList,
};
