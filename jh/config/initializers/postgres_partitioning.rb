# frozen_string_literal: true

Gitlab::Database::Partitioning.register_models([
  Phone::VerificationCode
])

Gitlab::Database::Partitioning.sync_partitions_ignore_db_error
