# frozen_string_literal: true

module ContentValidation
  class CommitServiceWorker
    include ApplicationWorker

    data_consistency :delayed

    sidekiq_options retry: true

    loggable_arguments 0, 1

    feature_category_not_owned!

    idempotent!

    deduplicate :until_executing

    worker_has_external_dependencies!

    queue_namespace :content_validation

    def perform(commit_id, container_identifier, user_id)
      container, project, repo_type = Gitlab::GlRepository.parse(container_identifier)
      commit = container.repository.commit(commit_id)
      user = User.find(user_id)

      ContentValidation::CommitService.new(
        commit: commit,
        container: container,
        project: project,
        repo_type: repo_type,
        user: user
      ).execute
    end
  end
end
