import { waitForCaptchaToBeSolved } from '~/captcha/wait_for_captcha_to_be_solved';
import { initTencentRecaptchaScript } from './init_recaptcha_script';

function calloutTencentRecaptcha(TencentCaptcha) {
  return new Promise((resolve, reject) => {
    const captcha = new TencentCaptcha(gon.tencent_captcha_app_id, (res) => {
      if (res.ret === 0) {
        const params = {
          rand_str: res.randstr,
          ticket: res.ticket,
        };
        resolve(params);
      } else {
        reject(res);
      }
    });
    captcha.show();
  });
}

const tencentCaptchaHandler = (resolve, reject) => {
  if (window.TencentCaptcha) {
    calloutTencentRecaptcha(window.TencentCaptcha)
      .then((response) => {
        resolve(response);
      })
      .catch(reject);
  } else {
    initTencentRecaptchaScript()
      .then(async (TencentCaptcha) => {
        try {
          const response = await calloutTencentRecaptcha(TencentCaptcha);
          resolve(response);
        } catch (e) {
          reject(e);
        }
      })
      .catch(reject);
  }
};

const gCaptchaHandler = (resolve, reject) => {
  waitForCaptchaToBeSolved(window.gon.recaptcha_sitekey)
    .then((response) => {
      const params = {
        'g-recaptcha-response': response,
      };
      resolve(params);
    })
    .catch((e) => reject(e));
};

export const captchaCheck = () =>
  new Promise((resolve, reject) => {
    if (gon.tencent_captcha_enabled) {
      tencentCaptchaHandler(resolve, reject);
    } else if (gon.recaptcha_enabled) {
      gCaptchaHandler(resolve, reject);
    }
  });
