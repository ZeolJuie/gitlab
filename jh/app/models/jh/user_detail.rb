# frozen_string_literal: true

module JH
  # User JH mixin
  #
  # This module is intended to encapsulate JH-specific model logic
  # and be prepended in the  model

  module UserDetail
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      include ContentValidateable
      validates :pronouns, :pronunciation, :job_title, :bio, content_validation: true, if: :should_validate_content?
    end
  end
end
