# frozen_string_literal: true

module JH
  module ApplicationSettingsHelper
    extend ::Gitlab::Utils::Override

    override :visible_attributes
    def visible_attributes
      super + [
        :content_validation_endpoint_enabled,
        :content_validation_endpoint_url,
        :content_validation_api_key
      ]
    end
  end
end
