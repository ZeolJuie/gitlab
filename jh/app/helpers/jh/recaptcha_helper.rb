# frozen_string_literal: true

module JH
  module RecaptchaHelper
    extend ::Gitlab::Utils::Override

    override :show_recaptcha_sign_up?
    # Skip since JH use phone verification include this
    def show_recaptcha_sign_up?
      return if ::Gitlab.jh? && ::Feature.enabled?(:real_name_system)

      super
    end
  end
end
