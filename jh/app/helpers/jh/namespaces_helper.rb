# frozen_string_literal: true

module JH
  module NamespacesHelper
    extend ::Gitlab::Utils::Override

    def buy_additional_minutes_path(_namespace)
      ::Gitlab::SubscriptionPortal.subscriptions_more_minutes_url
    end

    def buy_storage_path(_namespace)
      ::Gitlab::SubscriptionPortal.subscriptions_more_storage_url
    end
  end
end
