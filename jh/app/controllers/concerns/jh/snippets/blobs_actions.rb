# frozen_string_literal: true

module JH
  module Snippets::BlobsActions
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    override :raw
    def raw
      return super unless ::ContentValidation::Setting.block_enabled?(snippet)

      last_commit = snippet.repository.last_commit_for_path(snippet.default_branch, blob.path, literal_pathspec: false)
      content_blocked_state = ::ContentValidation::ContentBlockedState.find_by_container_commit_path(snippet, last_commit&.id, blob.path)

      return super unless content_blocked_state.present?

      render plain: s_("ContentValidation|According to the relevant laws and regulations, this content is not displayed.")
    end
  end
end
