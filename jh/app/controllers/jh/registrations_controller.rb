# frozen_string_literal: true

module JH
  module RegistrationsController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override
    include CheckPhoneAndCode

    prepended do
      before_action :verify_code_received_by_phone, only: [:create], if: -> { ::Gitlab.dev_env_or_com? && ::Gitlab.jh? && ::Feature.enabled?(:real_name_system) }
    end

    private

    # add phone verification_code for JH
    override :sign_up_params_attributes
    def sign_up_params_attributes
      if ::Feature.enabled?(:real_name_system)
        super + [:phone, :verification_code, :area_code]
      else
        super
      end
    end

    def verify_code_received_by_phone
      ensure_correct_params!
      check_verification_code
    end

    override :check_captcha
    def check_captcha
      # Skip the form check_captcha if JH SaaS or dev
      return if ::Gitlab.jh? && ::Gitlab.dev_env_or_com? && ::Feature.enabled?(:real_name_system)

      super
    end
  end
end
