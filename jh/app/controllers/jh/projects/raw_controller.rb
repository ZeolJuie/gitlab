# frozen_string_literal: true

module JH
  module Projects::RawController
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    override :show
    def show
      return super unless ::ContentValidation::Setting.block_enabled?(project)

      ref, path = extract_ref(get_id)
      last_commit = ::Gitlab::Git::Commit.last_for_path(repository, ref, path, literal_pathspec: true)
      content_blocked_state = ::ContentValidation::ContentBlockedState.find_by_container_commit_path(project, last_commit, path)

      return super unless content_blocked_state.present?

      render plain: s_("ContentValidation|According to the relevant laws and regulations, this content is not displayed.")
    end
  end
end
